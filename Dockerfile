# Stage 1 : build the vue application
FROM node:lts-alpine3.10 AS build-front

USER root
WORKDIR /work

COPY ./virtual-teacher /work
RUN npm install
RUN npm run build


## Stage 2 : build with maven builder image with native capabilities
FROM quay.io/quarkus/centos-quarkus-maven:20.1.0-java11 AS build-back

USER root
RUN yum install -y zip
USER quarkus
ENV TZ=Europe/Paris
COPY api/pom.xml /usr/src/app/
RUN mvn -f /usr/src/app/pom.xml -B de.qaware.maven:go-offline-maven-plugin:1.2.5:resolve-dependencies
COPY api/src /usr/src/app/src
COPY --from=build-front /work/target /usr/src/app/src/main/resources/META-INF/resources
USER root
RUN chown -R quarkus /usr/src/app

USER quarkus
RUN mvn -f /usr/src/app/pom.xml -Pnative clean package


## Stage 3 : create the docker final image
FROM registry.access.redhat.com/ubi8/ubi-minimal

WORKDIR /work/
ENV TZ=Europe/Paris

USER root
RUN microdnf install yum
RUN yum install -y openldap* zip openssl

COPY --from=build-back /usr/src/app/target/*-runner /work/application
COPY prod/exec.sh /work/exec
COPY prod/config/application.properties /work/config/application.properties

# set up permissions for user `1001`
RUN chmod 775 /work /work/application /work/exec \
  && chown -R 1001 /work \
  && chmod -R "g+rwX" /work \
  && chown -R 1001:root /work

EXPOSE 8080
USER 1001

CMD ["./exec"]