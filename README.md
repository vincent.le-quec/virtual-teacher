# Virtual Teacher

## Build & serve the project
```
make
```
This will create the production front application, package it into the back et and build the production bin application. Then the application will be served on port 80.

## Run project in developement mode
*Needs npm and nodeJs installed*
```
make dev
```
This will run the api in docker exposed on port 8081 and the front locally on port 8080.

## Build the project
```
make build
```
This will create the production front application, package it into the back et and build the production bin application.

## Deploy the project
```
make serve
```
This will deploy the app in the "prod" folder with its files and be exposed on port 80.

## Extract bin Application
```
make build
make extract
```
This will add the project bin inside the "prod" folder.
