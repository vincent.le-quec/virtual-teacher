export IMAGE_NAME=virtual-teacher-api

.PHONY: build run stop

all: build extract clean serve

build:
	docker build -t virtual-teacher-api:prod .
	# docker build -t virtual-teacher-api:prod . --no-cache

dev:
	npm --prefix virtual-teacher run serve & \
	make -C api dev

serve:
	docker-compose -p virtual-teacher-api up

clean:
	docker-compose -p virtual-teacher-api down

extract:
	docker-compose -p virtual-teacher-api up -d
	sleep 2
	docker cp $$(docker ps | grep 'virtual-teacher-api:prod' | cut -d' ' -f1):/work/application prod/application

serve-bin:
	docker-compose -p virtual-teacher-api -f prod/docker-compose.yml up --build

clear-bin:
	rm -f prod/application