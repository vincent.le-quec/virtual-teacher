--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.0 (Debian 13.0-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: field; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.field (
    id bigint NOT NULL,
    description character varying(255),
    field_name character varying(255),
    icon character varying(255)
);


ALTER TABLE public.field OWNER TO postgres;

--
-- Name: field_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.field_group (
    id bigint NOT NULL,
    field_id bigint,
    group_id bigint
);


ALTER TABLE public.field_group OWNER TO postgres;

--
-- Name: field_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.field_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.field_group_id_seq OWNER TO postgres;

--
-- Name: field_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.field_group_id_seq OWNED BY public.field_group.id;


--
-- Name: field_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.field_id_seq OWNER TO postgres;

--
-- Name: field_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.field_id_seq OWNED BY public.field.id;


--
-- Name: field_person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.field_person (
    id bigint NOT NULL,
    field_id bigint,
    person_id bigint
);


ALTER TABLE public.field_person OWNER TO postgres;

--
-- Name: field_person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.field_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.field_person_id_seq OWNER TO postgres;

--
-- Name: field_person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.field_person_id_seq OWNED BY public.field_person.id;


--
-- Name: groupe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.groupe (
    id bigint NOT NULL,
    title character varying(255)
);


ALTER TABLE public.groupe OWNER TO postgres;

--
-- Name: groupe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.groupe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupe_id_seq OWNER TO postgres;

--
-- Name: groupe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.groupe_id_seq OWNED BY public.groupe.id;


--
-- Name: lesson; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lesson (
    id bigint NOT NULL,
    beginning timestamp without time zone,
    description character varying(255),
    ending timestamp without time zone,
    hide boolean NOT NULL,
    lesson_order integer,
    title character varying(255),
    type_lesson integer,
    field_id bigint
);


ALTER TABLE public.lesson OWNER TO postgres;

--
-- Name: lesson_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lesson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lesson_id_seq OWNER TO postgres;

--
-- Name: lesson_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lesson_id_seq OWNED BY public.lesson.id;


--
-- Name: paragraph; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.paragraph (
    id bigint NOT NULL,
    content text,
    paragraph_order integer,
    title character varying(255),
    title_size integer,
    type integer,
    lesson_id bigint
);


ALTER TABLE public.paragraph OWNER TO postgres;

--
-- Name: paragraph_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.paragraph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paragraph_id_seq OWNER TO postgres;

--
-- Name: paragraph_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.paragraph_id_seq OWNED BY public.paragraph.id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id bigint NOT NULL,
    email character varying(255),
    first_name character varying(255),
    login character varying(255),
    name character varying(255),
    type_person integer,
    group_id bigint
);


ALTER TABLE public.person OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task (
    id bigint NOT NULL,
    beginning timestamp without time zone,
    contains_work boolean,
    description character varying(255),
    displayed boolean NOT NULL,
    ending timestamp without time zone,
    title character varying(255),
    lesson_id bigint
);


ALTER TABLE public.task OWNER TO postgres;

--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_id_seq OWNER TO postgres;

--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.task_id_seq OWNED BY public.task.id;


--
-- Name: work; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.work (
    id bigint NOT NULL,
    file_name character varying(255),
    rendering_date timestamp without time zone,
    person_id bigint,
    task_id bigint
);


ALTER TABLE public.work OWNER TO postgres;

--
-- Name: work_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.work_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_id_seq OWNER TO postgres;

--
-- Name: work_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.work_id_seq OWNED BY public.work.id;


--
-- Name: field id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field ALTER COLUMN id SET DEFAULT nextval('public.field_id_seq'::regclass);


--
-- Name: field_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_group ALTER COLUMN id SET DEFAULT nextval('public.field_group_id_seq'::regclass);


--
-- Name: field_person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_person ALTER COLUMN id SET DEFAULT nextval('public.field_person_id_seq'::regclass);


--
-- Name: groupe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groupe ALTER COLUMN id SET DEFAULT nextval('public.groupe_id_seq'::regclass);


--
-- Name: lesson id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson ALTER COLUMN id SET DEFAULT nextval('public.lesson_id_seq'::regclass);


--
-- Name: paragraph id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paragraph ALTER COLUMN id SET DEFAULT nextval('public.paragraph_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Name: task id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task ALTER COLUMN id SET DEFAULT nextval('public.task_id_seq'::regclass);


--
-- Name: work id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work ALTER COLUMN id SET DEFAULT nextval('public.work_id_seq'::regclass);


--
-- Data for Name: field; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.field (id, description, field_name, icon) FROM stdin;
1	Un cours sur CUDA. Lorem ipsum dolor sit amet.	CUDA	mdi-school
2	Un cours sur Android. Lorem ipsum dolor sit amet.	Android	mdi-science
3	Lorem ipsum dolor sit amet.	Management de Projets	mdi-school
4	Lorem ipsum dolor sit amet.	Objets connectés	mdi-cloud-upload
5	Un cours sur les Design Pattern. Lorem ipsum dolor sit amet.	Design Pattern	mdi-science
\.


--
-- Data for Name: field_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.field_group (id, field_id, group_id) FROM stdin;
1	1	3
2	2	3
3	2	2
\.


--
-- Data for Name: field_person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.field_person (id, field_id, person_id) FROM stdin;
1	1	1
2	1	2
3	2	1
4	2	2
5	3	2
6	3	1
7	4	3
8	5	2
\.


--
-- Data for Name: groupe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.groupe (id, title) FROM stdin;
1	2018TL3INF1
2	2019TM1INF1
3	2020TM2ACD1
\.


--
-- Data for Name: lesson; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lesson (id, beginning, description, ending, hide, lesson_order, title, type_lesson, field_id) FROM stdin;
1	2020-10-22 14:00:00	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-12-30 14:00:00	f	1	Introduction et historique	0	1
2	2020-10-22 14:00:00	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-12-30 14:00:00	f	2	Programmation avec CUDA	0	1
3	2020-10-22 14:00:00	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-12-30 14:00:00	f	3	TP1	1	1
4	2020-11-01 14:00:00	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-12-30 14:00:00	f	4	Le modèle logique	0	1
5	2020-11-02 14:00:00	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-12-30 14:00:00	f	5	Examen	1	1
\.


--
-- Data for Name: paragraph; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.paragraph (id, content, paragraph_order, title, title_size, type, lesson_id) FROM stdin;
1	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1	Introduction	1	0	1
2	Lorem ipslessonsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2	Première sous partie 	2	0	1
3	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	3	Deuxième sous partie 	2	0	1
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, email, first_name, login, name, type_person, group_id) FROM stdin;
1	tesla@ldap.forumsys.com	tesla	tesla	Tesla	2	\N
2	einstein@ldap.forumsys.com	einstein	einstein	Einstein	1	\N
3	euler@ldap.forumsys.com	euler	euler	Euler	1	\N
4	riemann@ldap.forumsys.com	riemann	riemann	Riemann	0	3
5	gauss@ldap.forumsys.com	gauss	gauss	Gauss	0	3
6	euclid@ldap.forumsys.com	euclid	euclid	Euclid	0	2
7	newton@ldap.forumsys.com	newton	newton	Newton	0	2
8	newton@ldap.forumsys.com	admin	admin	Admin	2	\N
9	galieleo@ldap.forumsys.com	galieleo	galieleo	Galilei	0	1
\.


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.task (id, beginning, contains_work, description, displayed, ending, title, lesson_id) FROM stdin;
1	2020-08-22 19:10:25	f	ceci est une description	t	2021-06-22 19:10:25	task 1	1
2	2020-08-22 19:10:25	f	une nouvelle description	t	2021-06-22 19:10:25	task 2	1
3	2020-08-22 19:10:25	f	Et bosser...	t	2021-06-22 19:10:25	task 3	2
4	2020-08-22 19:10:25	t	faites le TP	t	2021-06-22 19:10:25	TP1	3
5	2020-08-22 19:10:25	t	bosser vos exam	t	2021-06-22 19:10:25	Exam	5
\.


--
-- Data for Name: work; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.work (id, file_name, rendering_date, person_id, task_id) FROM stdin;
\.


--
-- Name: field_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.field_group_id_seq', 3, true);


--
-- Name: field_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.field_id_seq', 5, true);


--
-- Name: field_person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.field_person_id_seq', 8, true);


--
-- Name: groupe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.groupe_id_seq', 3, true);


--
-- Name: lesson_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lesson_id_seq', 5, true);


--
-- Name: paragraph_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.paragraph_id_seq', 3, true);


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 9, true);


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.task_id_seq', 5, true);


--
-- Name: work_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.work_id_seq', 1, false);


--
-- Name: field_group field_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_group
    ADD CONSTRAINT field_group_pkey PRIMARY KEY (id);


--
-- Name: field_person field_person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_person
    ADD CONSTRAINT field_person_pkey PRIMARY KEY (id);


--
-- Name: field field_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field
    ADD CONSTRAINT field_pkey PRIMARY KEY (id);


--
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (id);


--
-- Name: lesson lesson_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson
    ADD CONSTRAINT lesson_pkey PRIMARY KEY (id);


--
-- Name: paragraph paragraph_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paragraph
    ADD CONSTRAINT paragraph_pkey PRIMARY KEY (id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: work work_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work
    ADD CONSTRAINT work_pkey PRIMARY KEY (id);


--
-- Name: work fk2a1eirtusge8dbbeybx0najmw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work
    ADD CONSTRAINT fk2a1eirtusge8dbbeybx0najmw FOREIGN KEY (task_id) REFERENCES public.task(id);


--
-- Name: task fk5x8hrayewoued0usmps6rhk9e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fk5x8hrayewoued0usmps6rhk9e FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: field_group fka2345iq3m9imtlqlmmqipfm6a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_group
    ADD CONSTRAINT fka2345iq3m9imtlqlmmqipfm6a FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- Name: field_person fkarsp6gnq8kndy3pnxxjrdmrjs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_person
    ADD CONSTRAINT fkarsp6gnq8kndy3pnxxjrdmrjs FOREIGN KEY (field_id) REFERENCES public.field(id);


--
-- Name: field_group fkdd491j8kp3nwlilmttu855c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_group
    ADD CONSTRAINT fkdd491j8kp3nwlilmttu855c FOREIGN KEY (field_id) REFERENCES public.field(id);


--
-- Name: paragraph fkg4figvrj2wdwdsg0wymje0esf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paragraph
    ADD CONSTRAINT fkg4figvrj2wdwdsg0wymje0esf FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: lesson fkjifj0958a61ry8ky25a7va80k; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson
    ADD CONSTRAINT fkjifj0958a61ry8ky25a7va80k FOREIGN KEY (field_id) REFERENCES public.field(id);


--
-- Name: work fkmwnxlplhsq3j4kyipvbku7mwi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work
    ADD CONSTRAINT fkmwnxlplhsq3j4kyipvbku7mwi FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: field_person fkpt55jifk77pckc9w0ct0ufnc1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.field_person
    ADD CONSTRAINT fkpt55jifk77pckc9w0ct0ufnc1 FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person fkqjh4mqwej3hwh13ooarxanntc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT fkqjh4mqwej3hwh13ooarxanntc FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- PostgreSQL database dump complete
--

