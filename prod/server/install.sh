#!/bin/bash


echo "--- Upgrade"
apt-get update -y
apt-get upgrade -y
apt-get autoremove -y

echo "--- docker"
apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt update -y

apt-get install -y docker-ce docker-ce-cli containerd.io


echo "--- docker-compose"
curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

echo "--- NodeJS"
curl -sL https://deb.nodesource.com/setup_lts.x | bash -
apt-get install -y nodejs

echo "--- Emacs"
apt-get install -y emacs

echo "--- .bash_aliases"
echo "export PS1='\[\e[0;32m\](\w)\n\[\e[0;36m\]$? \[\e[0;33m\]\u \[\e[0;39m\]$ '
# some more ls aliases
alias l='ls -CF'
alias ll='ls -lF'
alias lll='ls -alF'
alias la='ls -A'

alias ...='cd .. ; cd ..'
alias ..='cd ..'
alias ne='emacs -nw'
" >> ~/.bashrc