# Server

https://wiki.info.univ-angers.fr/leria:cloud

```bash
ssh vlequec@leria-etud.univ-angers.fr -p 2019
# Le mot de passe est identique à celui de l'ENT

# Add ssh-key identification
ssh-copy-id vlequec@leria-etud.univ-angers.fr -p 2019

ssh root@etud-kvm-vlequec
http://etud-kvm-vlequec.leria-etud.univ-angers.fr
```

```bash
# docker
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt update

apt-get install docker-ce docker-ce-cli containerd.io

# docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# NodeJS
curl -sL https://deb.nodesource.com/setup_lts.x | bash -
apt-get install -y nodejs
```
