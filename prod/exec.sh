#!/bin/bash
openssl genrsa -out rsaPrivateKey.pem 2048
openssl rsa -pubout -in rsaPrivateKey.pem -out public.pem
openssl pkcs8 -topk8 -nocrypt -inform pem -in rsaPrivateKey.pem -outform pem -out private.pem
rm -rf rsaPrivateKey.pem

./application -Dquarkus.http.host=0.0.0.0

