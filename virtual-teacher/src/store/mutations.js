import {
    SET_AUTHENTICATION,
    SET_CURRENTLESSONID,
    SET_ERROR,
    SET_FIELD,
    SET_LESSONS,
    SET_LOADING,
    SET_STUDENT_GROUP,
    SET_TEACHERS_INFOS,
    SET_USER
} from "./mutations.type";

const mutations = {
    [SET_AUTHENTICATION] (state, authentication) {
        state.authentication = authentication;
        if (authentication == null) {
            localStorage.removeItem(SET_AUTHENTICATION);
        } else {
            localStorage.setItem(SET_AUTHENTICATION, authentication);
        }
    },
    [SET_ERROR] (state, error) {
        state.error = error;
    },
    [SET_LOADING] (state, v) {
        state.isLoading = v;
    },
    [SET_USER] (state, user) {
        state.user = user;
    },
    [SET_LESSONS] (state, newLessons) {
        newLessons.forEach((lesson, index) => {
            lesson.order = index + 1;
        })

        // sort the lesson by id (order field should exist)
        newLessons.sort((lesson1, lesson2) => {
            return lesson1.order - lesson2.order;
        })

        // sort paragraph by order field
        newLessons.forEach(({ paragraphs }) => {
            paragraphs.sort((paragraph1, paragraph2) => {
                return paragraph1.order - paragraph2.order
            })
        })

        state.lessons = newLessons;
    },
    [SET_CURRENTLESSONID] (state, currentLessonId) {
        state.currentLessonId = currentLessonId;
    },
    [SET_FIELD] (state, field) {
        if (field != null && field.teachers.length > 0) {
            field.teachers.forEach(teacher => {
                // add fullname property to each teacher
                teacher.fullName = teacher.first_name + " - " + teacher.name;

                return teacher;
            })
        }

        state.field = field;
    },
    [SET_TEACHERS_INFOS] (state, infos) {
        state.teachersInfos = infos;
    },
    [SET_STUDENT_GROUP] (state, groups) {
        state.studentGroup = groups;
    },
};
export default mutations;
