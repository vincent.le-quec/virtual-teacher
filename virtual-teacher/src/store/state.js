export default () => ({
    authentication: null,
    isLoading: false,
    user: null,
    error: null,
    lessons: [],
    currentLessonId: 0,
    field: null,
    teachersInfos: [],
    studentGroup: []
});
