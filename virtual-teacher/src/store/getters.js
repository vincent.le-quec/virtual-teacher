import { SET_AUTHENTICATION } from "@/store/mutations.type";

export default {
    authentication: state => {
        return state.authentication || localStorage.getItem(SET_AUTHENTICATION);
    },
    isLoading: state => {
        return state.isLoading;
    },
    error: state => {
        return state.error;
    },
    user: state => {
        return state.user;
    },
    lessons: ({ lessons }) => {
        return lessons;
    },
    currentLessonId: ({ currentLessonId }) => {
        return currentLessonId;
    },
    field: ({ field }) => {
        return field;
    },
    teachersInfos: state => {
        return state.teachersInfos;
    },
    studentGroup: state => {
        return state.studentGroup;
    },
};
