import { LOGIN, LOGOUT, REORDER_LESSONS, REORDER_PARAGRAPHS } from "./actions.type";
import { SET_AUTHENTICATION, SET_LESSONS, SET_USER } from "@/store/mutations.type";
import Services from "@/services/api"

const actions = {
    [LOGIN] ({ commit }, { token }) {
        commit(SET_USER, null);
        commit(SET_AUTHENTICATION, token);
    },
    [LOGOUT] ({ commit }) {
        commit(SET_AUTHENTICATION, null);
        commit(SET_USER, null);
    },
    async [REORDER_LESSONS] ({ commit }, { newLessons }) {
        newLessons.forEach((lesson, index) => {
            lesson.order = index + 1;
        })

        for (const { id, title, order } of newLessons) {
            await Services.Lesson.patchLesson({
                id,
                title,
                order
            })
        }

        commit(SET_LESSONS, newLessons)
    },
    async [REORDER_PARAGRAPHS] ({ commit }, { lessons, newLesson, index }) {
        newLesson.paragraphs.forEach((paragraph, index) => {
            paragraph.order = index + 1;
        })

        // If all the requests have succeed
        const oks = newLesson.paragraphs.map(async ({ id, order }) => {
            const { ok } = await Services.Paragraph.patchParagraph({
                id,
                order
            })

            return ok
        })

        const ok = oks.filter(ok => ok)

        // If api call was successful then notify Vuex of the changes
        if (ok) {
            lessons[index] = newLesson;
            commit(SET_LESSONS, lessons) // statement to remove after implementing patch request server side
        }
    }
};
export default actions;
