export default {
    'LNG_LOGIN_TITLE': 'Authentication',
    'LNG_LOGIN_LOGIN': 'Login',
    'LNG_LOGIN_PASSWORD': 'Password',
    'LNG_LOGIN_INPUT': 'Required Field',
    'LNG_LOGIN_CNX': 'Sign in',
    'LNG_LOADER_INFO': 'We\'re recovering your data...',
    'LNG_LOGIN_401': 'Wrong username or password.',
    'LNG_LOGIN_500': 'Server unavailable, please try again later...',
    'LNG_LOGIN_UNKNOWN': 'An unknown error has occurred, we are doing our best to deal with this problem.',

    'LNG_ICON_PIKER_TITLE': 'Icon picker',
    'LNG_ICON_PIKER_DELETE_TOOLTIP': 'Delete your current icon',
    'LNG_ICON_PIKER_CHOOSE_TOOLTIP': 'Choose an icon for your discipline',

    'LNG_SHOW_LESSON_TOOLTIP': 'Choose to display or not the lesson to the students',
    'LNG_SHOW_LESSON_HIDDEN': 'Hidden',
    'LNG_SHOW_LESSON_VISIBLE': 'Visible',


    'LNG_APP_BAR_LOGIN': 'Log in',

    'LNG_DROP_DOWN_TASKS': 'My tasks',
    'LNG_DROP_DOWN_FIELDS': 'My lessons',
    'LNG_DROP_DOWN_LOGOUT': 'Logout',

    'LNG_EDITOR_BUTTON': 'Save',
    'LNG_EDITOR_SNACKBAR': 'Something changed',
    'LNG_EDITOR_RETURN': 'Return',
    'LNG_MEDIA_ERROR': 'An error occured while uploading file',

    'LNG_FIELD_ADD': 'Add a new field',
    'LNG_FIELD_EDIT': 'Modify a field',
    'LNG_FIELD_NAME': 'Field name',
    'LNG_FIELD_ICON': 'Field icon',
    'LNG_FIELD_ICON_TOOLTIP': 'Choose an icon for your discipline',
    'LNG_FIELD_DESCRIPTION': 'Description',
    'LNG_FIELD_TEACHERS': 'Teachers',
    'LNG_FIELD_STUDENTS': 'Groups of students',
    'LNG_FIELD_SUBMIT': 'Submit',
    'LNG_FIELD_CANCEL': 'Cancel',
    'LNG_LESSON_REQUIRED': 'A name is required',
    'LNG_LESSON_ATLEASTONETEACHER': 'Must have at least 1 teacher',
    'LNG_FORBIDDEN_ACCESS': 'Sorry, you don\'t have access to this page',
    'LNG_FIELDS_TITLE': 'Your fields',
    'LNG_FIELDS_BTNADD': 'Add',
    'LNG_FIELDS_CONFIRMATION': 'Are you sure you want to delete this field ?',
    'LNG_FIELDS_deletion': 'Delete',
    'LNG_FIELDS_CANCEL': 'Cancel',
    'LNG_FIELD_ADDED': 'The new field has been added !',
    'LNG_FIELD_UPDATED': 'The field has been updated !',
    'LNG_FIELD_ERROR400': 'Sorry, the request failed, data types are incorrect.',
    'LNG_FIELD_CLIENTERROR': 'Sorry, a web client error has occurred, the request failed.',
    'LNG_FIELD_SERVERERROR': 'Sorry, a server error has occurred, the request failed.',
    'LNG_FIELD_FAILED': 'Sorry, the request failed.',


    "Task": {
        'LNG_TASK_ADD': 'Add a task',
        'LNG_TASK_EDIT': 'Edit the task',
        'LNG_TASK_TITLE': 'Title',
        'LNG_TASK_DESCRIPTION': 'Description',
        'LNG_TASK_CONTAINSWORK': 'Contains work',

        'LNG_TASK_BEGINNING': 'Beginning',
        'LNG_TASK_ENDING': 'Ending',
        'LNG_TASK_CANCEL': 'Cancel',
        'LNG_TASK_MANDATORY': 'This field is required',
        'LNG_TASK_MAX_LENGHT': 'Max 255 characters',
        'LNG_TASK_ADDED': 'The new task has been created !',
        'LNG_TASK_UPDATED': 'The new task has been updated !',
        'LNG_TASK_FAILED': 'Sorry, the request failed.',

        'LNG_TASK_TASKS': 'Your tasks',
        'LNG_TASK_FIELD': 'Lessons',
        'LNG_TASK_TA': 'Tasks',
        'LNG_TASK_DATE': 'For the ',

        'LNG_TASK_DELETE': 'Delete',
        'LNG_TASK_DELETE_QUESTION': 'Do you really want to delete this task ?',
        'LNG_TASK_DELETE_CANCEL': 'Cancel',
        'LNG_TASK_DELETE_DELETED': 'The task has been deleted.',
        'LNG_TASK_DELETE_404': 'The task doesn\'t exist.',
        'LNG_TASK_DELETE_CLIENTERROR': 'Sorry, a web client error has occurred, the request failed.',
        'LNG_TASK_DELETE_SERVERERROR': 'Sorry, a server error has occurred, the request failed.',
        'LNG_TASK_DELETE_FAILED': 'Sorry, the request failed.',

        'LNG_TASK_NO_DATE' : 'No ending date',
    },

    "Work": {
        'LNG_WORK_SHOW': 'See the works',
        'LNG_WORK_DATE': 'Date',
        'LNG_WORK_USER': 'User',
        'LNG_WORK_FILE': 'File',
        'LNG_WORK_DOWNLOAD': 'Download',
        'LNG_WORK_DOWNLOAD_ALL': 'Download all',
        'LNG_WORK_LABEL': 'Your work',
        'LNG_WORK_DELETE': 'Delete',
        'LNG_WORK_UPLOAD': 'Upload',
    },

    'LNG_LESSON_ADDLESSON': 'Add a new lesson',
    'LNG_LESSON_EDITLESSON': 'Edit the lesson',
    'LNG_LESSON_TITLE': 'Title',
    'LNG_LESSON_TYPE': 'Type',
    'LNG_LESSON_CM': 'Lecture',
    'LNG_LESSON_TDTP': 'Directed Study / Practical Work',
    'LNG_LESSON_CONTAINSWORK': 'Contains work',
    'LNG_LESSON_HIDE': 'Hide the lesson',
    'LNG_LESSON_VALIDATE': 'Validate',
    'LNG_LESSON_CANCEL': 'Cancel',
    'LNG_LESSON_MANDATORY': 'This field is required',
    'LNG_LESSON_DATESTART': 'Beginning date',
    'LNG_LESSON_DATEEND': 'Ending date',
    'LNG_LESSON_TIMESTART': 'Beginning time',
    'LNG_LESSON_TIMEEND': 'Ending time',
    'LNG_LESSON_BEGAFTEREND': 'Warning : The beginning date is after is the ending date.',
    'LNG_LESSON_ADD': 'Add',
    'LNG_LESSON_ADDED': 'The new lesson has been created !',
    'LNG_LESSON_UPDATED': 'The lesson has been updated !',
    'LNG_LESSON_FAILED': 'Sorry, the request failed.',
    'LNG_LESSON_ERROR400': 'Sorry, the request failed, data types are incorrect.',
    'LNG_LESSON_CLIENTERROR': 'Sorry, a web client error has occurred, the request failed.',
    'LNG_LESSON_SERVERERROR': 'Sorry, a server error has occurred, the request failed.',
    'LNG_LESSON_EDIT': 'Edit',
    'LNG_LESSON_WORKFORMINCORRECT': 'Warning : Dates aren\'t well filled.',
    'LNG_LESSON_DELETE': 'Delete',
    'LNG_LESSON_DELETE_QUESTION': 'Do you really want to delete this lesson ?',
    'LNG_LESSON_DELETE_CANCEL': 'Cancel',
    'LNG_LESSON_DELETE_DELETED': 'The lesson has been deleted.',
    'LNG_LESSON_DELETE_CLIENTERROR': 'Sorry, a web client error has occurred, the request failed.',
    'LNG_LESSON_DELETE_SERVERERROR': 'Sorry, a server error has occurred, the request failed.',
    'LNG_LESSON_DELETE_FAILED': 'Sorry, the request failed.',
    'LNG_LESSON_OPEN_EDITOR': 'Open the lesson editor',
};
