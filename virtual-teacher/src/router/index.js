import Vue from 'vue'
import VueRouter from 'vue-router'

import FieldsAll from '../views/Fields.view'

const Error404      = () => import(/* webpackChunkName: "Error404" */ '../views/Error404.view');
const LessonEditor  = () => import(/* webpackChunkName: "LessonEditor" */ '../views/LessonEditor.view');
const LessonReader  = () => import(/* webpackChunkName: "LessonReader" */ '../views/LessonReader.view');
const Logout        = () => import(/* webpackChunkName: "Logout" */ '../views/Logout.view');
const Login         = () => import(/* webpackChunkName: "Login" */ '../views/Login.view');
const MyTasks       = () => import(/* webpackChunkName: "MyTasks" */ '../views/MyTasks.view');
const Work          = () => import(/* webpackChunkName: "Work" */ '../views/Work.view');

import store from "../store";

Vue.use(VueRouter)

// const ifNotAuthenticated = (to, from, next) => {
//     if (!store.getters.authentication) {
//         next()
//         return
//     }
//     next('/')
// }

const ifAuthenticated = (to, from, next) => {
    if (store.getters.authentication) {
        next()
        return
    }
    next('/login')
}

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login,
        // beforeEnter: ifNotAuthenticated,
    },
    {
        path: '/logout',
        name: 'Logout',
        component: Logout,
        beforeEnter: ifAuthenticated,
    },
    {
        path: "/",
        name: "Fields",
        component: FieldsAll,
        // beforeEnter: ifAuthenticated,
    },
    {
        path: "/lesson/:id",
        name: "LessonReader",
        component: LessonReader,
        beforeEnter: ifAuthenticated,
    },
    {
        path: "/editor/:id/lesson/:lessonId",
        name: "LessonEditor",
        component: LessonEditor
    },
    {
        path: "/mytasks",
        name: "MyTasks",
        component: MyTasks,
        beforeEnter: ifAuthenticated,
    },
    {
        path: "/work/:taskId",
        name: "Work",
        component: Work,
        beforeEnter: ifAuthenticated,
    },
    {
        path: '*',
        name: '404 Not found',
        component: Error404
    },
    
]

const router = new VueRouter({
    routes
})

export default router
