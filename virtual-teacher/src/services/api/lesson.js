import ApiUtils from './utils';
import { API_URL } from './index';
import store from "../../store";

export default {
    async getLesson(id) {
        return fetch(`${ API_URL }/lessons/${id}`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    async getLessonByField(id) {
        return fetch(`${ API_URL }/lessons/field/${id}`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    async patchLesson(data) {
        return await fetch(`${ API_URL }/lessons`, {
            method: 'PATCH',
            headers: new Headers({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        })
    },
    async postLesson(data){
        return await fetch(`${ API_URL }/lessons`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type' : 'application/json',
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        })
    },
    async deleteLesson(id){
        return fetch(`${ API_URL }/lessons/${id}`, {
            method: 'DELETE',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })
    }
}
