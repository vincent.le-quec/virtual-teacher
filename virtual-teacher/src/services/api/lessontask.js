import ApiUtils from './utils';
import { API_URL } from './index';
import store from "../../store";

export default {
    async addTask(data) {
        return await fetch(`${ API_URL }/tasks`, {
            method: 'POST',
            headers: new Headers({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        });
    },
    async updateTask(data) {
        return await fetch(`${ API_URL }/tasks`, {
            method: 'PATCH',
            headers: new Headers({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        });
    },
    async getTasks() {
        return await fetch(`${ API_URL }/tasks`, {
            method: 'GET',
            headers: new Headers({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + store.getters.authentication
            }),
        }).then(ApiUtils.extract);
    },
    async deleteTask(id){
        return fetch(`${ API_URL }/tasks/${id}`, {
            method: 'DELETE',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })
    }
}
