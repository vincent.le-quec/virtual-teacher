import ApiUtils from './utils';
import { API_URL } from './index';
import store from "@/store";

export default {
    async addWork (data) {
        return await fetch(`${API_URL}/works`, {
            method: 'POST',
            headers: new Headers({
                // "Content-Type": "multipart/form-data",
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: data
        })//.then(ApiUtils.extract);
    },
    async getWork (taskId) {
        return await fetch(`${API_URL}/works/${taskId}`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    async downloadAllWork (taskId) {
        return await fetch(`${API_URL}/works/${taskId}/download`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })//.then(ApiUtils.extract);
    },
    async downloadWork (taskId, workId) {
        return await fetch(`${API_URL}/works/${taskId}/download/${workId}`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })//.then(ApiUtils.extract);
    },
    async deleteWork (taskId) {
        return await fetch(`${API_URL}/works/${taskId}`, {
            method: 'DELETE',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })//.then(ApiUtils.extract);
    },
}