export default {
    extract: (res) => {
        return new Promise((resolve, reject) => {
            if (res.status === 200) {
                res.json().then(data => {
                    resolve(data);
                }).catch(reject);
            } else {
                reject(res);
            }
        });
    },
    timeout: (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
};
