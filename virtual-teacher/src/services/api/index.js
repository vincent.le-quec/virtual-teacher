import ApiUtils from './utils';
import Fields from './fields';
import Lesson from './lesson';
import Task from './lessontask';
import Paragraph from './paragraph';
import Work from './work';
import store from "../../store";

export const API_URL = process.env.VUE_APP_API_HTTP;

export default {
    Fields,
    Lesson,
    Task,
    Paragraph,
    Work,
    login(login, password) {
        return fetch(`${ API_URL }/user/auth`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
            }),
            body: JSON.stringify({
                login: login,
                password: password
            })
        });
    },
    getUser(autoExtract = true) {
        if (autoExtract)
            return fetch(`${ API_URL }/user/me`, {
                method: 'GET',
                headers: new Headers({
                    "Authorization": "Bearer " + store.getters.authentication
                })
            }).then(ApiUtils.extract);
        else
            return fetch(`${ API_URL }/user/me`, {
                method: 'GET',
                headers: new Headers({
                    "Authorization": "Bearer " + store.getters.authentication
                })
            });
    },
    usersGET() {
        return fetch(`${ API_URL }/users`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    teachersGET() {
        return fetch(`${ API_URL }/user/teachers`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    groupsGET() {
        return fetch(`${ API_URL }/groups`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    }
}
