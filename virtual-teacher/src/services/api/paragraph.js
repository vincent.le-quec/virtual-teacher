import { API_URL } from './index';
import store from "../../store";

export default {
    async postParagraph(data){
        return await fetch(`${ API_URL }/paragraphs`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type' : 'application/json',
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        })
    },
    async patchParagraph(data) {
        return await fetch(`${ API_URL }/paragraphs`, {
            method: 'PATCH',
            headers: new Headers({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        })
    },
    async deleteParagraph(id){
        return fetch(`${ API_URL }/paragraphs/${id}`, {
            method: 'DELETE',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        })
    }
}
