import ApiUtils from './utils';
import { API_URL } from './index';
import store from "../../store";

export default {
    async getField(id) {
        return await fetch(`${ API_URL }/fields/${ id }`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    async fieldPOST(data) {
        return await fetch(`${ API_URL }/fields`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        });
    },
    async getFields() {
        return await fetch(`${ API_URL }/fields`, {
            method: 'GET',
            headers: new Headers({
                "Authorization": "Bearer " + store.getters.authentication
            })
        }).then(ApiUtils.extract);
    },
    async deleteFields(id) {
        return fetch(`${ API_URL }/fields/${ id }`, {
            method: 'DELETE',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + store.getters.authentication
            })
        });
    },
    async fieldPATCH(data) {
        return await fetch(`${ API_URL }/fields`, {
            method: 'PATCH',
            headers: new Headers({
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + store.getters.authentication
            }),
            body: JSON.stringify(data)
        });
    }
}
