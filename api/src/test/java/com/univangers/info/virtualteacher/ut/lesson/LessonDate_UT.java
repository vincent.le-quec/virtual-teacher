package com.univangers.info.virtualteacher.ut.lesson;

import com.univangers.info.virtualteacher.core.Lesson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class LessonDate_UT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    @Test
    public void testDateBetweenBeginningAndEnding() {
        final Lesson lesson = new Lesson("title", "description", aDayBefore, aDayAfter, false, 0, null, null, null);
        Assertions.assertTrue(lesson.toDisplay());
    }

    @Test
    public void testDateBeforeBeginning() {
        final Lesson lesson = new Lesson("title", "description", aDayAfter, aDayAfter, false, 0, null, null, null);
        Assertions.assertFalse(lesson.toDisplay());
    }

    @Test
    public void testDateAfterEnding() {
        final Lesson lesson = new Lesson("title", "description", aDayBefore, aDayBefore, false, 0, null, null, null);
        Assertions.assertFalse(lesson.toDisplay());
    }

    @Test
    public void testDateHidden() {
        final Lesson lesson = new Lesson("title", "description", aDayBefore, aDayAfter, true, 0, null, null, null);
        Assertions.assertFalse(lesson.toDisplay());
    }

    @Test
    public void testDateAllNull() {
        final Lesson lesson = new Lesson("title", "description", null, null, false, 0, null, null, null);
        Assertions.assertTrue(lesson.toDisplay());
    }

    @Test
    public void testDateBeginningNullAndBeforeEnding() {
        final Lesson lesson = new Lesson("title", "description", null, aDayAfter, false, 0, null, null, null);
        Assertions.assertTrue(lesson.toDisplay());
    }

    @Test
    public void testDateBeginningNullAndAfterEnding() {
        final Lesson lesson = new Lesson("title", "description", null, aDayBefore, false, 0, null, null, null);
        Assertions.assertFalse(lesson.toDisplay());
    }

    @Test
    public void testDateEndingNullAndBeforeBeginning() {
        final Lesson lesson = new Lesson("title", "description", aDayAfter, null, false, 0, null, null, null);
        Assertions.assertFalse(lesson.toDisplay());
    }

    @Test
    public void testDateEndingNullAndAfterBeginning() {
        final Lesson lesson = new Lesson("title", "description", aDayBefore, null, false, 0, null, null, null);
        Assertions.assertTrue(lesson.toDisplay());
    }
}