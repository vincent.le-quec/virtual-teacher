package com.univangers.info.virtualteacher.ut.task;

import com.univangers.info.virtualteacher.core.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class TaskDate_UT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    @Test
    public void testDateBetweenBeginningAndEnding() {
        final Task task = new Task("title", "description", aDayBefore, aDayAfter, true, false);
        Assertions.assertTrue(task.toDisplay());
    }

    @Test
    public void testDateBeforeBeginning() {
        final Task task = new Task("title", "description", aDayAfter, aDayAfter, true, false);
        Assertions.assertFalse(task.toDisplay());
    }

    @Test
    public void testDateAfterEnding() {
        final Task task = new Task("title", "description", aDayBefore, aDayBefore, true, false);
        Assertions.assertFalse(task.toDisplay());
    }

    @Test
    public void testDateHidden() {
        final Task task = new Task("title", "description", aDayBefore, aDayAfter, false, false);
        Assertions.assertFalse(task.toDisplay());
    }

    @Test
    public void testDateAllNull() {
        final Task task = new Task("title", "description", null, null, true, false);
        Assertions.assertTrue(task.toDisplay());
    }

    @Test
    public void testDateBeginningNullAndBeforeEnding() {
        final Task task = new Task("title", "description", null, aDayAfter, true, false);
        Assertions.assertTrue(task.toDisplay());
    }

    @Test
    public void testDateBeginningNullAndAfterEnding() {
        final Task task = new Task("title", "description", null, aDayBefore, true, false);
        Assertions.assertFalse(task.toDisplay());
    }

    @Test
    public void testDateEndingNullAndBeforeBeginning() {
        final Task task = new Task("title", "description", aDayAfter, null, true, false);
        Assertions.assertFalse(task.toDisplay());
    }

    @Test
    public void testDateEndingNullAndAfterBeginning() {
        final Task task = new Task("title", "description", aDayBefore, null, true, false);
        Assertions.assertTrue(task.toDisplay());
    }
}