package com.univangers.info.virtualteacher.it.task;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskAdd_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentLogin = "m.alexis",
            adminLogin = "p.nom";

    private Lesson lessonAlgo, lessonReseaux;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person student = new Person("alexis", "migniau", studentLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        student.group = group;
        student.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        lessonAlgo = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lessonAlgo.persist();

        lessonReseaux = new Lesson("Réseaux",
                "Cours de réseaux",
                new Date(),
                new Date(),
                false,
                1,
                fieldAccess,
                null,
                null);

        lessonReseaux.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                lessonAlgo.id,
                "Ex1",
                "Faire l'ex1",
                new Date(),
                new Date(),
                true,
                true
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(201)
                .body(
                        "title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getBeginning().getTime()),
                        "displayed", is(contract.getDisplayed()),
                        "contains_work", is(contract.getContainsWork()),
                        "id_lesson", is((int) contract.getIdLesson())
                );

        // Then
        final Optional<Task> optional = Task.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                lessonAlgo.id,
                "Ex2",
                "Faire l'ex2",
                new Date(),
                new Date(),
                true,
                false
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + lessonAlgo.field.id + "'"));
    }

    @Test
    @TestSecurity(user = studentLogin, roles = "STUDENT")
    public void testAddStudent() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                lessonAlgo.id,
                "Ex3",
                "Faire l'ex3",
                new Date(),
                new Date(),
                true,
                false
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(403)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testAddAdmin() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                lessonReseaux.id,
                "Ex4",
                "Faire l'ex4",
                new Date(),
                new Date(),
                true,
                false
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(201)
                .body(
                        "title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getBeginning().getTime()),
                        "displayed", is(contract.getDisplayed()),
                        "contains_work", is(contract.getContainsWork()),
                        "id_lesson", is((int) contract.getIdLesson())
                );

        // Then
        final Optional<Task> optional = Task.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testLessonNotExist() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                999,
                "Ex3",
                "Faire l'ex3",
                new Date(),
                new Date(),
                true,
                true
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testAddOptional() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                lessonReseaux.id,
                "Ex5",
                null,
                null,
                null,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/tasks")
                .then()
                .statusCode(201)
                .body(
                        "title", is(contract.getTitle()),
                        "id_lesson", is((int) contract.getIdLesson())
                );

        // Then
        final Optional<Task> optional = Task.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }
}
