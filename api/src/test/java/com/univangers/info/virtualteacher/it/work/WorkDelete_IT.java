package com.univangers.info.virtualteacher.it.work;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WorkDelete_IT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    private long t1Id, t2Id, w1Id, w2Id, w3Id, w4Id, w5Id, f1Id;
    private String w1Filename, w2Filename, w3Filename, w4Filename, w5Filename;

    @BeforeAll
    @Transactional
    public void before() {
        Person admin = TestUtils.createPerson("admin_name", "admin_first_name", "admin", "admin_email", TypePerson.ADMIN);
        admin.persist();
        Person teacherWithAccess = TestUtils.createPerson("teacherWithAccess_name", "teacherWithAccess_first_name", "teacher1", "teacherWithAccess_email", TypePerson.TEACHER);
        teacherWithAccess.persist();
        Person teacherWithoutAccess = TestUtils.createPerson("teacherWithoutAccess_name", "teacherWithoutAccess_first_name", "teacher2", "teacherWithoutAccess_email", TypePerson.TEACHER);
        teacherWithoutAccess.persist();
        Person teacherOther = TestUtils.createPerson("teacherOther_name", "teacherOther_first_name", "teacher3", "teacherOther_email", TypePerson.TEACHER);
        teacherOther.persist();
        Person studentWithAccess = TestUtils.createPerson("studentWithAccess_name", "studentWithAccess_first_name", "student1", "studentWithAccess_email", TypePerson.STUDENT);
        studentWithAccess.persist();
        Person studentWithoutAccess = TestUtils.createPerson("studentWithoutAccess_name", "studentWithoutAccess_first_name", "student2", "studentWithoutAccess_email", TypePerson.STUDENT);
        studentWithoutAccess.persist();
        Person studentOther = TestUtils.createPerson("studentOther_name", "studentOther_first_name", "student3", "studentOther_email", TypePerson.STUDENT);
        studentOther.persist();

        Group g1 = new Group("g1");
        g1.persist();
        Group g2 = new Group("g2");
        g2.persist();

        studentWithAccess.group = g1;
        studentOther.group = g1;
        studentWithoutAccess.group = g2;

        Field f = new Field("fieldName", "description", "icon");
        f.persist();
        f1Id = f.id;
        f.teachers = Arrays.asList(new FieldPerson(f, teacherWithAccess), new FieldPerson(f, teacherOther));
        f.groups = Collections.singletonList(new FieldGroup(f, g1));

        Lesson l = new Lesson("title", "description", aDayBefore, aDayAfter, false, 0, f);
        l.persist();

        Task t1 = new Task("t1", "description", aDayBefore, aDayAfter, true, true, l);
        t1.persist();
        Task t2 = new Task("t2", "description", aDayBefore, aDayAfter, true, true, l);
        t2.persist();

        Work w1 = TestUtils.createWork(studentWithAccess, t1);
        Work w2 = TestUtils.createWork(studentWithAccess, t2);
        Work w3 = TestUtils.createWork(studentOther, t1);
        Work w4 = TestUtils.createWork(studentOther, t2);
        Work w5 = TestUtils.createWork(teacherWithAccess, t1);

        t1Id = t1.id;
        t2Id = t2.id;

        w1Id = w1.id;
        w2Id = w2.id;
        w3Id = w3.id;
        w4Id = w4.id;
        w5Id = w5.id;

        w1Filename = w1.getWorkPath();
        w2Filename = w2.getWorkPath();
        w3Filename = w3.getWorkPath();
        w4Filename = w4.getWorkPath();
        w5Filename = w5.getWorkPath();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testDeleteAdmin() {
        given()
                .when()
                .delete("/works/" + t1Id)
                .then()
                .statusCode(404)
                .body(is("[admin] delete : No work found for this user on task with id '" + t1Id + "'"));
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testDeleteWorkNotExisting() {
        given()
                .when()
                .delete("/works/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testDeleteTeacherWithAccess() {
        given()
                .when()
                .delete("/works/" + t1Id)
                .then()
                .statusCode(200);

        Optional<Work> optional = Work.findByIdOptional(w5Id);
        Assertions.assertTrue(optional.isEmpty());
        Assertions.assertFalse(new File(w5Filename).exists());
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testDeleteTeacherWithoutAccess() {
        given()
                .when()
                .delete("/works/" + t1Id)
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + f1Id + "'"));

        Optional<Work> optional = Work.findByIdOptional(w3Id);
        Assertions.assertTrue(optional.isPresent());
        Assertions.assertTrue(new File(w3Filename).exists());
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testDeleteStudent() {
        given()
                .when()
                .delete("/works/" + t2Id)
                .then()
                .statusCode(200);

        Optional<Work> optional = Work.findByIdOptional(w2Id);
        Assertions.assertTrue(optional.isEmpty());
        Assertions.assertFalse(new File(w2Filename).exists());
    }
}
