package com.univangers.info.virtualteacher.ut.field;

import com.univangers.info.virtualteacher.contract.field.Field;
import com.univangers.info.virtualteacher.core.FieldPerson;
import com.univangers.info.virtualteacher.provider.mapper.field.FieldMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FieldMapper_UT {

    @Test
    public void testFromContract() {
        final Field contract = new Field("title", "some description", "nom-icon", Collections.emptyList());
        final com.univangers.info.virtualteacher.core.Field core = FieldMapper.fromContract(contract);

        Assertions.assertEquals(contract.getFieldName(), core.fieldName);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getIcon(), core.icon);
        Assertions.assertEquals(contract.getTeachers(), Collections.emptyList());
    }

    @Test
    public void testFromContractNull() {
        final Field contract = new Field("title", null, null, null);
        final com.univangers.info.virtualteacher.core.Field core = FieldMapper.fromContract(contract);

        Assertions.assertEquals(contract.getFieldName(), core.fieldName);
        Assertions.assertNull(core.description);
        Assertions.assertNull(core.icon);
        Assertions.assertEquals(Collections.emptyList(), core.teachers);
    }

    @Test
    public void testUpdateFromContract() {
        List<com.univangers.info.virtualteacher.core.FieldPerson> teachers = new ArrayList<>();
        Field contract = new Field("title", "some description", "nom-icon", Collections.emptyList());

        final com.univangers.info.virtualteacher.core.Field core1 = new com.univangers.info.virtualteacher.core.Field("title", "some description", "nom-icon", teachers, Collections.emptyList(), Collections.emptyList());
        com.univangers.info.virtualteacher.core.Field core2 = FieldMapper.fromContract(contract, core1);

        Assertions.assertEquals(contract.getFieldName(), core2.fieldName);
        Assertions.assertEquals(contract.getDescription(), core2.description);
        Assertions.assertEquals(contract.getIcon(), core2.icon);
        Assertions.assertEquals(contract.getTeachers().size(), core2.teachers.size());
        for (String s : contract.getTeachers().stream().map(com.univangers.info.virtualteacher.contract.Person::getEmail).collect(Collectors.toList())) {
            Assertions.assertTrue(core2.teachers.contains(s));
        }
        contract = new Field("title2", "some description2", "nom-icon2", Collections.emptyList());
        core2 = FieldMapper.fromContract(contract, core1);

        Assertions.assertEquals(contract.getFieldName(), core2.fieldName);
        Assertions.assertEquals(contract.getDescription(), core2.description);
        Assertions.assertEquals(contract.getIcon(), core2.icon);
        Assertions.assertEquals(contract.getTeachers().size(), core2.teachers.size());
        for (String s : contract.getTeachers().stream().map(com.univangers.info.virtualteacher.contract.Person::getEmail).collect(Collectors.toList())) {
            Assertions.assertTrue(core2.teachers.contains(s));
        }
    }

    @Test
    public void testToContract() {
        List<FieldPerson> teachers = new ArrayList<>();
        final com.univangers.info.virtualteacher.core.Field core = new com.univangers.info.virtualteacher.core.Field("title", "some description", "nom-icon", teachers, Collections.emptyList(), Collections.emptyList());
        final Field contract = FieldMapper.toContract(core);

        Assertions.assertEquals(core.fieldName, contract.getFieldName());
        Assertions.assertEquals(core.description, contract.getDescription());
        Assertions.assertEquals(core.icon, contract.getIcon());
        Assertions.assertEquals(core.teachers, teachers);
    }

}
