package com.univangers.info.virtualteacher.it.field;

import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FieldGet_IT {

    int field1, field2;

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
        field1 = (int) Field.findByFieldNameOptional("field1_name").get().id;
        field2 = (int) Field.findByFieldNameOptional("field2_name").get().id;
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testGetAllTFieldForAdmin() {
        given()
                .when()
                .get("/fields")
                .then()
                .statusCode(200)
                .body("size()", is(3),
                        "[0].field_name", is("field1_name"),
                        "[0].description", is("field1_description"),
                        "[0].icon", is("field1_icon"),
                        "[0].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[0].teachers.size()", is(2),

                        "[0].teachers[0].first_name", is("teacherOther_first_name"),
                        "[0].teachers[0].name", is("teacherOther_name"),
                        "[0].teachers[0].email", is("teacherOther_email"),
                        "[0].teachers[0].type", is("TEACHER"),

                        "[0].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[0].teachers[1].name", is("teacherWithAccess_name"),
                        "[0].teachers[1].email", is("teacherWithAccess_email"),
                        "[0].teachers[1].type", is("TEACHER"),

                        "[1].field_name", is("field2_name"),
                        "[1].description", is("field2_description"),
                        "[1].icon", is("field2_icon"),
                        "[1].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[1].teachers.size()", is(2),

                        "[1].teachers[0].first_name", is("teacherOther_first_name"),
                        "[1].teachers[0].name", is("teacherOther_name"),
                        "[1].teachers[0].email", is("teacherOther_email"),
                        "[1].teachers[0].type", is("TEACHER"),

                        "[1].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[1].teachers[1].name", is("teacherWithAccess_name"),
                        "[1].teachers[1].email", is("teacherWithAccess_email"),
                        "[1].teachers[1].type", is("TEACHER"),

                        "[2].field_name", is("field3_name"),
                        "[2].description", is("field3_description"),
                        "[2].icon", is("field3_icon"),
                        "[2].groups", is(Collections.emptyList()),
                        "[2].teachers", is(Collections.emptyList())
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetAllTFieldForTeacher() {
        given()
                .when()
                .get("/fields")
                .then()
                .statusCode(200)
                .body("size()", is(2),
                        "[0].field_name", is("field1_name"),
                        "[0].description", is("field1_description"),
                        "[0].icon", is("field1_icon"),
                        "[0].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[0].teachers.size()", is(2),

                        "[0].teachers[0].first_name", is("teacherOther_first_name"),
                        "[0].teachers[0].name", is("teacherOther_name"),
                        "[0].teachers[0].email", is("teacherOther_email"),
                        "[0].teachers[0].type", is("TEACHER"),

                        "[0].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[0].teachers[1].name", is("teacherWithAccess_name"),
                        "[0].teachers[1].email", is("teacherWithAccess_email"),
                        "[0].teachers[1].type", is("TEACHER"),

                        "[1].field_name", is("field2_name"),
                        "[1].description", is("field2_description"),
                        "[1].icon", is("field2_icon"),
                        "[1].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[1].teachers.size()", is(2),

                        "[1].teachers[0].first_name", is("teacherOther_first_name"),
                        "[1].teachers[0].name", is("teacherOther_name"),
                        "[1].teachers[0].email", is("teacherOther_email"),
                        "[1].teachers[0].type", is("TEACHER"),

                        "[1].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[1].teachers[1].name", is("teacherWithAccess_name"),
                        "[1].teachers[1].email", is("teacherWithAccess_email"),
                        "[1].teachers[1].type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testGetAllTFieldForTeacherWithNoField() {
        given()
                .when()
                .get("/fields")
                .then()
                .statusCode(200)
                .body("size()", is(0));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetAllTFieldForStudent() {
        given()
                .when()
                .get("/fields")
                .then()
                .statusCode(200)
                .body("size()", is(2),
                        "[0].field_name", is("field1_name"),
                        "[0].description", is("field1_description"),
                        "[0].icon", is("field1_icon"),
                        "[0].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[0].teachers.size()", is(2),

                        "[0].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[0].teachers[1].name", is("teacherWithAccess_name"),
                        "[0].teachers[1].email", is("teacherWithAccess_email"),
                        "[0].teachers[1].type", is("TEACHER"),

                        "[0].teachers[0].first_name", is("teacherOther_first_name"),
                        "[0].teachers[0].name", is("teacherOther_name"),
                        "[0].teachers[0].email", is("teacherOther_email"),
                        "[0].teachers[0].type", is("TEACHER"),

                        "[1].field_name", is("field2_name"),
                        "[1].description", is("field2_description"),
                        "[1].icon", is("field2_icon"),
                        "[1].groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "[1].teachers.size()", is(2),

                        "[1].teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "[1].teachers[1].name", is("teacherWithAccess_name"),
                        "[1].teachers[1].email", is("teacherWithAccess_email"),
                        "[1].teachers[1].type", is("TEACHER"),

                        "[1].teachers[0].first_name", is("teacherOther_first_name"),
                        "[1].teachers[0].name", is("teacherOther_name"),
                        "[1].teachers[0].email", is("teacherOther_email"),
                        "[1].teachers[0].type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetFieldForTeacherWithAccess() {
        given()
                .when()
                .get("/fields/" + field1)
                .then()
                .statusCode(200)
                .body("id", is(field1),
                        "field_name", is("field1_name"),
                        "description", is("field1_description"),
                        "icon", is("field1_icon"),
                        "groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "teachers.size()", is(2),

                        "teachers[0].first_name", is("teacherOther_first_name"),
                        "teachers[0].name", is("teacherOther_name"),
                        "teachers[0].email", is("teacherOther_email"),
                        "teachers[0].type", is("TEACHER"),

                        "teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "teachers[1].name", is("teacherWithAccess_name"),
                        "teachers[1].email", is("teacherWithAccess_email"),
                        "teachers[1].type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testGetFieldForTeacherWithoutAccess() {
        given()
                .when()
                .get("/fields/" + field1)
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + field1 + "'"));

    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetFieldByStudentWithAccess() {
        given()
                .when()
                .get("/fields/" + field2)
                .then()
                .statusCode(200)
                .body("id", is(field2),
                        "field_name", is("field2_name"),
                        "description", is("field2_description"),
                        "icon", is("field2_icon"),
                        "groups", containsInAnyOrder("L3INFO", "M1INFO"),
                        "teachers.size()", is(2),

                        "teachers[0].first_name", is("teacherOther_first_name"),
                        "teachers[0].name", is("teacherOther_name"),
                        "teachers[0].email", is("teacherOther_email"),
                        "teachers[0].type", is("TEACHER"),

                        "teachers[1].first_name", is("teacherWithAccess_first_name"),
                        "teachers[1].name", is("teacherWithAccess_name"),
                        "teachers[1].email", is("teacherWithAccess_email"),
                        "teachers[1].type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "student2", roles = "STUDENT")
    public void testGetFieldForStudentWithoutAccess() {
        given()
                .when()
                .get("/fields/" + field1)
                .then()
                .statusCode(403)
                .body(is("[student2] verifyHaveAccessToField : access refused to field with id '" + field1 + "'"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetFieldNotExist() {
        given()
                .when()
                .get("/fields/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown field with id '999'"));
    }
}