package com.univangers.info.virtualteacher.it.user;

import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.it.TestUtils;
import com.univangers.info.virtualteacher.provider.UserService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserFactory_IT {

    @Inject
    UserService userService;

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @Transactional
    void testFromLdapInfoToStudent() throws IOException {
        String ldapData = "# extended LDIF\n" +
                "#\n" +
                "# LDAPv3\n" +
                "# base <dc=univ-angers,dc=fr> with scope subtree\n" +
                "# filter: uid=tbouriaud\n" +
                "# requesting: ALL\n" +
                "#\n" +
                "\n" +
                "# tbouriaud, people, univ-angers.fr\n" +
                "dn: uid=tbouriaud,ou=people,dc=univ-angers,dc=fr\n" +
                "mail: thomas.bouriaud@etud.univ-angers.fr\n" +
                "uid: student\n" +
                "sn: Bouriaud\n" +
                "auaStatut: etu\n" +
                "givenName: Thomas\n" +
                "auaEtapeMillesime: 2018TL3INF1\n" +
                "auaEtapeMillesime: 2019TM1INF1\n" +
                "auaEtapeMillesime: 2020TM2ACD1\n" +
                "\n" +
                "# search result\n" +
                "search: 2\n" +
                "result: 0 Success\n" +
                "\n" +
                "# numResponses: 2\n" +
                "# numEntries: 1";

        Reader inputString = new StringReader(ldapData);
        BufferedReader reader = new BufferedReader(inputString);

        final Person person = userService.extractData(reader);

        Assertions.assertEquals("Bouriaud", person.name);
        Assertions.assertEquals("Thomas", person.firstName);
        Assertions.assertEquals("student", person.login);
        Assertions.assertEquals("thomas.bouriaud@etud.univ-angers.fr", person.email);
        Assertions.assertEquals(TypePerson.STUDENT, person.typePerson);
        Assertions.assertEquals("2020TM2ACD1", person.group.title);
    }

    @Test
    @Transactional
    void testFromLdapInfoToTeacher() throws IOException {
        String ldapData = "# extended LDIF\n" +
                "#\n" +
                "# LDAPv3\n" +
                "# base <dc=univ-angers,dc=fr> with scope subtree\n" +
                "# filter: uid=tbouriaud\n" +
                "# requesting: ALL\n" +
                "#\n" +
                "\n" +
                "# tbouriaud, people, univ-angers.fr\n" +
                "dn: uid=tbouriaud,ou=people,dc=univ-angers,dc=fr\n" +
                "mail: thomas.bouriaud@etud.univ-angers.fr\n" +
                "uid: teacher\n" +
                "sn: Bouriaud\n" +
                "auaStatut: perso\n" +
                "givenName: Thomas\n" +
                "auaEtapeMillesime: 2018TL3INF1\n" +
                "auaEtapeMillesime: 2019TM1INF1\n" +
                "auaEtapeMillesime: 2020TM2ACD1\n" +
                "\n" +
                "# search result\n" +
                "search: 2\n" +
                "result: 0 Success\n" +
                "\n" +
                "# numResponses: 2\n" +
                "# numEntries: 1";

        Reader inputString = new StringReader(ldapData);
        BufferedReader reader = new BufferedReader(inputString);

        final Person person = userService.extractData(reader);

        Assertions.assertEquals("Bouriaud", person.name);
        Assertions.assertEquals("Thomas", person.firstName);
        Assertions.assertEquals("teacher", person.login);
        Assertions.assertEquals("thomas.bouriaud@etud.univ-angers.fr", person.email);
        Assertions.assertEquals(TypePerson.TEACHER, person.typePerson);
        Assertions.assertEquals("2020TM2ACD1", person.group.title);
    }

    @Test
    @Transactional
    void testFromLdapInfoToAdmin() throws IOException {
        String ldapData = "# extended LDIF\n" +
                "#\n" +
                "# LDAPv3\n" +
                "# base <dc=univ-angers,dc=fr> with scope subtree\n" +
                "# filter: uid=tbouriaud\n" +
                "# requesting: ALL\n" +
                "#\n" +
                "\n" +
                "# tbouriaud, people, univ-angers.fr\n" +
                "dn: uid=tbouriaud,ou=people,dc=univ-angers,dc=fr\n" +
                "mail: thomas.bouriaud@etud.univ-angers.fr\n" +
                "uid: admin\n" +
                "sn: Bouriaud\n" +
                "auaStatut: etu\n" +
                "givenName: Thomas\n" +
                "auaEtapeMillesime: 2018TL3INF1\n" +
                "auaEtapeMillesime: 2019TM1INF1\n" +
                "auaEtapeMillesime: 2020TM2ACD1\n" +
                "\n" +
                "# search result\n" +
                "search: 2\n" +
                "result: 0 Success\n" +
                "\n" +
                "# numResponses: 2\n" +
                "# numEntries: 1";

        Reader inputString = new StringReader(ldapData);
        BufferedReader reader = new BufferedReader(inputString);

        final Person person = userService.extractData(reader);

        Assertions.assertEquals("Bouriaud", person.name);
        Assertions.assertEquals("Thomas", person.firstName);
        Assertions.assertEquals("admin", person.login);
        Assertions.assertEquals("thomas.bouriaud@etud.univ-angers.fr", person.email);
        Assertions.assertEquals(TypePerson.ADMIN, person.typePerson);
        Assertions.assertEquals("2020TM2ACD1", person.group.title);
    }
}
