package com.univangers.info.virtualteacher.it.group;

import com.univangers.info.virtualteacher.core.Group;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GroupGet_IT {
    private Group g1, g2, g3;
    private Person student, teacher;

    @BeforeAll
    @Transactional
    public void before() {
        // Create data
        g1 = new Group("L1");
        g1.persist();
        g2 = new Group("L2");
        g2.persist();
        g3 = new Group("L3");
        g3.persist();

        student = new Person("student", "student", "student", "student", TypePerson.STUDENT);
        student.persist();

        teacher = new Person("teacher", "teacher", "teacher", "teacher", TypePerson.TEACHER);
        teacher.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        Group.deleteAll();
    }

    @Test
    @TestSecurity(user = "teacher", roles = "TEACHER")
    public void testGetTeacher() {
        given()
                .when()
                .get("/groups")
                .then()
                .statusCode(200)
                .body("size()", is(3),
                        "[0]", is(g1.title),
                        "[1]", is(g2.title),
                        "[2]", is(g3.title));
    }

    @Test
    @TestSecurity(user = "student", roles = "STUDENT")
    public void testGetStudent() {
        given()
                .when()
                .get("/groups")
                .then()
                .statusCode(403)
                .body(is(""));
    }
}
