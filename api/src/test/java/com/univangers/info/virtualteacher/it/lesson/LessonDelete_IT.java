package com.univangers.info.virtualteacher.it.lesson;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LessonDelete_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentLogin = "m.alexis",
            adminLogin = "p.nom";
    private Field fieldAccess;
    private Lesson lessonAlgo, lessonReseaux;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create data
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person student = new Person("alexis", "migniau", studentLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        student.group = group;
        student.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        lessonAlgo = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lessonAlgo.persist();

        lessonReseaux = new Lesson("Réseaux",
                "Cours de réseaux",
                new Date(),
                new Date(),
                false,
                1,
                fieldAccess,
                null,
                null);

        lessonReseaux.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testDeleteTeacherWithAccess() {
        given()
                .when()
                .delete("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(200);

        Optional<Lesson> optional = Lesson.findByTitleOptional(lessonAlgo.title);
        Assertions.assertTrue(optional.isEmpty());
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testDeleteTeacherWithoutAccess() {
        given()
                .when()
                .delete("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + lessonAlgo.field.id + "'"));

        Optional<Lesson> optional = Lesson.findByTitleOptional(lessonReseaux.title);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    @TestSecurity(user = studentLogin, roles = "STUDENT")
    public void testDeleteStudent() {
        given()
                .when()
                .delete("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(403)
                .body(is(""));

        Optional<Lesson> optional = Lesson.findByTitleOptional(lessonAlgo.title);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testDeleteLessonNotExist() {
        given()
                .when()
                .delete("/lessons/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testDeleteAdmin() {
        given()
                .when()
                .delete("/lessons/" + lessonReseaux.id)
                .then()
                .statusCode(200);

        Optional<Lesson> optional = Lesson.findByTitleOptional(lessonReseaux.title);
        Assertions.assertTrue(optional.isEmpty());
    }
}
