package com.univangers.info.virtualteacher.ut.paragraph;

import com.univangers.info.virtualteacher.contract.paragraph.Paragraph;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.TypeFile;
import com.univangers.info.virtualteacher.provider.mapper.paragraph.ParagraphMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParagraphMapper_UT {

    @Test
    public void testFromContract() {
        // Create contract
        final Paragraph contract = new Paragraph("Titre", 5, "Content", 1, "TXT", 1L);

        // Transform contract in core
        final com.univangers.info.virtualteacher.core.Paragraph core = ParagraphMapper.fromContract(contract);

        // Assert
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(TypeFile.valueOf(contract.getType()), core.type);
    }

    @Test
    public void testFromContractNull() {
        // Create contract
        final Paragraph contract = new Paragraph("Titre", null, null, null, null, null);

        // Transform contract in core
        final com.univangers.info.virtualteacher.core.Paragraph core = ParagraphMapper.fromContract(contract);

        // Assert
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(0, core.titleSize);
        Assertions.assertNull(core.content);
        Assertions.assertEquals(0, core.order);
        Assertions.assertEquals(TypeFile.TXT, core.type);
    }

    @Test
    public void testUpdateFromContract() {
        // Create contract
        final Paragraph contract = new Paragraph("Titre", 5, "Content", 1, "TXT", 1L);

        // Transform contract in core
        final com.univangers.info.virtualteacher.core.Paragraph core1 = new com.univangers.info.virtualteacher.core.Paragraph(
                "Titre updated",
                5,
                "Content updated",
                2,
                TypeFile.TXT);

        // Update contract from core1
        final com.univangers.info.virtualteacher.core.Paragraph core2 = ParagraphMapper.fromContract(contract, core1);

        // Assert
        Assertions.assertEquals(contract.getTitle(), core2.title);
        Assertions.assertEquals(contract.getTitleSize(), core2.titleSize);
        Assertions.assertEquals(contract.getContent(), core2.content);
        Assertions.assertEquals(contract.getOrder(), core2.order);
        Assertions.assertEquals(TypeFile.valueOf(contract.getType()), core2.type);
    }

    @Test
    public void testToContract() {
        // Create data
        final Lesson lesson = new Lesson();

        final com.univangers.info.virtualteacher.core.Paragraph core = new com.univangers.info.virtualteacher.core.Paragraph(
                "Titre updated",
                5,
                "Content updated",
                2,
                TypeFile.TXT
        );

        core.lesson = lesson;

        final Paragraph contract = ParagraphMapper.toContract(core);

        // Assert
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(TypeFile.valueOf(contract.getType()), core.type);
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }
}
