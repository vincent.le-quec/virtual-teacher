package com.univangers.info.virtualteacher.ut.task;

import com.univangers.info.virtualteacher.contract.task.Task;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.provider.mapper.task.TaskMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class TaskMapper_UT {

    @Test
    public void testFromContract() {
        // Create contract
        final Task contract = new Task(1L, "Title", "Description", new Date(), new Date(), true, true);

        // transform contract to core
        final com.univangers.info.virtualteacher.core.Task core = TaskMapper.fromContract(contract);

        //Assert
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getContainsWork(), core.containsWork);
    }

    @Test
    public void testFromContractNull() {
        // Create contract
        final Task contract = new Task(1L, "Title", null, null, null, null, null);

        // transform contract to core
        final com.univangers.info.virtualteacher.core.Task core = TaskMapper.fromContract(contract);

        //Assert
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertNull(core.description);
        Assertions.assertNull(core.beginning);
        Assertions.assertNull(core.ending);
        Assertions.assertTrue(core.displayed);
        Assertions.assertFalse(core.containsWork);
    }

    @Test
    public void testUpdateFromContract() {
        // Create contract
        final Task contract = new Task(1L, "Title", "Description", new Date(), new Date(), true, false);

        // Create core
        final com.univangers.info.virtualteacher.core.Task core1 = new com.univangers.info.virtualteacher.core.Task(
                "Title update",
                "Description updated",
                new Date(),
                new Date(),
                false,
                true,
                null
        );

        // Update contract from core1
        com.univangers.info.virtualteacher.core.Task core2 = TaskMapper.fromContract(contract, core1);

        // Assert if the values of contract are updated
        Assertions.assertEquals(core2.title, core1.title);
        Assertions.assertEquals(core2.description, core1.description);
        Assertions.assertEquals(core2.beginning, contract.getBeginning());
        Assertions.assertEquals(core2.ending, contract.getEnding());
        Assertions.assertEquals(core2.displayed, core1.displayed);
        Assertions.assertEquals(contract.getContainsWork(), contract.getContainsWork());
    }

    @Test
    public void testToContract() {
        // Create data
        final Lesson lesson = new Lesson();

        final com.univangers.info.virtualteacher.core.Task core = new com.univangers.info.virtualteacher.core.Task(
                "Titre",
                "Description",
                new Date(),
                new Date(),
                true,
                false,
                lesson
        );

        // Transform core to contract
        final Task contract = TaskMapper.toContract(core);

        //Assert if the values of contract equals values of core
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getContainsWork(), core.containsWork);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }
}
