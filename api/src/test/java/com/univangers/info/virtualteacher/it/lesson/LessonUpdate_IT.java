package com.univangers.info.virtualteacher.it.lesson;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LessonUpdate_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentLogin = "m.alexis",
            adminLogin = "p.nom";
    private Field fieldAccess;
    private Lesson lessonAlgo, lessonReseaux;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create data
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person student = new Person("alexis", "migniau", studentLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        student.group = group;
        student.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        lessonAlgo = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                1,
                fieldAccess,
                null,
                null);

        lessonAlgo.persist();

        lessonReseaux = new Lesson("Réseaux",
                "Cours de réseaux",
                new Date(),
                new Date(),
                false,
                1,
                fieldAccess,
                null,
                null);

        lessonReseaux.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "try to change the title",
                lessonReseaux.description,
                lessonReseaux.beginning,
                lessonReseaux.beginning,
                lessonReseaux.hide,
                2,
                lessonReseaux.field.id,
                null,
                null
        );

        contract.setId(lessonReseaux.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(403);

        // THEN
        final Optional<Lesson> optional = Lesson.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(lessonReseaux.title, core.title);
        Assertions.assertEquals(lessonReseaux.description, core.description);
        Assertions.assertEquals(lessonReseaux.beginning, core.beginning);
        Assertions.assertEquals(lessonReseaux.ending, core.ending);
        Assertions.assertEquals(lessonReseaux.hide, core.hide);
        Assertions.assertEquals(lessonReseaux.field.id, core.field.id);
        Assertions.assertEquals(lessonReseaux.order, core.order);
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "title Updated",
                lessonAlgo.description,
                lessonAlgo.beginning,
                lessonAlgo.ending,
                lessonAlgo.hide,
                2,
                lessonAlgo.field.id,
                null,
                null
        );

        contract.setId(lessonAlgo.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(200)
                .body("title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "hide", is(contract.isHide()),
                        "field_id", is(contract.getFieldId().intValue())
                );

        // THEN
        final Optional<Lesson> optional = Lesson.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.isHide(), core.hide);
        Assertions.assertEquals(contract.getFieldId(), core.field.id);
        Assertions.assertEquals(contract.getOrder(), core.order);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testUpdateAdmin() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                lessonAlgo.title,
                "description updated",
                lessonAlgo.beginning,
                lessonAlgo.ending,
                lessonAlgo.hide,
                1,
                lessonAlgo.field.id,
                null,
                null
        );

        contract.setId(lessonAlgo.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(200)
                .body("title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "hide", is(contract.isHide()),
                        "field_id", is(contract.getFieldId().intValue())
                );

        // THEN
        final Optional<Lesson> optional = Lesson.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.isHide(), core.hide);
        Assertions.assertEquals(contract.getFieldId(), core.field.id);
        Assertions.assertEquals(contract.getOrder(), core.order);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testUpdateLessonNotExist() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                lessonAlgo.title,
                lessonAlgo.description,
                lessonAlgo.beginning,
                lessonAlgo.ending,
                lessonAlgo.hide,
                2,
                lessonAlgo.field.id,
                null,
                null
        );

        contract.setId(999);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testUpdateFieldNotExist() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "title Updated",
                lessonAlgo.description,
                lessonAlgo.beginning,
                lessonAlgo.ending,
                lessonAlgo.hide,
                2,
                (long) 999,
                null,
                null
        );

        contract.setId(999);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }

    @Test
    @TestSecurity(user = studentLogin, roles = "STUDENT")
    public void testUpdateStudent() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "title Updated",
                lessonAlgo.description,
                lessonAlgo.beginning,
                lessonAlgo.ending,
                lessonAlgo.hide,
                2,
                lessonAlgo.field.id,
                null,
                null
        );

        contract.setId(lessonAlgo.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/lessons")
                .then()
                .statusCode(403)
                .body(is(""));

        // THEN
        final Optional<Lesson> optional = Lesson.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(lessonAlgo.title, core.title);
        Assertions.assertEquals(lessonAlgo.description, core.description);
        Assertions.assertEquals(lessonAlgo.beginning, core.beginning);
        Assertions.assertEquals(lessonAlgo.ending, core.ending);
        Assertions.assertEquals(lessonAlgo.hide, core.hide);
        Assertions.assertEquals(lessonAlgo.field.id, core.field.id);
        Assertions.assertEquals(lessonAlgo.order, core.order);
    }
}
