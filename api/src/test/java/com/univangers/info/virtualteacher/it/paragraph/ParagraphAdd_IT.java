package com.univangers.info.virtualteacher.it.paragraph;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ParagraphAdd_IT {
    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentLogin = "m.alexis",
            adminLogin = "p.nom";

    private Lesson lessonAlgo;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person student = new Person("alexis", "migniau", studentLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        student.group = group;
        student.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        lessonAlgo = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lessonAlgo.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Paragraph 1",
                10,
                "Content",
                1,
                "TXT",
                lessonAlgo.id
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/paragraphs")
                .then()
                .statusCode(201)
                .body(
                        "title", is(contract.getTitle()),
                        "title_size", is(contract.getTitleSize()),
                        "content", is(contract.getContent()),
                        "order", is(contract.getOrder()),
                        "type", is(contract.getType()),
                        "lesson_id", is(contract.getLessonId().intValue())
                );

        // Then
        final Optional<Paragraph> optional = Paragraph.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Paragraph 1",
                10,
                "Content",
                1,
                "TXT",
                lessonAlgo.id
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/paragraphs")
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + lessonAlgo.field.id + "'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testAddAdmin() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Paragraph 2",
                10,
                "Content",
                1,
                "TXT",
                lessonAlgo.id
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/paragraphs")
                .then()
                .statusCode(201)
                .body(
                        "title", is(contract.getTitle()),
                        "title_size", is(contract.getTitleSize()),
                        "content", is(contract.getContent()),
                        "order", is(contract.getOrder()),
                        "type", is(contract.getType()),
                        "lesson_id", is(contract.getLessonId().intValue())
                );

        // Then
        final Optional<Paragraph> optional = Paragraph.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = studentLogin, roles = "STUDENT")
    public void testAddStudent() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Paragraph 3",
                10,
                "Content",
                1,
                "TXT",
                lessonAlgo.id
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/paragraphs")
                .then()
                .statusCode(403)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testAddLessonNotExist() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Paragraph 4",
                10,
                "Content",
                1,
                "TXT",
                999L
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/paragraphs")
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }
}
