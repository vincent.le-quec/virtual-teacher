package com.univangers.info.virtualteacher.ut;

import com.univangers.info.virtualteacher.contract.Person;
import com.univangers.info.virtualteacher.core.Group;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PersonMapper_UT {

    @Test
    public void testToContract() {
        final com.univangers.info.virtualteacher.core.Person core = new com.univangers.info.virtualteacher.core.Person("name", "firstName", "login", "email", TypePerson.STUDENT);
        core.group = new Group("title");
        final Person contract = PersonMapper.toContract(core);

        Assertions.assertEquals(contract.getEmail(), core.email);
        Assertions.assertEquals(contract.getName(), core.name);
        Assertions.assertEquals(contract.getFirstName(), core.firstName);
        Assertions.assertEquals(contract.getTypePerson(), core.typePerson);
        Assertions.assertEquals(contract.getGroup(), core.group.title);
    }
}