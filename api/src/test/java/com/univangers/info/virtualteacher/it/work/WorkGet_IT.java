package com.univangers.info.virtualteacher.it.work;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WorkGet_IT {

    private Person teacher, student, otherStudent, student4, student5;
    private Field field1;
    private Lesson hiddenLesson;
    private Task task1, hiddenTask;
    private Work teacherWork;

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
        student = Person.findByLogin("student1");
        otherStudent = Person.findByLogin("student3");
        student4 = Person.findByLogin("student4");
        student5 = Person.findByLogin("student5");
        field1 = Field.findByFieldNameOptional("field1_name").get();
        task1 = Lesson.findByTitleOptional("cours1")
                .get()
                .tasks
                .stream()
                .filter(e -> "t1".equals(e.title))
                .findFirst()
                .orElseThrow(NullPointerException::new);
        hiddenTask = Lesson.findByTitleOptional("cours1")
                .get()
                .tasks
                .stream()
                .filter(e -> "hiddenTask".equals(e.title))
                .findFirst()
                .orElseThrow(NullPointerException::new);
        hiddenLesson = Lesson.findByTitleOptional("hidden").get();

        teacher = Person.findByLogin("teacher1");
        Date fixedDate;
        try {
            fixedDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2001");
            teacherWork = new Work("teacherFile.txt", fixedDate, teacher, task1);
            teacherWork.persist();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        File dir = new File(task1.getWorkFolder());
        dir.mkdirs();
        File tmp = new File(teacherWork.getWorkPath());
        try {
            tmp.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testGetWorksByAdmin() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(200)
                .body("size()", is(5),

                        "[0].task_id", is((int) task1.id),
                        "[0].file_name", is(teacherWork.fileName),
                        "[0].rendering_date", equalTo(978303600000L),
                        "[0].user.login", is(teacher.login),
                        "[0].user.name", is(teacher.name),
                        "[0].user.first_name", is(teacher.firstName),
                        "[0].user.email", is(teacher.email),
                        "[0].user.group", equalTo(null),
                        "[0].user.type", is(teacher.typePerson.toString()),

                        "[1].task_id", is((int) task1.id),
                        "[1].file_name", is("file1_" + student4.login + "_" + task1.id + ".txt"),
                        "[1].rendering_date", equalTo(978303600000L),
                        "[1].user.login", is(student4.login),
                        "[1].user.name", is(student4.name),
                        "[1].user.first_name", is(student4.firstName),
                        "[1].user.email", is(student4.email),
                        "[1].user.group", is(student4.group.title),
                        "[1].user.type", is(student4.typePerson.toString()),

                        "[2].task_id", is((int) task1.id),
                        "[2].file_name", is("file1_" + student5.login + "_" + task1.id + ".txt"),
                        "[2].rendering_date", equalTo(978303600000L),
                        "[2].user.login", is(student5.login),
                        "[2].user.name", is(student5.name),
                        "[2].user.first_name", is(student5.firstName),
                        "[2].user.email", is(student5.email),
                        "[2].user.group", is(student5.group.title),
                        "[2].user.type", is(student5.typePerson.toString()),

                        "[3].task_id", is((int) task1.id),
                        "[3].file_name", equalTo(null),
                        "[3].rendering_date", equalTo(null),
                        "[3].user.login", is(otherStudent.login),
                        "[3].user.name", is(otherStudent.name),
                        "[3].user.first_name", is(otherStudent.firstName),
                        "[3].user.email", is(otherStudent.email),
                        "[3].user.group", is(otherStudent.group.title),
                        "[3].user.type", is(otherStudent.typePerson.toString()),

                        "[4].task_id", is((int) task1.id),
                        "[4].file_name", is("file1_" + student.login + "_" + task1.id + ".txt"),
                        "[4].rendering_date", equalTo(978303600000L),
                        "[4].user.login", is(student.login),
                        "[4].user.name", is(student.name),
                        "[4].user.first_name", is(student.firstName),
                        "[4].user.email", is(student.email),
                        "[4].user.group", is(student.group.title),
                        "[4].user.type", is(student.typePerson.toString())
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetWorksByTeacherWithAccess() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(200)
                .log().all()
                .body("size()", is(5),

                        "[0].task_id", is((int) task1.id),
                        "[0].file_name", is(teacherWork.fileName),
                        "[0].rendering_date", equalTo(978303600000L),
                        "[0].user.login", is(teacher.login),
                        "[0].user.name", is(teacher.name),
                        "[0].user.first_name", is(teacher.firstName),
                        "[0].user.email", is(teacher.email),
                        "[0].user.group", equalTo(null),
                        "[0].user.type", is(teacher.typePerson.toString()),

                        "[1].task_id", is((int) task1.id),
                        "[1].file_name", is("file1_" + student4.login + "_" + task1.id + ".txt"),
                        "[1].rendering_date", equalTo(978303600000L),
                        "[1].user.login", is(student4.login),
                        "[1].user.name", is(student4.name),
                        "[1].user.first_name", is(student4.firstName),
                        "[1].user.email", is(student4.email),
                        "[1].user.group", is(student4.group.title),
                        "[1].user.type", is(student4.typePerson.toString()),

                        "[2].task_id", is((int) task1.id),
                        "[2].file_name", is("file1_" + student5.login + "_" + task1.id + ".txt"),
                        "[2].rendering_date", equalTo(978303600000L),
                        "[2].user.login", is(student5.login),
                        "[2].user.name", is(student5.name),
                        "[2].user.first_name", is(student5.firstName),
                        "[2].user.email", is(student5.email),
                        "[2].user.group", is(student5.group.title),
                        "[2].user.type", is(student5.typePerson.toString()),

                        "[3].task_id", is((int) task1.id),
                        "[3].file_name", equalTo(null),
                        "[3].rendering_date", equalTo(null),
                        "[3].user.login", is(otherStudent.login),
                        "[3].user.name", is(otherStudent.name),
                        "[3].user.first_name", is(otherStudent.firstName),
                        "[3].user.email", is(otherStudent.email),
                        "[3].user.group", is(otherStudent.group.title),
                        "[3].user.type", is(otherStudent.typePerson.toString()),

                        "[4].task_id", is((int) task1.id),
                        "[4].file_name", is("file1_" + student.login + "_" + task1.id + ".txt"),
                        "[4].rendering_date", equalTo(978303600000L),
                        "[4].user.login", is(student.login),
                        "[4].user.name", is(student.name),
                        "[4].user.first_name", is(student.firstName),
                        "[4].user.email", is(student.email),
                        "[4].user.group", is(student.group.title),
                        "[4].user.type", is(student.typePerson.toString())
                );
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testGetWorksByTeacherWithoutAccess() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + field1.id + "'"));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetWorksByStudentWithAccess() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(200)
                .body("size()", is(2),

                        "[0].task_id", is((int) task1.id),
                        "[0].file_name", is(teacherWork.fileName),
                        "[0].rendering_date", equalTo(978303600000L),
                        "[0].user.login", is(teacher.login),
                        "[0].user.name", is(teacher.name),
                        "[0].user.first_name", is(teacher.firstName),
                        "[0].user.email", is(teacher.email),
                        "[0].user.group", equalTo(null),
                        "[0].user.type", is(teacher.typePerson.toString()),

                        "[1].task_id", is((int) task1.id),
                        "[1].file_name", is("file1_" + student.login + "_" + task1.id + ".txt"),
                        "[1].rendering_date", equalTo(978303600000L),
                        "[1].user.login", is(student.login),
                        "[1].user.name", is(student.name),
                        "[1].user.first_name", is(student.firstName),
                        "[1].user.email", is(student.email),
                        "[1].user.group", is(student.group.title),
                        "[1].user.type", is(student.typePerson.toString())
                );
    }

    @Test
    @TestSecurity(user = "student3", roles = "STUDENT")
    public void testGetWorksByStudentWithAccessAndNoWork() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(200)
                .body("size()", is(2),

                        "[0].task_id", is((int) task1.id),
                        "[0].file_name", is(teacherWork.fileName),
                        "[0].rendering_date", equalTo(978303600000L),
                        "[0].user.login", is(teacher.login),
                        "[0].user.name", is(teacher.name),
                        "[0].user.first_name", is(teacher.firstName),
                        "[0].user.email", is(teacher.email),
                        "[0].user.group", equalTo(null),
                        "[0].user.type", is(teacher.typePerson.toString()),

                        "[1].task_id", is((int) task1.id),
                        "[1].file_name", equalTo(null),
                        "[1].rendering_date", equalTo(null),
                        "[1].user.login", is(otherStudent.login),
                        "[1].user.name", is(otherStudent.name),
                        "[1].user.first_name", is(otherStudent.firstName),
                        "[1].user.email", is(otherStudent.email),
                        "[1].user.group", is(otherStudent.group.title),
                        "[1].user.type", is(otherStudent.typePerson.toString())
                );
    }

    @Test
    @TestSecurity(user = "student2", roles = "STUDENT")
    public void testGetWorksByStudentWithoutAccess() {
        given()
                .when()
                .get("/works/" + task1.id)
                .then()
                .statusCode(403)
                .body(is("[student2] verifyHaveAccessToField : access refused to field with id '" + field1.id + "'"));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetWorksForHiddenLesson() {
        given()
                .when()
                .get("/works/" + hiddenLesson.tasks.get(0).id)
                .then()
                .statusCode(404)
                .body(is("[student1] Unknown lesson with id '" + hiddenLesson.id + "'"));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetWorksForHiddenTask() {
        given()
                .when()
                .get("/works/" + hiddenTask.id)
                .then()
                .statusCode(404)
                .body(is("[student1] Unknown task with id '" + hiddenTask.id + "'"));
    }
}
