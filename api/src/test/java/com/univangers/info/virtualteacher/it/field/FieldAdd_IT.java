package com.univangers.info.virtualteacher.it.field;

import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.it.TestUtils;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FieldAdd_IT {

    Person admin, teacher1, teacherOther, student;

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
        admin = Person.findByLogin("admin");
        teacher1 = Person.findByLogin("teacher1");
        teacherOther = Person.findByLogin("teacher3");
        student = Person.findByLogin("student1");
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAddFieldByAdmin() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_1", "description", "icon", Arrays.asList(PersonMapper.toContract(admin), PersonMapper.toContract(teacher1)), Arrays.asList("L3INFO", "M1INFO")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(201)
                .body(
                        "field_name", is(contract.getFieldName()),
                        "description", is(contract.getDescription()),
                        "icon", is(contract.getIcon())
                );
        final Optional<Field> optional = Field.findByFieldNameOptional(contract.getFieldName());

        Assertions.assertTrue(optional.isPresent());

        final Field core = optional.get();

        Assertions.assertEquals(contract.getFieldName(), core.fieldName);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getIcon(), core.icon);
        Assertions.assertEquals(contract.getTeachers().size(), core.teachers.size());
        List<Person> s = core.teachers.stream().map(a -> a.person).sorted(Comparator.comparing(o -> o.id)).collect(Collectors.toList());
        for (int i = 0; i < core.teachers.size(); i++) {
            Assertions.assertEquals(contract.getTeachers().get(i).getFirstName(), s.get(i).firstName);
            Assertions.assertEquals(contract.getTeachers().get(i).getTypePerson(), s.get(i).typePerson);
            Assertions.assertEquals(contract.getTeachers().get(i).getEmail(), s.get(i).email);
            Assertions.assertEquals(contract.getTeachers().get(i).getName(), s.get(i).name);
        }

        Assertions.assertEquals(contract.getGroups().size(), core.groups.size());
        contract.getGroups().forEach(group -> Assertions
                .assertTrue(core.groups
                        .stream()
                        .map(g -> g.group.title)
                        .collect(Collectors.toList())
                        .contains(group)));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddFieldByTeacher() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_2", "description", "icon", Arrays.asList(PersonMapper.toContract(teacher1), PersonMapper.toContract(teacherOther)), Arrays.asList("L3INFO", "M1INFO")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(201)
                .body(
                        "field_name", is(contract.getFieldName()),
                        "description", is(contract.getDescription()),
                        "icon", is(contract.getIcon())
                );
        final Optional<Field> optional = Field.findByFieldNameOptional(contract.getFieldName());

        Assertions.assertTrue(optional.isPresent());

        final Field core = optional.get();

        Assertions.assertEquals(contract.getFieldName(), core.fieldName);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getIcon(), core.icon);
        List<Person> s = core.teachers.stream().map(a -> a.person).sorted(Comparator.comparing(o -> o.id)).collect(Collectors.toList());
        for (int i = 0; i < core.teachers.size(); i++) {
            Assertions.assertEquals(contract.getTeachers().get(i).getFirstName(), s.get(i).firstName);
            Assertions.assertEquals(contract.getTeachers().get(i).getTypePerson(), s.get(i).typePerson);
            Assertions.assertEquals(contract.getTeachers().get(i).getEmail(), s.get(i).email);
            Assertions.assertEquals(contract.getTeachers().get(i).getName(), s.get(i).name);
        }

        Assertions.assertEquals(contract.getGroups().size(), core.groups.size());
        contract.getGroups().forEach(group -> Assertions
                .assertTrue(core.groups
                        .stream()
                        .map(g -> g.group.title)
                        .collect(Collectors.toList())
                        .contains(group)));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testAddFieldByStudent() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                "field_name", "description", "icon", null
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(403);
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddWithEmptyFieldName() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                "", "description", "icon", null
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(400)
                .body(is("A field needs to have a name"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddWithNoTeacher() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_2", "description", "icon", Collections.emptyList(), Arrays.asList("L3INFO", "M1INFO")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(400)
                .body(is("A field needs to have at least one teacher"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddWithAStudentAsTeacher() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_2", "description", "icon", Collections.singletonList(PersonMapper.toContract(student)), Arrays.asList("L3INFO", "M1INFO")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(400)
                .body(is("A student cannot teach a field"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddWithUnknownTeacher() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_2", "description", "icon", Collections.singletonList(new com.univangers.info.virtualteacher.contract.Person("name", "firstName", "login", "email", TypePerson.TEACHER, null)), Arrays.asList("L3INFO", "M1INFO")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(404)
                .body(is("Unknown teacher with name 'name'"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testAddWithUnknownGroup() {
        final com.univangers.info.virtualteacher.contract.field.Field contract = new com.univangers.info.virtualteacher.contract.field.Field(
                0, "field_name_2", "description", "icon", Arrays.asList(PersonMapper.toContract(teacherOther), PersonMapper.toContract(teacher1)), Collections.singletonList("Unknown")
        );
        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/fields")
                .then()
                .statusCode(404)
                .body(is("Unknown group with title 'Unknown'"));
    }
}