package com.univangers.info.virtualteacher.it.task;

import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.Task;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.util.Date;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskWorkFolder_IT {

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @Transactional
    void testWorkFolder() {
        final Field field = new Field("fieldName", "description", "icon");
        field.persist();
        final Lesson lesson = new Lesson("lesson title", "description", new Date(), new Date(), false, 1, field);
        lesson.persist();
        final Task task = new Task("task title", "description", new Date(), new Date(), true, true, lesson);
        task.persist();

        Assertions.assertEquals("uploads/" + field.id + "/" + lesson.id + "/" + task.id + "/", task.getWorkFolder());
    }
}
