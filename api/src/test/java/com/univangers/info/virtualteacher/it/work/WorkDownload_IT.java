package com.univangers.info.virtualteacher.it.work;

import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.Task;
import com.univangers.info.virtualteacher.core.Work;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WorkDownload_IT {

    private long fieldId, taskId, workId, teacherWorkId;
    private String taskWorkFilename;

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
        Task task = Lesson.findByTitleOptional("cours1")
                .get()
                .tasks
                .stream()
                .filter(e -> "t1".equals(e.title))
                .findFirst()
                .orElseThrow(NullPointerException::new);
        fieldId = task.lesson.field.id;
        taskId = task.id;
        taskWorkFilename = task.getWorkFilename() + ".zip";
        Person student = Person.findByLogin("student1");
        Person teacher = Person.findByLogin("teacher1");
        workId = Work.findForTaskAndPersonOptional(task, student).get().id;
        teacherWorkId = TestUtils.createWork(teacher, task).id;
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAdminDownloadTask() {
        given()
                .when()
                .get("/works/" + taskId + "/download")
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(3000))
                .header("Content-Disposition", is("attachment; filename=\"field1_name-cours1-t1.zip\""));

        Assertions.assertFalse(new File(taskWorkFilename).exists());
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAdminDownloadUnknownTask() {
        given()
                .when()
                .get("/works/" + 999 + "/download")
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));

        Assertions.assertFalse(new File(taskWorkFilename).exists());
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testTeacherDownloadTask() {
        given()
                .when()
                .get("/works/" + taskId + "/download")
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(3000))
                .header("Content-Disposition", is("attachment; filename=\"field1_name-cours1-t1.zip\""));

        Assertions.assertFalse(new File(taskWorkFilename).exists());
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testTeacherWithoutAccessDownloadTask() {
        given()
                .when()
                .get("/works/" + taskId + "/download")
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + fieldId + "'"));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testStudentDownloadTask() {
        given()
                .when()
                .get("/works/" + taskId + "/download")
                .then()
                .statusCode(403)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAdminDownloadWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + workId)
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(2000))
                .header("Content-Disposition", is("attachment; filename=\"file1_student1_" + taskId + ".txt\""));
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAdminDownloadUnknownWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown work with id '999'"));
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testTeacherDownloadWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + workId)
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(2000))
                .header("Content-Disposition", is("attachment; filename=\"file1_student1_" + taskId + ".txt\""));
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testTeacherWithoutAccessDownloadWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + workId)
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + fieldId + "'"));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testStudentDownloadHisWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + workId)
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(2000))
                .header("Content-Disposition", is("attachment; filename=\"file1_student1_" + taskId + ".txt\""));
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testStudentDownloadTeacherWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + teacherWorkId)
                .then()
                .statusCode(200)
                .header("Content-Length", Integer::parseInt, greaterThan(2000))
                .header("Content-Disposition", is("attachment; filename=\"file1_teacher1_" + taskId + ".txt\""));
    }

    @Test
    @TestSecurity(user = "student3", roles = "STUDENT")
    public void testStudentDownloadOthersWork() {
        given()
                .when()
                .get("/works/" + taskId + "/download/" + workId)
                .then()
                .statusCode(403)
                .body(is("[student3] delete : access refused to work with id '" + workId + "'"));
    }
}
