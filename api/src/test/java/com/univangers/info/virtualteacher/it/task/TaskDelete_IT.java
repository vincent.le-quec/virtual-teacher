package com.univangers.info.virtualteacher.it.task;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskDelete_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentWithAccessLogin = "m.alexis",
            adminLogin = "p.nom";

    private Task task1, task2;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person studentWithAccess = new Person("alexis", "migniau", studentWithAccessLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        studentWithAccess.group = group;
        studentWithAccess.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        final Lesson lesson = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lesson.persist();

        task1 = new Task("Ex1",
                "Faire l'ex1",
                new Date(),
                new Date(),
                true,
                true,
                lesson);

        task1.persist();

        task2 = new Task("Ex2",
                "Faire l'ex2",
                new Date(),
                new Date(),
                true,
                true,
                lesson);

        task2.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testDeleteTeacherWithoutAccess() {
        given()
                .when()
                .delete("/tasks/" + task1.id)
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + task1.lesson.field.id + "'"));

        Optional<Task> optional = Task.findByIdOptional(task1.id);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testDeleteStudent() {
        given()
                .when()
                .delete("/tasks/" + task1.id)
                .then()
                .statusCode(403)
                .body(is(""));

        Optional<Task> optional = Task.findByIdOptional(task1.id);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testDeleteTeacherWithAccess() {
        given()
                .when()
                .delete("/tasks/" + task1.id)
                .then()
                .statusCode(200);

        Optional<Task> optional = Task.findByIdOptional(task1.id);
        Assertions.assertTrue(optional.isEmpty());
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testDeleteAdmin() {
        given()
                .when()
                .delete("/tasks/" + task2.id)
                .then()
                .statusCode(200);

        Optional<Task> optional = Task.findByIdOptional(task2.id);
        Assertions.assertTrue(optional.isEmpty());
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testDeleteTaskNotExist() {
        given()
                .when()
                .delete("/tasks/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));
    }
}
