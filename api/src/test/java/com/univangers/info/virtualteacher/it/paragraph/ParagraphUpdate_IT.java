package com.univangers.info.virtualteacher.it.paragraph;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ParagraphUpdate_IT {
    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentWithAccessLogin = "m.alexis",
            adminLogin = "p.nom";

    private Paragraph para1, para2, para3;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person studentWithAccess = new Person("alexis", "migniau", studentWithAccessLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        studentWithAccess.group = group;
        studentWithAccess.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        final Lesson lesson = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lesson.persist();

        para1 = new Paragraph("Paragraph 1",
                10,
                "Content",
                1,
                TypeFile.TXT,
                lesson
        );
        para1.persist();

        para2 = new Paragraph("Paragraph 2",
                10,
                "Content",
                2,
                TypeFile.TXT,
                lesson
        );
        para2.persist();

        para3 = new Paragraph("Paragraph 3",
                10,
                "Content",
                3,
                TypeFile.TXT,
                lesson
        );
        para3.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                para1.title,
                para1.titleSize,
                "Contenu updated",
                para1.order,
                para1.type.toString(),
                para1.lesson.id
        );
        contract.setId(para1.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/paragraphs")
                .then()
                .statusCode(200)
                .body(
                        "title", is(contract.getTitle()),
                        "title_size", is(contract.getTitleSize()),
                        "content", is(contract.getContent()),
                        "order", is(contract.getOrder()),
                        "type", is(contract.getType()),
                        "lesson_id", is(contract.getLessonId().intValue())
                );

        // Then
        final Optional<Paragraph> optional = Paragraph.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Titre updated",
                para2.titleSize,
                para2.content,
                para2.order,
                para2.type.toString(),
                para2.lesson.id
        );
        contract.setId(para2.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/paragraphs")
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + para2.lesson.field.id + "'"));

        // Then
        final Optional<Paragraph> optional = Paragraph.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(para2.title, core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testUpdateStudent() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                para2.title,
                para2.titleSize,
                para2.content,
                5,
                para2.type.toString(),
                para2.lesson.id
        );
        contract.setId(para2.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/paragraphs")
                .then()
                .statusCode(403)
                .body(is(""));

        // Then
        final Optional<Paragraph> optional = Paragraph.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(para2.order, core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
        Assertions.assertEquals(contract.getLessonId(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testUpdateAdmin() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Titre updated",
                para3.titleSize,
                "Contenu updated",
                para3.order,
                para3.type.toString(),
                null
        );
        contract.setId(para3.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/paragraphs")
                .then()
                .statusCode(200)
                .body(
                        "title", is(contract.getTitle()),
                        "title_size", is(contract.getTitleSize()),
                        "content", is(contract.getContent()),
                        "order", is(contract.getOrder()),
                        "type", is(contract.getType())
                );

        // Then
        final Optional<Paragraph> optional = Paragraph.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Paragraph core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getTitleSize(), core.titleSize);
        Assertions.assertEquals(contract.getContent(), core.content);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.getType(), core.type.toString());
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testParagraphNotExist() {
        final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                "Titre updated",
                para3.titleSize,
                "Contenu updated",
                para1.order,
                para3.type.toString(),
                para3.lesson.id
        );
        contract.setId(999);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/paragraphs")
                .then()
                .statusCode(404)
                .body(is("Unknown paragraph with id '999'"));
    }
}
