package com.univangers.info.virtualteacher.it.field;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class FieldDelete_IT {

    long field1, field2, field3, fieldPerson1Id, fieldPerson2Id, fieldGroup1Id, fieldGroup2Id;
    Person admin, teacher1, teacherOther, student;

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
        admin = Person.findByLogin("admin");
        teacher1 = Person.findByLogin("teacher1");
        teacherOther = Person.findByLogin("teacher3");
        student = Person.findByLogin("student1");

        Field f2 = Field.findByFieldNameOptional("field2_name").get();
        field1 = Field.findByFieldNameOptional("field1_name").get().id;
        field2 = f2.id;
        field3 = Field.findByFieldNameOptional("field3_name").get().id;

        fieldPerson1Id = f2.teachers.get(0).id;
        fieldPerson2Id = f2.teachers.get(1).id;
        fieldGroup1Id = f2.groups.get(0).id;
        fieldGroup2Id = f2.groups.get(1).id;
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testDeleteFieldByAdmin() {
        given()
                .when()
                .delete("/fields/" + field2)
                .then()
                .statusCode(200);

        Optional<Field> optional = Field.findByIdOptional(field2);
        Assertions.assertTrue(optional.isEmpty());

        // verify teachers have not been deleted
        Assertions.assertTrue(Person.findByLoginOptional("teacher1").isPresent());
        Assertions.assertTrue(Person.findByLoginOptional("teacher3").isPresent());

        // verify groups have not been deleted
        Assertions.assertTrue(Group.findByByNameOptional("L3INFO").isPresent());
        Assertions.assertTrue(Group.findByByNameOptional("M1INFO").isPresent());

        // verify FieldPerson are deleted
        Assertions.assertTrue(FieldPerson.findByIdOptional(fieldPerson1Id).isEmpty());
        Assertions.assertTrue(FieldPerson.findByIdOptional(fieldPerson2Id).isEmpty());

        // verify FieldGroup are deleted
        Assertions.assertTrue(FieldGroup.findByIdOptional(fieldGroup1Id).isEmpty());
        Assertions.assertTrue(FieldGroup.findByIdOptional(fieldGroup2Id).isEmpty());
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testDeleteFieldByTeacherWithAccess() {
        given()
                .when()
                .delete("/fields/" + field1)
                .then()
                .statusCode(200);

        Optional<Field> optional = Field.findByIdOptional(field1);
        Assertions.assertTrue(optional.isEmpty());

        // verify teachers have not been deleted
        Assertions.assertTrue(Person.findByLoginOptional("teacher1").isPresent());
        Assertions.assertTrue(Person.findByLoginOptional("teacher3").isPresent());

        // verify groups have not been deleted
        Assertions.assertTrue(Group.findByByNameOptional("L3INFO").isPresent());
        Assertions.assertTrue(Group.findByByNameOptional("M1INFO").isPresent());
    }

    @Test
    @TestSecurity(user = "teacher2", roles = "TEACHER")
    public void testDeleteFieldByTeacherWithoutAccess() {
        given()
                .when()
                .delete("/fields/" + field3)
                .then()
                .statusCode(403)
                .body(is("[teacher2] verifyHaveAccessToField : access refused to field with id '" + field3 + "'"));

        Optional<Field> optional = Field.findByIdOptional(field3);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testDeleteFieldByStudent() {
        given()
                .when()
                .delete("/fields/" + field3)
                .then()
                .statusCode(403)
                .body(is(""));

        Optional<Field> optional = Field.findByIdOptional(field3);
        Assertions.assertTrue(optional.isPresent());
    }
}
