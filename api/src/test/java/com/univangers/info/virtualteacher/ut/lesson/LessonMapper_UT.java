package com.univangers.info.virtualteacher.ut.lesson;

import com.univangers.info.virtualteacher.contract.lesson.Lesson;
import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.core.TypeFile;
import com.univangers.info.virtualteacher.provider.mapper.lesson.LessonMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LessonMapper_UT {

    @Test
    public void testFromContract() {
        final Lesson contract = new Lesson(
                "Title",
                "Description",
                new Date(),
                new Date(),
                false,
                1,
                0L,
                null,
                null
        );

        // Transform contract to core
        final com.univangers.info.virtualteacher.core.Lesson core = LessonMapper.fromContract(contract);

        // Assert if the values of contract equals values of core
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getOrder(), core.order);
        Assertions.assertEquals(contract.isHide(), core.hide);
    }

    @Test
    public void testFromContractNull() {
        final Lesson contract = new Lesson(
                "Title",
                null,
                null,
                null,
                null,
                null,
                0L,
                null,
                null
        );

        // Transform contract to core
        final com.univangers.info.virtualteacher.core.Lesson core = LessonMapper.fromContract(contract);

        // Assert if the values of contract equals values of core
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertNull(core.description);
        Assertions.assertNull(core.beginning);
        Assertions.assertNull(core.ending);
        Assertions.assertEquals(0, core.order);
        Assertions.assertFalse(core.hide);
    }

    @Test
    public void testUpdateFromContract() {
        // Create contract
        Lesson contract = new Lesson(
                "Title",
                "Description",
                new Date(),
                new Date(),
                false,
                2,
                0L,
                new ArrayList<>(),
                new ArrayList<>()
        );

        final com.univangers.info.virtualteacher.core.Lesson core1 = new com.univangers.info.virtualteacher.core.Lesson("Title",
                "Description",
                new Date(),
                new Date(),
                false,
                0,
                null,
                new ArrayList<>(),
                new ArrayList<>());

        com.univangers.info.virtualteacher.core.Lesson core2 = LessonMapper.fromContract(contract, core1);

        // Assert if the values of contract equals values of core
        Assertions.assertEquals(contract.getTitle(), core2.title);
        Assertions.assertEquals(contract.getDescription(), core2.description);
        Assertions.assertEquals(contract.getBeginning(), core2.beginning);
        Assertions.assertEquals(contract.getEnding(), core2.ending);
        Assertions.assertEquals(contract.isHide(), core2.hide);
        Assertions.assertEquals(contract.getOrder(), core2.order);
        //Assertions.assertEquals(contract.getFieldId(), core2.field.id);


        contract = new Lesson("Title",
                null,
                new Date(),
                new Date(),
                false,
                0,
                0L,
                new ArrayList<>(),
                null);

        core2 = LessonMapper.fromContract(contract, core1);

        Assertions.assertEquals(contract.getTitle(), core2.title);
        Assertions.assertEquals(core1.description, core2.description);
        Assertions.assertEquals(contract.getBeginning(), core2.beginning);
        Assertions.assertEquals(contract.getEnding(), core2.ending);
        Assertions.assertEquals(contract.isHide(), core2.hide);
        Assertions.assertEquals(core1.order, core2.order);
        //Assertions.assertEquals(contract.getFieldId(), core2.field.id);
    }

    @Test
    public void testToContract() {
        // Create data
        List<com.univangers.info.virtualteacher.core.Paragraph> paragraphs = new ArrayList<>();
        paragraphs.add(new com.univangers.info.virtualteacher.core.Paragraph("Intro", 5, "Début", 1, TypeFile.TXT));
        paragraphs.add(new com.univangers.info.virtualteacher.core.Paragraph("Conclu", 6, "Fin", 0, TypeFile.TXT));

        Field field = new Field();

        final com.univangers.info.virtualteacher.core.Lesson core = new com.univangers.info.virtualteacher.core.Lesson("Title",
                "Description",
                new Date(),
                new Date(),
                false,
                0,
                field,
                paragraphs,
                new ArrayList<>());

        // Transform core to contract
        final Lesson contract = LessonMapper.toContract(core);

        // Assert if the values of contract equals values of core
        Assertions.assertEquals(core.title, contract.getTitle());
        Assertions.assertEquals(core.description, contract.getDescription());
        Assertions.assertEquals(core.beginning, contract.getBeginning());
        Assertions.assertEquals(core.ending, contract.getEnding());
        Assertions.assertEquals(core.hide, contract.isHide());
        Assertions.assertEquals(core.field.id, contract.getFieldId());
        Assertions.assertEquals(core.order, contract.getOrder());
        //Assertions.assertEquals(core.paragraphs.size(), contract.getParagraphs().size());
    }
}
