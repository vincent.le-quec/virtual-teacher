package com.univangers.info.virtualteacher.it.lesson;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LessonAdd_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentLogin = "m.alexis",
            adminLogin = "p.nom";
    private Field fieldAccess;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create data
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person student = new Person("alexis", "migniau", studentLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        student.group = group;
        student.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson2",
                "Description",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess.id,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(201)
                .body("title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "hide", is(contract.isHide()),
                        "field_id", is(contract.getFieldId().intValue())
                );

        // THEN
        final Optional<Lesson> optional = Lesson.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.isHide(), core.hide);
        Assertions.assertEquals(contract.getFieldId(), core.field.id);
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testAddTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson3",
                "Description",
                new Date(),
                new Date(),
                true,
                1,
                fieldAccess.id,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + fieldAccess.id + "'"));
    }

    @Test
    @TestSecurity(user = studentLogin, roles = "STUDENT")
    public void testAddStudent() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson4",
                "Description",
                new Date(),
                new Date(),
                false,
                1,
                fieldAccess.id,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(403)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testAddAdmin() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson6",
                "Description",
                new Date(),
                new Date(),
                false,
                2,
                fieldAccess.id,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(201)
                .body("title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "hide", is(contract.isHide()),
                        "field_id", is(contract.getFieldId().intValue())
                );

        // THEN
        final Optional<Lesson> optional = Lesson.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.isHide(), core.hide);
        Assertions.assertEquals(contract.getFieldId(), core.field.id);
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testFieldNotExist() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson4",
                "Description",
                new Date(),
                new Date(),
                false,
                6,
                (long) 999,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(404)
                .body(is("Unknown field with id '999'"));
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testAddOptional() {
        final com.univangers.info.virtualteacher.contract.lesson.Lesson contract = new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                "Lesson5",
                null,
                null,
                null,
                null,
                null,
                fieldAccess.id,
                null,
                null
        );

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .post("/lessons")
                .then()
                .statusCode(201)
                .body("title", is(contract.getTitle()),
                        "field_id", is(contract.getFieldId().intValue())
                );

        // THEN
        final Optional<Lesson> optional = Lesson.findByTitleOptional(contract.getTitle());
        Assertions.assertTrue(optional.isPresent());
        final Lesson core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getFieldId(), core.field.id);
    }
}
