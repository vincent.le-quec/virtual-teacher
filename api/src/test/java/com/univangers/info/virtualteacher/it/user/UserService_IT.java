package com.univangers.info.virtualteacher.it.user;

import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserService_IT {

    @BeforeAll
    @Transactional
    public void before() {
        TestUtils.generateDB();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testGetMyDataAdmin() {
        given()
                .when()
                .get("/user/me")
                .then()
                .statusCode(200)
                .body(
                        "name", is("admin_name"),
                        "first_name", is("admin_first_name"),
                        "login", is("admin"),
                        "email", is("admin_email"),
                        "type", is("ADMIN"),
                        "group", is(nullValue())
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetMyDataTeacher() {
        given()
                .when()
                .get("/user/me")
                .then()
                .statusCode(200)
                .body(
                        "name", is("teacherWithAccess_name"),
                        "first_name", is("teacherWithAccess_first_name"),
                        "login", is("teacher1"),
                        "email", is("teacherWithAccess_email"),
                        "type", is("TEACHER"),
                        "group", is(nullValue())
                );
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetMyDataStudent() {
        given()
                .when()
                .get("/user/me")
                .then()
                .statusCode(200)
                .body("name", is("studentWithAccess_name"),
                        "first_name", is("studentWithAccess_first_name"),
                        "login", is("student1"),
                        "email", is("studentWithAccess_email"),
                        "type", is("STUDENT"),
                        "group", is("L3INFO")
                );
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testGetTeachersAdmin() {
        given()
                .when()
                .get("/user/teachers")
                .then()
                .statusCode(200)
                .body("size()", is(4),

                        "[0].name", is("admin_name"),
                        "[0].first_name", is("admin_first_name"),
                        "[0].login", is("admin"),
                        "[0].email", is("admin_email"),
                        "[0].type", is("ADMIN"),
                        "[0].group", is(nullValue()),

                        "[1].name", is("teacherOther_name"),
                        "[1].first_name", is("teacherOther_first_name"),
                        "[1].login", is("teacher3"),
                        "[1].email", is("teacherOther_email"),
                        "[1].type", is("TEACHER"),
                        "[1].group", is(nullValue()),

                        "[2].name", is("teacherWithAccess_name"),
                        "[2].first_name", is("teacherWithAccess_first_name"),
                        "[2].login", is("teacher1"),
                        "[2].email", is("teacherWithAccess_email"),
                        "[2].type", is("TEACHER"),
                        "[2].group", is(nullValue()),

                        "[3].name", is("teacherWithoutAccess_name"),
                        "[3].first_name", is("teacherWithoutAccess_first_name"),
                        "[3].login", is("teacher2"),
                        "[3].email", is("teacherWithoutAccess_email"),
                        "[3].type", is("TEACHER"),
                        "[3].group", is(nullValue())
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testGetTeachersTeacher1() {
        given()
                .when()
                .get("/user/teachers")
                .then()
                .statusCode(200)
                .body("size()", is(4),

                        "[0].name", is("admin_name"),
                        "[0].first_name", is("admin_first_name"),
                        "[0].login", is("admin"),
                        "[0].email", is("admin_email"),
                        "[0].type", is("ADMIN"),
                        "[0].group", is(nullValue()),

                        "[1].name", is("teacherOther_name"),
                        "[1].first_name", is("teacherOther_first_name"),
                        "[1].login", is("teacher3"),
                        "[1].email", is("teacherOther_email"),
                        "[1].type", is("TEACHER"),
                        "[1].group", is(nullValue()),

                        "[2].name", is("teacherWithAccess_name"),
                        "[2].first_name", is("teacherWithAccess_first_name"),
                        "[2].login", is("teacher1"),
                        "[2].email", is("teacherWithAccess_email"),
                        "[2].type", is("TEACHER"),
                        "[2].group", is(nullValue()),

                        "[3].name", is("teacherWithoutAccess_name"),
                        "[3].first_name", is("teacherWithoutAccess_first_name"),
                        "[3].login", is("teacher2"),
                        "[3].email", is("teacherWithoutAccess_email"),
                        "[3].type", is("TEACHER"),
                        "[3].group", is(nullValue())
                );
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testGetTeachersStudent1() {
        given()
                .when()
                .get("/user/teachers")
                .then()
                .statusCode(403)
                .body(is(""));
    }
}
