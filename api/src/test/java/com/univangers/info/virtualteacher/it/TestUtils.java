package com.univangers.info.virtualteacher.it;

import com.univangers.info.virtualteacher.core.*;
import org.apache.commons.io.FileUtils;
import org.jboss.logging.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestUtils {

    private static final Logger LOGGER = Logger.getLogger(TestUtils.class);

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    static Group l3, m1, m2;
    static Person admin, teacherWithAccess, teacherWithoutAccess, teacherOther, studentWithAccess, studentWithoutAccess, studentOther, student4, student5;
    static Field field1, field2, field3;
    private static Date fixedDate;

    static {
        try {
            fixedDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2001");
        } catch (ParseException ignored) {
        }
    }

    private TestUtils() {
    }

    public static void generateDB() {
        createListOfGroups();
        createListOfPersons();
        studentWithAccess.group = l3;
        student4.group = l3;
        student5.group = l3;
        studentWithoutAccess.group = m2;
        studentOther.group = m1;

        createListOfFields(l3, m1, teacherWithAccess, teacherOther);
    }

    public static void deleteDB() {
        try {
            FileUtils.deleteDirectory(new File("uploads"));
            new File("filename.txt").delete();
        } catch (IOException e) {
            System.err.println("ERROR : error while delete uploads folder and created files");
        }
        Work.deleteAll();
        Paragraph.deleteAll();
        Task.deleteAll();
        Lesson.deleteAll();
        FieldPerson.deleteAll();
        FieldGroup.deleteAll();
        Field.deleteAll();
        Person.deleteAll();
        Group.deleteAll();
    }

    public static List<Person> createListOfPersons() {
        admin = createPerson("admin_name", "admin_first_name", "admin", "admin_email", TypePerson.ADMIN);
        admin.persist();
        teacherWithAccess = createPerson("teacherWithAccess_name", "teacherWithAccess_first_name", "teacher1", "teacherWithAccess_email", TypePerson.TEACHER);
        teacherWithAccess.persist();
        teacherWithoutAccess = createPerson("teacherWithoutAccess_name", "teacherWithoutAccess_first_name", "teacher2", "teacherWithoutAccess_email", TypePerson.TEACHER);
        teacherWithoutAccess.persist();
        teacherOther = createPerson("teacherOther_name", "teacherOther_first_name", "teacher3", "teacherOther_email", TypePerson.TEACHER);
        teacherOther.persist();
        studentWithAccess = createPerson("studentWithAccess_name", "studentWithAccess_first_name", "student1", "studentWithAccess_email", TypePerson.STUDENT);
        studentWithAccess.persist();
        studentWithoutAccess = createPerson("studentWithoutAccess_name", "studentWithoutAccess_first_name", "student2", "studentWithoutAccess_email", TypePerson.STUDENT);
        studentWithoutAccess.persist();
        studentOther = createPerson("studentOther_name", "studentOther_first_name", "student3", "studentOther_email", TypePerson.STUDENT);
        studentOther.persist();
        student4 = createPerson("student4", "student4", "student4", "student4", TypePerson.STUDENT);
        student4.persist();
        student5 = createPerson("student5", "student5", "student5", "student5", TypePerson.STUDENT);
        student5.persist();

        return Arrays.asList(admin, teacherWithAccess, teacherWithoutAccess, teacherOther, studentWithAccess, studentWithoutAccess, studentOther, student4, student5);
    }

    public static Person createPerson(final String name, final String firstName, final String login, final String email, final TypePerson typePerson) {
        return new Person(name, firstName, login, email, typePerson);
    }

    public static List<Group> createListOfGroups() {
        l3 = createGroup("L3INFO");
        l3.persist();
        m1 = createGroup("M1INFO");
        m1.persist();
        m2 = createGroup("M2INFO");
        m2.persist();

        return Arrays.asList(l3, m1, m2);
    }

    public static Group createGroup(final String name) {
        return new Group(name);
    }

    public static List<Field> createListOfFields(Group g1, Group g2, Person t1, Person t2) {
        field1 = createField("field1_name", "field1_description", "field1_icon");
        new File("uploads/" + field1.fieldName + "/").mkdirs();
        field1.persist();
        field2 = createField("field2_name", "field2_description", "field2_icon");
        new File("uploads/" + field2.fieldName + "/").mkdirs();
        field2.persist();
        field3 = createField("field3_name", "field3_description", "field3_icon");
        new File("uploads/" + field3.fieldName + "/").mkdirs();
        field3.persist();

        field1.lessons = new ArrayList<>(createListOfLessons(field1));
        field1.teachers = Arrays.asList(new FieldPerson(field1, t1), new FieldPerson(field1, t2));
        field1.groups = Arrays.asList(new FieldGroup(field1, g1), new FieldGroup(field1, g2));

        field2.lessons = new ArrayList<>(createListOfLessons(field1));
        field2.teachers = Arrays.asList(new FieldPerson(field2, t1), new FieldPerson(field2, t2));
        field2.groups = Arrays.asList(new FieldGroup(field2, g1), new FieldGroup(field2, g2));

        return Arrays.asList(field1, field2, field3);
    }

    public static Field createField(final String fieldName, final String description, final String icon) {
        return new Field(fieldName, description, icon);
    }

    public static List<Lesson> createListOfLessons(final Field field) {
        Lesson intro = createLesson("intro on field " + field.fieldName, "description of intro on " + field.fieldName, 1, false, field);
        intro.persist();
        intro.paragraphs = new ArrayList<>(createListOfParagraphs(intro));

        Lesson cours1 = createLesson("cours1", "description of cours1 on " + field.fieldName, 2, false, field);
        cours1.persist();
        cours1.paragraphs = new ArrayList<>(createListOfParagraphs(cours1));
        cours1.tasks = new ArrayList<>(createListOfTasks(cours1));

        Lesson cours2 = createLesson("cours2 on field " + field.fieldName, "description of cours2 on " + field.fieldName, 3, false, field);
        cours2.persist();
        cours2.paragraphs = new ArrayList<>(createListOfParagraphs(cours2));
        cours2.tasks = new ArrayList<>(createListOfTasks(cours2));

        Lesson tp1 = createLesson("tp1 on field " + field.fieldName, "description of cours2 on " + field.fieldName, 4, false, field);
        tp1.persist();
        tp1.tasks = new ArrayList<>(createListOfTasks(tp1));

        Lesson exam = createLesson("hidden", "exam on " + field.fieldName, 4, true, field);
        exam.persist();
        exam.tasks = new ArrayList<>(createListOfTasks(exam));

        return Arrays.asList(intro, cours1, cours2, tp1);
    }

    public static Lesson createLesson(String title, String description, int order, boolean hide, Field field) {
        return new Lesson(title, description, aDayBefore, aDayAfter, hide, order, field);
    }

    public static List<Paragraph> createListOfParagraphs(Lesson lesson) {
        Paragraph p1 = createParagraph(1, 1, lesson);
        p1.persist();
        Paragraph p2 = createParagraph(2, 2, lesson);
        p2.persist();
        Paragraph p3 = createParagraph(2, 3, lesson);
        p3.persist();
        Paragraph p4 = createParagraph(3, 4, lesson);
        p4.persist();
        Paragraph p5 = createParagraph(3, 5, lesson);
        p5.persist();
        Paragraph p6 = createParagraph(1, 6, lesson);
        p6.persist();
        Paragraph p7 = createParagraph(1, 7, lesson);
        p7.persist();

        return Arrays.asList(p1, p2, p3, p4, p5, p6, p7);
    }

    public static Paragraph createParagraph(int titleSize, int order, Lesson lesson) {
        return new Paragraph("L" + lesson.title + " p" + order + " s" + titleSize, titleSize, "This is some content for the paragraph with order " + order + " and title size " + titleSize + " and lesson " + lesson.title + " : " + lesson.description, order, TypeFile.TXT, lesson);
    }

    public static List<Task> createListOfTasks(final Lesson lesson) {
        Task t1 = createTask("t1", true, lesson);
        t1.persist();
        Task t2 = createTask("t2", true, lesson);
        t2.persist();
        Task exam = createTask("hiddenTask", false, lesson);
        exam.persist();

        t1.works.addAll(Arrays.asList(createWork(studentWithAccess, t1), createWork(student4, t1), createWork(student5, t1)));
        t2.works.addAll(Arrays.asList(createWork(studentWithAccess, t2), createWork(student4, t2), createWork(student5, t2)));

        return Arrays.asList(t1, t2, exam);
    }

    public static Task createTask(String title, boolean displayed, Lesson lesson) {
        return new Task(title, "description of the task with title " + title, aDayBefore, aDayAfter, displayed, true, lesson);
    }

    public static Work createWork(Person person, Task task) {
        final String fileName = "file1_" + person.login + "_" + task.id + ".txt";
        final Work core = new Work(fileName, fixedDate, person, task);
        core.persist();

        File dir = new File(task.getWorkFolder());
        dir.mkdirs();
        File tmp = new File(core.getWorkPath());
        try {
            tmp.createNewFile();
            fillFile(core.getWorkPath());
        } catch (IOException e) {
            LOGGER.error("createWork:" + e.getMessage());
        }

        return core;
    }

    public static void fillFile(final String filename) {
        try {
            FileWriter myWriter = new FileWriter(filename);
            getFileContent()
                    .forEach(l -> {
                        try {
                            myWriter.write(l);
                        } catch (IOException e) {
                            LOGGER.error("fillFile:getFileContent:forEach:" + e.getMessage());
                        }
                    });
            myWriter.close();
        } catch (IOException e) {
            LOGGER.error("fillFile:" + e.getMessage());
        }
    }

    public static List<String> getFileContent() {
        return Arrays.asList(
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
        );
    }
}