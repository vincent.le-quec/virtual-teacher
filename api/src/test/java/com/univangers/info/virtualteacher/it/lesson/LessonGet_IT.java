package com.univangers.info.virtualteacher.it.lesson;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LessonGet_IT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentWithAccessLogin = "m.alexis",
            studentWithoutAccessLogin = "b.lucas",
            adminLogin = "p.nom";
    private Field fieldAccess;
    private Lesson lessonAlgo, lessonReseaux, lessonHidden;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();
        final Group groupUnused = new Group("groupUnused");
        groupUnused.persist();

        // Create data
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person studentWithAccess = new Person("alexis", "migniau", studentWithAccessLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        studentWithAccess.group = group;
        studentWithAccess.persist();

        Person studentWithoutAccess = new Person("benjamin", "lucas", studentWithoutAccessLogin, "b.lucas@gmail.com", TypePerson.STUDENT);
        studentWithoutAccess.group = groupUnused;
        studentWithoutAccess.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        lessonAlgo = new Lesson("Algorithmie",
                "Cours d'algo",
                aDayBefore,
                aDayAfter,
                false,
                0,
                fieldAccess,
                null,
                null);

        lessonAlgo.persist();

        lessonReseaux = new Lesson("Réseaux",
                "Cours de réseaux",
                aDayBefore,
                aDayAfter,
                false,
                1,
                fieldAccess,
                null,
                null);

        lessonReseaux.persist();

        lessonHidden = new Lesson("Hidden",
                "Cours Hidden",
                aDayAfter,
                aDayAfter,
                false,
                2,
                fieldAccess,
                null,
                null);

        lessonHidden.persist();
    }

    /* TODO
    test :
    - student get all with a lesson missing which is hidden
    - student get a hidden lesson -> 404
    - teacher get hidden
    - admin get hidden
     */

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetTeacherWithAccess() {
        given()
                .when()
                .get("/lessons/field/" + fieldAccess.id)
                .then()
                .statusCode(200)
                .body("size()", is(3),
                        "[0].title", is(lessonAlgo.title),
                        "[0].description", is(lessonAlgo.description),
                        "[0].beginning", is(lessonAlgo.beginning.getTime()),
                        "[0].ending", is(lessonAlgo.ending.getTime()),
                        "[0].hide", is(lessonAlgo.hide),
                        "[0].order", is(lessonAlgo.order),

                        "[1].title", is(lessonReseaux.title),
                        "[1].description", is(lessonReseaux.description),
                        "[1].beginning", is(lessonReseaux.beginning.getTime()),
                        "[1].ending", is(lessonReseaux.ending.getTime()),
                        "[1].hide", is(lessonReseaux.hide),
                        "[1].order", is(lessonReseaux.order),

                        "[2].title", is(lessonHidden.title),
                        "[2].description", is(lessonHidden.description),
                        "[2].beginning", is(lessonHidden.beginning.getTime()),
                        "[2].ending", is(lessonHidden.ending.getTime()),
                        "[2].hide", is(lessonHidden.hide),
                        "[2].order", is(lessonHidden.order)
                );
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testGetStudentWithAccess() {
        given()
                .when()
                .get("/lessons/field/" + fieldAccess.id)
                .then()
                .statusCode(200)
                .body("size()", is(2),
                        "[0].title", is(lessonAlgo.title),
                        "[0].description", is(lessonAlgo.description),
                        "[0].beginning", is(lessonAlgo.beginning.getTime()),
                        "[0].ending", is(lessonAlgo.ending.getTime()),
                        "[0].hide", is(lessonAlgo.hide),
                        "[0].order", is(lessonAlgo.order),

                        "[1].title", is(lessonReseaux.title),
                        "[1].description", is(lessonReseaux.description),
                        "[1].beginning", is(lessonReseaux.beginning.getTime()),
                        "[1].ending", is(lessonReseaux.ending.getTime()),
                        "[1].hide", is(lessonReseaux.hide),
                        "[1].order", is(lessonReseaux.order)
                );
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetAdmin() {
        given()
                .when()
                .get("/lessons/field/" + fieldAccess.id)
                .then()
                .statusCode(200)
                .body("size()", is(3),
                        "[0].title", is(lessonAlgo.title),
                        "[0].description", is(lessonAlgo.description),
                        "[0].beginning", is(lessonAlgo.beginning.getTime()),
                        "[0].ending", is(lessonAlgo.ending.getTime()),
                        "[0].hide", is(lessonAlgo.hide),
                        "[0].order", is(lessonAlgo.order),

                        "[1].title", is(lessonReseaux.title),
                        "[1].description", is(lessonReseaux.description),
                        "[1].beginning", is(lessonReseaux.beginning.getTime()),
                        "[1].ending", is(lessonReseaux.ending.getTime()),
                        "[1].hide", is(lessonReseaux.hide),
                        "[1].order", is(lessonReseaux.order),

                        "[2].title", is(lessonHidden.title),
                        "[2].description", is(lessonHidden.description),
                        "[2].beginning", is(lessonHidden.beginning.getTime()),
                        "[2].ending", is(lessonHidden.ending.getTime()),
                        "[2].hide", is(lessonHidden.hide),
                        "[2].order", is(lessonHidden.order)
                );
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testGetTeacherWithoutAccess() {
        given()
                .when()
                .get("/lessons/field/" + fieldAccess.id)
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + fieldAccess.id + "'"));
    }

    @Test
    @TestSecurity(user = studentWithoutAccessLogin, roles = "STUDENT")
    public void testGetStudentWithoutAccess() {
        given()
                .when()
                .get("/lessons/field/" + fieldAccess.id)
                .then()
                .statusCode(403)
                .body(is("[b.lucas] verifyHaveAccessToField : access refused to field with id '" + fieldAccess.id + "'"));
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetFieldNotExist() {
        given()
                .when()
                .get("/lessons/field/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown field with id '999'"));
    }






    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetLessonFromAdmin() {
        given()
                .when()
                .get("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(200)
                .body("title", is(lessonAlgo.title),
                        "description", is(lessonAlgo.description),
                        "beginning", is(lessonAlgo.beginning.getTime()),
                        "ending", is(lessonAlgo.ending.getTime()),
                        "hide", is(lessonAlgo.hide),
                        "order", is(lessonAlgo.order)
                );
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetLessonFromTeacher() {
        given()
                .when()
                .get("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(200)
                .body("title", is(lessonAlgo.title),
                        "description", is(lessonAlgo.description),
                        "beginning", is(lessonAlgo.beginning.getTime()),
                        "ending", is(lessonAlgo.ending.getTime()),
                        "hide", is(lessonAlgo.hide),
                        "order", is(lessonAlgo.order)
                );
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testGetLessonFromTeacherWithoutAccess() {
        given()
                .when()
                .get("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + fieldAccess.id + "'"));
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testGetLessonFromStudent() {
        given()
                .when()
                .get("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(200)
                .body("title", is(lessonAlgo.title),
                        "description", is(lessonAlgo.description),
                        "beginning", is(lessonAlgo.beginning.getTime()),
                        "ending", is(lessonAlgo.ending.getTime()),
                        "hide", is(lessonAlgo.hide),
                        "order", is(lessonAlgo.order)
                );
    }

    @Test
    @TestSecurity(user = studentWithoutAccessLogin, roles = "STUDENT")
    public void testGetLessonFromStudentWithoutAccess() {
        given()
                .when()
                .get("/lessons/" + lessonAlgo.id)
                .then()
                .statusCode(403)
                .body(is("[b.lucas] verifyHaveAccessToField : access refused to field with id '" + fieldAccess.id + "'"));
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetLessonNotExist() {
        given()
                .when()
                .get("/lessons/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown lesson with id '999'"));
    }
}
