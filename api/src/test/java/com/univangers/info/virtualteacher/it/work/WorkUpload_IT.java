package com.univangers.info.virtualteacher.it.work;

import com.univangers.info.virtualteacher.contract.work.WorkJson;
import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WorkUpload_IT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    private long t1Id, t2Id, adminId, teacher1Id, student1Id, hiddenTaskId;
    private String alreadyTakenFilename;
    private File file;

    @BeforeAll
    @Transactional
    public void before() {
        Person admin = TestUtils.createPerson("admin_name", "admin_first_name", "admin", "admin_email", TypePerson.ADMIN);
        admin.persist();
        Person teacherWithAccess = TestUtils.createPerson("teacherWithAccess_name", "teacherWithAccess_first_name", "teacher1", "teacherWithAccess_email", TypePerson.TEACHER);
        teacherWithAccess.persist();
        Person teacherWithoutAccess = TestUtils.createPerson("teacherWithoutAccess_name", "teacherWithoutAccess_first_name", "teacher2", "teacherWithoutAccess_email", TypePerson.TEACHER);
        teacherWithoutAccess.persist();
        Person teacherOther = TestUtils.createPerson("teacherOther_name", "teacherOther_first_name", "teacher3", "teacherOther_email", TypePerson.TEACHER);
        teacherOther.persist();
        Person studentWithAccess = TestUtils.createPerson("studentWithAccess_name", "studentWithAccess_first_name", "student1", "studentWithAccess_email", TypePerson.STUDENT);
        studentWithAccess.persist();
        Person studentWithoutAccess = TestUtils.createPerson("studentWithoutAccess_name", "studentWithoutAccess_first_name", "student2", "studentWithoutAccess_email", TypePerson.STUDENT);
        studentWithoutAccess.persist();
        Person studentOther = TestUtils.createPerson("studentOther_name", "studentOther_first_name", "student3", "studentOther_email", TypePerson.STUDENT);
        studentOther.persist();

        adminId = admin.id;
        teacher1Id = teacherWithAccess.id;
        student1Id = studentWithAccess.id;

        Group g1 = new Group("g1");
        g1.persist();
        Group g2 = new Group("g2");
        g2.persist();

        studentWithAccess.group = g1;
        studentOther.group = g1;
        studentWithoutAccess.group = g2;

        Field f = new Field("fieldName", "description", "icon");
        f.persist();
        f.teachers = Arrays.asList(new FieldPerson(f, teacherWithAccess), new FieldPerson(f, teacherOther));
        f.groups = Collections.singletonList(new FieldGroup(f, g1));

        Lesson l = new Lesson("title", "description", aDayBefore, aDayAfter, false, 0, f);
        l.persist();
        Lesson hiddenLesson = new Lesson("hidden task", "description", aDayBefore, aDayAfter, true, 1, f);
        hiddenLesson.persist();

        Task t1 = new Task("t1", "description", aDayBefore, aDayAfter, true, true, l);
        t1.persist();
        Task t2 = new Task("t2", "description", aDayBefore, aDayAfter, true, true, l);
        t2.persist();
        Task hiddenTask = new Task("hidden task", "description", aDayBefore, aDayAfter, false, true, hiddenLesson);
        hiddenTask.persist();
        hiddenTaskId = hiddenTask.id;

        t1Id = t1.id;
        t2Id = t2.id;

        Work w1 = TestUtils.createWork(studentOther, t2);
        alreadyTakenFilename = w1.fileName;

        file = new File("file2.txt");
        try {
            file.createNewFile();
            TestUtils.fillFile(file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = "admin", roles = "ADMIN")
    public void testAdminUpload() {
        final String fileName = "admin_myFile.txt";
        given()
                .contentType("multipart/form-data")
                .multiPart("file", file, "multipart/form-data")
                .formParam("file_name", fileName, "multipart/form-data")
                .formParam("task", t1Id, "multipart/form-data")
                .post("/works")
                .then()
                .statusCode(200)
                .log().all()
                .body("$", hasKey("id"),
                        "task_id", is((int) t1Id),
                        "file_name", is(WorkJson.formatFilename(adminId, fileName)),
                        "$", hasKey("rendering_date"),
                        "user.login", is("admin"),
                        "user.name", is("admin_name"),
                        "user.first_name", is("admin_first_name"),
                        "user.email", is("admin_email"),
                        "user.group", equalTo(null),
                        "user.type", is("ADMIN")
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testTeacherUpload() {
        final String fileName = "teacher1_myFile.txt";
        given()
                .contentType("multipart/form-data")
                .multiPart("file", file, "multipart/form-data")
                .formParam("file_name", fileName, "multipart/form-data")
                .formParam("task", t1Id, "multipart/form-data")
                .post("/works")
                .then()
                .statusCode(200)
                .log().all()
                .body("$", hasKey("id"),
                        "task_id", is((int) t1Id),
                        "file_name", is(WorkJson.formatFilename(teacher1Id, fileName)),
                        "$", hasKey("rendering_date"),
                        "user.login", is("teacher1"),
                        "user.name", is("teacherWithAccess_name"),
                        "user.first_name", is("teacherWithAccess_first_name"),
                        "user.email", is("teacherWithAccess_email"),
                        "user.group", equalTo(null),
                        "user.type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "teacher1", roles = "TEACHER")
    public void testTeacherUploadWhileLessonAndTaskNotVisible() {
        final String fileName = "teacher1_myFile.txt";
        given()
                .contentType("multipart/form-data")
                .multiPart("file", file, "multipart/form-data")
                .formParam("file_name", fileName, "multipart/form-data")
                .formParam("task", hiddenTaskId, "multipart/form-data")
                .post("/works")
                .then()
                .statusCode(200)
                .log().all()
                .body("$", hasKey("id"),
                        "task_id", is((int) hiddenTaskId),
                        "file_name", is(WorkJson.formatFilename(teacher1Id, fileName)),
                        "$", hasKey("rendering_date"),
                        "user.login", is("teacher1"),
                        "user.name", is("teacherWithAccess_name"),
                        "user.first_name", is("teacherWithAccess_first_name"),
                        "user.email", is("teacherWithAccess_email"),
                        "user.group", equalTo(null),
                        "user.type", is("TEACHER")
                );
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testStudentUpload() {
        final String fileName = "student1_myFile.txt";
        given()
                .contentType("multipart/form-data")
                .multiPart("file", file, "multipart/form-data")
                .formParam("file_name", fileName, "multipart/form-data")
                .formParam("task", t1Id, "multipart/form-data")
                .post("/works")
                .then()
                .statusCode(200)
                .log().all()
                .body("$", hasKey("id"),
                        "task_id", is((int) t1Id),
                        "file_name", is(WorkJson.formatFilename(student1Id, fileName)),
                        "$", hasKey("rendering_date"),
                        "user.login", is("student1"),
                        "user.name", is("studentWithAccess_name"),
                        "user.first_name", is("studentWithAccess_first_name"),
                        "user.email", is("studentWithAccess_email"),
                        "user.group", is("g1"),
                        "user.type", is("STUDENT")
                );
    }

    @Test
    @TestSecurity(user = "student1", roles = "STUDENT")
    public void testStudentUploadUnknownTask() {
        final String fileName = "student1_myFile.txt";
        given()
                .contentType("multipart/form-data")
                .multiPart("file", file, "multipart/form-data")
                .formParam("file_name", fileName, "multipart/form-data")
                .formParam("task", 999, "multipart/form-data")
                .post("/works")
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));
    }
}
