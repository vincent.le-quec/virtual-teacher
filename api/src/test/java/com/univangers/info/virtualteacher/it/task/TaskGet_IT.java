package com.univangers.info.virtualteacher.it.task;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskGet_IT {

    private static final Date aDayBefore = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private static final Date aDayAfter = Date.from(LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentWithAccessLogin = "m.alexis",
            studentWithoutAccessLogin = "d.jean",
            adminLogin = "p.nom";

    private Task task, task1, hiddenTask, taskLessonHidden;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();
        final Group unusedGroup = new Group("unusedGroup");
        unusedGroup.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person studentWithAccess = new Person("alexis", "migniau", studentWithAccessLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        studentWithAccess.group = group;
        studentWithAccess.persist();

        Person studentWithoutAccess = new Person("jean", "dupont", studentWithoutAccessLogin, "d.jean@gmail.com", TypePerson.STUDENT);
        studentWithoutAccess.group = unusedGroup;
        studentWithoutAccess.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        final Lesson lesson = new Lesson("Algorithmie",
                "Cours d'algo",
                aDayBefore,
                aDayAfter,
                false,
                0,
                fieldAccess,
                null,
                null);

        lesson.persist();

        task = new Task("Ex1",
                "Faire l'ex1",
                aDayBefore,
                aDayAfter,
                true,
                true,
                lesson);

        task.persist();

        task1 = new Task("Ex2",
                "Faire l'ex2",
                aDayBefore,
                aDayAfter,
                true,
                true,
                lesson);

        task1.persist();

        hiddenTask = new Task("hidden",
                "hidden",
                aDayAfter,
                aDayAfter,
                true,
                false,
                lesson);

        hiddenTask.persist();

        final Lesson hiddenLesson = new Lesson("hidden", "Cours d'algo", aDayBefore, aDayAfter, true, 0, fieldAccess, null, null);
        hiddenLesson.persist();
        taskLessonHidden = new Task("task", "Some task", aDayBefore, aDayAfter, true, true, hiddenLesson);
        taskLessonHidden.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetAllAdmin() {
        given()
                .when()
                .get("/tasks")
                .then()
                .statusCode(200)
                .body("size()", is(4),

                        "[0].title", is(task.title),
                        "[0].description", is(task.description),
                        "[0].beginning", is(task.beginning.getTime()),
                        "[0].ending", is(task.ending.getTime()),
                        "[0].displayed", is(task.displayed),
                        "[0].contains_work", is(task.containsWork),
                        "[0].id_lesson", is((int) task.lesson.id),

                        "[1].title", is(task1.title),
                        "[1].description", is(task1.description),
                        "[1].beginning", is(task1.beginning.getTime()),
                        "[1].ending", is(task1.ending.getTime()),
                        "[1].displayed", is(task1.displayed),
                        "[1].contains_work", is(task1.containsWork),
                        "[1].id_lesson", is((int) task1.lesson.id),

                        "[2].title", is(hiddenTask.title),
                        "[2].description", is(hiddenTask.description),
                        "[2].beginning", is(hiddenTask.beginning.getTime()),
                        "[2].ending", is(hiddenTask.ending.getTime()),
                        "[2].displayed", is(hiddenTask.displayed),
                        "[2].contains_work", is(hiddenTask.containsWork),
                        "[2].id_lesson", is((int) hiddenTask.lesson.id),

                        "[3].title", is(taskLessonHidden.title),
                        "[3].description", is(taskLessonHidden.description),
                        "[3].beginning", is(taskLessonHidden.beginning.getTime()),
                        "[3].ending", is(taskLessonHidden.ending.getTime()),
                        "[3].displayed", is(taskLessonHidden.displayed),
                        "[3].contains_work", is(taskLessonHidden.containsWork),
                        "[3].id_lesson", is((int) taskLessonHidden.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetAllTeacherWithAccess() {
        given()
                .when()
                .get("/tasks")
                .then()
                .statusCode(200)
                .body("size()", is(4),

                        "[0].title", is(task.title),
                        "[0].description", is(task.description),
                        "[0].beginning", is(task.beginning.getTime()),
                        "[0].ending", is(task.ending.getTime()),
                        "[0].displayed", is(task.displayed),
                        "[0].contains_work", is(task.containsWork),
                        "[0].id_lesson", is((int) task.lesson.id),

                        "[1].title", is(task1.title),
                        "[1].description", is(task1.description),
                        "[1].beginning", is(task1.beginning.getTime()),
                        "[1].ending", is(task1.ending.getTime()),
                        "[1].displayed", is(task1.displayed),
                        "[1].contains_work", is(task1.containsWork),
                        "[1].id_lesson", is((int) task1.lesson.id),

                        "[2].title", is(hiddenTask.title),
                        "[2].description", is(hiddenTask.description),
                        "[2].beginning", is(hiddenTask.beginning.getTime()),
                        "[2].ending", is(hiddenTask.ending.getTime()),
                        "[2].displayed", is(hiddenTask.displayed),
                        "[2].contains_work", is(hiddenTask.containsWork),
                        "[2].id_lesson", is((int) hiddenTask.lesson.id),

                        "[3].title", is(taskLessonHidden.title),
                        "[3].description", is(taskLessonHidden.description),
                        "[3].beginning", is(taskLessonHidden.beginning.getTime()),
                        "[3].ending", is(taskLessonHidden.ending.getTime()),
                        "[3].displayed", is(taskLessonHidden.displayed),
                        "[3].contains_work", is(taskLessonHidden.containsWork),
                        "[3].id_lesson", is((int) taskLessonHidden.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testGetAllTeacherWithoutAccess() {
        given()
                .when()
                .get("/tasks")
                .then()
                .statusCode(200)
                .body("size()", is(0));
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testGetAllStudentWithAccess() {
        given()
                .when()
                .get("/tasks")
                .then()
                .statusCode(200)
                .body("size()", is(2),

                        "[0].title", is(task.title),
                        "[0].description", is(task.description),
                        "[0].beginning", is(task.beginning.getTime()),
                        "[0].ending", is(task.ending.getTime()),
                        "[0].displayed", is(task.displayed),
                        "[0].contains_work", is(task.containsWork),
                        "[0].id_lesson", is((int) task.lesson.id),

                        "[1].title", is(task1.title),
                        "[1].description", is(task1.description),
                        "[1].beginning", is(task1.beginning.getTime()),
                        "[1].ending", is(task1.ending.getTime()),
                        "[1].displayed", is(task1.displayed),
                        "[1].contains_work", is(task1.containsWork),
                        "[1].id_lesson", is((int) task1.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = studentWithoutAccessLogin, roles = "STUDENT")
    public void testGetAllStudentWithoutAccess() {
        given()
                .when()
                .get("/tasks")
                .then()
                .statusCode(200)
                .body("size()", is(0));
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetTeacherWithAccess() {
        given()
                .when()
                .get("/tasks/" + task.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(task.title),
                        "description", is(task.description),
                        "beginning", is(task.beginning.getTime()),
                        "ending", is(task.ending.getTime()),
                        "displayed", is(task.displayed),
                        "contains_work", is(task.containsWork),
                        "id_lesson", is((int) task.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testGetTeacherToHiddenTask() {
        given()
                .when()
                .get("/tasks/" + hiddenTask.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(hiddenTask.title),
                        "description", is(hiddenTask.description),
                        "beginning", is(hiddenTask.beginning.getTime()),
                        "ending", is(hiddenTask.ending.getTime()),
                        "displayed", is(hiddenTask.displayed),
                        "contains_work", is(hiddenTask.containsWork),
                        "id_lesson", is((int) hiddenTask.lesson.id)
                );

    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testGetStudentWithAccess() {
        given()
                .when()
                .get("/tasks/" + task.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(task.title),
                        "description", is(task.description),
                        "beginning", is(task.beginning.getTime()),
                        "ending", is(task.ending.getTime()),
                        "displayed", is(task.displayed),
                        "contains_work", is(task.containsWork),
                        "id_lesson", is((int) task.lesson.id)
                );

    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testGetStudentToHiddenTask() {
        given()
                .when()
                .get("/tasks/" + hiddenTask.id)
                .then()
                .statusCode(404)
                .body(is("[m.alexis] verifyHaveAccess : unknown task with id '" + hiddenTask.id + "'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetAdmin() {
        given()
                .when()
                .get("/tasks/" + task.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(task.title),
                        "description", is(task.description),
                        "beginning", is(task.beginning.getTime()),
                        "ending", is(task.ending.getTime()),
                        "displayed", is(task.displayed),
                        "contains_work", is(task.containsWork),
                        "id_lesson", is((int) task.lesson.id)
                );

    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetAdminToHiddenTask() {
        given()
                .when()
                .get("/tasks/" + hiddenTask.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(hiddenTask.title),
                        "description", is(hiddenTask.description),
                        "beginning", is(hiddenTask.beginning.getTime()),
                        "ending", is(hiddenTask.ending.getTime()),
                        "displayed", is(hiddenTask.displayed),
                        "contains_work", is(hiddenTask.containsWork),
                        "id_lesson", is((int) hiddenTask.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testGetTeacherWithoutAccess() {
        given()
                .when()
                .get("/tasks/" + task.id)
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + task.lesson.field.id + "'"));
    }

    @Test
    @TestSecurity(user = studentWithoutAccessLogin, roles = "STUDENT")
    public void testGetStudentWithoutAccess() {
        given()
                .when()
                .get("/tasks/" + task.id)
                .then()
                .statusCode(403)
                .body(is("[d.jean] verifyHaveAccessToField : access refused to field with id '" + task.lesson.field.id + "'"));
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testGetTaskNotExist() {
        given()
                .when()
                .get("/tasks/" + 999)
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testTeacherGetTaskForHiddenLesson() {
        given()
                .when()
                .get("/tasks/" + taskLessonHidden.id)
                .then()
                .statusCode(200)
                .body(
                        "title", is(taskLessonHidden.title),
                        "description", is(taskLessonHidden.description),
                        "beginning", is(taskLessonHidden.beginning.getTime()),
                        "ending", is(taskLessonHidden.ending.getTime()),
                        "displayed", is(taskLessonHidden.displayed),
                        "contains_work", is(taskLessonHidden.containsWork),
                        "id_lesson", is((int) taskLessonHidden.lesson.id)
                );
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "TEACHER")
    public void testStudentGetTaskForHiddenLesson() {
        given()
                .when()
                .get("/tasks/" + taskLessonHidden.id)
                .then()
                .statusCode(404)
                .body(is("[" + studentWithAccessLogin + "] Unknown lesson with id '" + taskLessonHidden.lesson.id + "'"));
    }
}
