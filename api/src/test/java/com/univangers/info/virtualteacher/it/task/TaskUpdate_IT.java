package com.univangers.info.virtualteacher.it.task;

import com.univangers.info.virtualteacher.core.*;
import com.univangers.info.virtualteacher.it.TestUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskUpdate_IT {

    private final String teacherWithAccessLogin = "j.dupont",
            teacherWithoutAccessLogin = "m.jean",
            studentWithAccessLogin = "m.alexis",
            adminLogin = "p.nom";

    private Task task1, task2, task3;

    @BeforeAll
    @Transactional
    public void before() {
        final Group group = new Group("group");
        group.persist();

        // Create person
        Person teacherWithAccess = new Person("jean", "dupont", teacherWithAccessLogin, "j.dupont@gmail.com", TypePerson.TEACHER);
        teacherWithAccess.persist();

        Field fieldAccess = new Field("informatique", "cours d'info", "path.jpg", null, null, null);
        fieldAccess.persist();

        Person teacherWithoutAccess = new Person("michel", "jean", teacherWithoutAccessLogin, "m.jean@gmail.com", TypePerson.TEACHER);
        teacherWithoutAccess.persist();

        Person studentWithAccess = new Person("alexis", "migniau", studentWithAccessLogin, "m.alexis@gmail.com", TypePerson.STUDENT);
        studentWithAccess.group = group;
        studentWithAccess.persist();

        Person admin = new Person("pascal", "nom", adminLogin, "p.nom@gmail.com", TypePerson.ADMIN);
        admin.persist();

        // Create access
        final FieldPerson fieldTeacher = new FieldPerson(fieldAccess, teacherWithAccess);
        fieldTeacher.persist();

        final FieldGroup fieldStudent = new FieldGroup(fieldAccess, group);
        fieldStudent.persist();

        // Create lesson
        final Lesson lesson = new Lesson("Algorithmie",
                "Cours d'algo",
                new Date(),
                new Date(),
                false,
                0,
                fieldAccess,
                null,
                null);

        lesson.persist();

        task1 = new Task("Ex1",
                "Faire l'ex1",
                new Date(),
                new Date(),
                true,
                true,
                lesson);

        task1.persist();

        task2 = new Task("Ex2",
                "Faire l'ex2",
                new Date(),
                new Date(),
                true,
                true,
                lesson);

        task2.persist();

        task3 = new Task("Ex3",
                "Faire l'ex3",
                new Date(),
                new Date(),
                true,
                true,
                lesson);

        task3.persist();
    }

    @AfterAll
    @Transactional
    public void after() {
        TestUtils.deleteDB();
    }

    @Test
    @TestSecurity(user = teacherWithAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithAccess() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                task1.lesson.id,
                "Changement du titre",
                task1.description,
                task1.beginning,
                task1.ending,
                task1.displayed,
                task1.containsWork
        );

        contract.setId(task1.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/tasks")
                .then()
                .statusCode(200)
                .body(
                        "title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "displayed", is(contract.getDisplayed()),
                        "contains_work", is(contract.getContainsWork()),
                        "id_lesson", is((int) contract.getIdLesson())
                );

        // Then
        final Optional<Task> optional = Task.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testUpdateAdmin() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                task2.lesson.id,
                "Changement du titre",
                task2.description,
                task2.beginning,
                task2.ending,
                task2.displayed,
                task2.containsWork
        );

        contract.setId(task2.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/tasks")
                .then()
                .statusCode(200)
                .body(
                        "title", is(contract.getTitle()),
                        "description", is(contract.getDescription()),
                        "beginning", is(contract.getBeginning().getTime()),
                        "ending", is(contract.getEnding().getTime()),
                        "displayed", is(contract.getDisplayed()),
                        "contains_work", is(contract.getContainsWork()),
                        "id_lesson", is((int) contract.getIdLesson())
                );

        // Then
        final Optional<Task> optional = Task.findByIdOptional(contract.getId());
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(contract.getTitle(), core.title);
        Assertions.assertEquals(contract.getDescription(), core.description);
        Assertions.assertEquals(contract.getBeginning(), core.beginning);
        Assertions.assertEquals(contract.getEnding(), core.ending);
        Assertions.assertEquals(contract.getDisplayed(), core.displayed);
        Assertions.assertEquals(contract.getIdLesson(), core.lesson.id);
    }

    @Test
    @TestSecurity(user = teacherWithoutAccessLogin, roles = "TEACHER")
    public void testUpdateTeacherWithoutAccess() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                task3.lesson.id,
                task3.title,
                "Changement de la description",
                task3.beginning,
                task3.ending,
                task3.displayed,
                task3.containsWork
        );

        contract.setId(task3.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/tasks")
                .then()
                .statusCode(403)
                .body(is("[m.jean] verifyHaveAccessToField : access refused to field with id '" + task3.lesson.field.id + "'"));

        // Then
        final Optional<Task> optional = Task.findByIdOptional(task3.id);
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(task3.title, core.title);
        Assertions.assertEquals(task3.description, core.description);
        Assertions.assertEquals(task3.beginning, core.beginning);
        Assertions.assertEquals(task3.ending, core.ending);
        Assertions.assertEquals(task3.displayed, core.displayed);
        Assertions.assertEquals(task3.containsWork, core.containsWork);
        Assertions.assertEquals(task3.lesson.id, core.lesson.id);
    }

    @Test
    @TestSecurity(user = studentWithAccessLogin, roles = "STUDENT")
    public void testUpdateStudent() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                task2.lesson.id,
                task2.title,
                "Changement de description",
                task2.beginning,
                task2.ending,
                task2.displayed,
                task2.containsWork
        );

        contract.setId(task2.id);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/tasks")
                .then()
                .statusCode(403)
                .body(is(""));

        // Then
        final Optional<Task> optional = Task.findByIdOptional(task2.id);
        Assertions.assertTrue(optional.isPresent());
        final Task core = optional.get();
        Assertions.assertEquals(task2.title, core.title);
        Assertions.assertEquals(task2.description, core.description);
        Assertions.assertEquals(task2.beginning, core.beginning);
        Assertions.assertEquals(task2.ending, core.ending);
        Assertions.assertEquals(task2.displayed, core.displayed);
        Assertions.assertEquals(task2.containsWork, core.containsWork);
        Assertions.assertEquals(task2.lesson.id, core.lesson.id);
    }

    @Test
    @TestSecurity(user = adminLogin, roles = "ADMIN")
    public void testTaskNotExist() {
        final com.univangers.info.virtualteacher.contract.task.Task contract = new com.univangers.info.virtualteacher.contract.task.Task(
                task2.lesson.id,
                task2.title,
                "Changement de description",
                task2.beginning,
                task2.ending,
                task2.displayed,
                task2.containsWork
        );

        contract.setId(999);

        given()
                .contentType(ContentType.JSON)
                .body(contract)
                .when()
                .patch("/tasks")
                .then()
                .statusCode(404)
                .body(is("Unknown task with id '999'"));
    }
}
