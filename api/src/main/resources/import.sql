INSERT INTO groupe(title) VALUES
('2018TL3INF1'),
('2019TM1INF1'),
('2020TM2ACD1');

INSERT INTO person(name, first_name, login, email, type_person, group_id) VALUES
('Tesla', 'tesla', 'tesla', 'tesla@ldap.forumsys.com', 2, null),
('Einstein', 'einstein', 'einstein', 'einstein@ldap.forumsys.com', 1, null),
('Euler', 'euler', 'euler', 'euler@ldap.forumsys.com', 1, null),
('Riemann', 'riemann', 'riemann', 'riemann@ldap.forumsys.com', 0, (SELECT id FROM groupe WHERE title='2020TM2ACD1')),
('Gauss', 'gauss', 'gauss', 'gauss@ldap.forumsys.com', 0, (SELECT id FROM groupe WHERE title='2020TM2ACD1')),
('Euclid', 'euclid', 'euclid', 'euclid@ldap.forumsys.com', 0, (SELECT id FROM groupe WHERE title='2019TM1INF1')),
('Newton', 'newton', 'newton', 'newton@ldap.forumsys.com', 0, (SELECT id FROM groupe WHERE title='2019TM1INF1')),
('Admin', 'admin', 'admin', 'newton@ldap.forumsys.com', 2, null),
('Galilei', 'galieleo', 'galieleo', 'galieleo@ldap.forumsys.com', 0, (SELECT id FROM groupe WHERE title='2018TL3INF1'));

INSERT INTO field(field_name, description, icon) VALUES
('CUDA', 'Un cours sur CUDA. Lorem ipsum dolor sit amet.', 'mdi-school'),
('Android', 'Un cours sur Android. Lorem ipsum dolor sit amet.', 'mdi-science'),
('Management de Projets', 'Lorem ipsum dolor sit amet.', 'mdi-school'),
('Objets connectés', 'Lorem ipsum dolor sit amet.', 'mdi-cloud-upload'),
('Design Pattern', 'Un cours sur les Design Pattern. Lorem ipsum dolor sit amet.', 'mdi-science');

INSERT INTO field_person(field_id, person_id) VALUES
((SELECT id FROM field WHERE field_name='CUDA'), (SELECT id FROM person WHERE login='tesla')),
((SELECT id FROM field WHERE field_name='CUDA'), (SELECT id FROM person WHERE login='einstein')),
((SELECT id FROM field WHERE field_name='Android'), (SELECT id FROM person WHERE login='tesla')),
((SELECT id FROM field WHERE field_name='Android'), (SELECT id FROM person WHERE login='einstein')),
((SELECT id FROM field WHERE field_name='Management de Projets'), (SELECT id FROM person WHERE login='einstein')),
((SELECT id FROM field WHERE field_name='Management de Projets'), (SELECT id FROM person WHERE login='tesla')),
((SELECT id FROM field WHERE field_name='Objets connectés'), (SELECT id FROM person WHERE login='euler')),
((SELECT id FROM field WHERE field_name='Design Pattern'), (SELECT id FROM person WHERE login='einstein'));

INSERT INTO lesson(title, type_lesson, description, lesson_order, beginning, ending, hide, field_id) VALUES
('Introduction et historique', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 1, '2020-10-22 14:00:00-00', '2021-12-30 14:00:00-00', false, (SELECT id FROM field WHERE field_name='CUDA')),
('Programmation avec CUDA', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 2, '2020-10-22 14:00:00-00', '2021-12-30 14:00:00-00', false, (SELECT id FROM field WHERE field_name='CUDA')),
('TP1', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 3, '2020-10-22 14:00:00-00', '2021-12-30 14:00:00-00', false, (SELECT id FROM field WHERE field_name='CUDA')),
('Le modèle logique', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 4, '2020-11-1 14:00:00-00', '2021-12-30 14:00:00-00', false, (SELECT id FROM field WHERE field_name='CUDA')),
('Examen', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 5, '2020-11-2 14:00:00-00', '2021-12-30 14:00:00-00', false, (SELECT id FROM field WHERE field_name='CUDA'));

INSERT INTO paragraph(title, title_size, content, paragraph_order, type, lesson_id) VALUES
('Introduction', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, (SELECT id FROM lesson WHERE title='Introduction et historique')),
('Première sous partie ', 2, 'Lorem ipslessonsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2, 0, (SELECT id FROM lesson WHERE title='Introduction et historique')),
('Deuxième sous partie ', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 3, 0, (SELECT id FROM lesson WHERE title='Introduction et historique'));

INSERT INTO task(title, description, beginning, ending, displayed, contains_work, lesson_id) VALUES
('task 1', 'ceci est une description', '2020-08-22 19:10:25-07', '2021-06-22 19:10:25-07', true, false, (SELECT id from lesson where title='Introduction et historique')),
('task 2', 'une nouvelle description', '2020-08-22 19:10:25-07', '2021-06-22 19:10:25-07', true, false, (SELECT id from lesson where title='Introduction et historique')),
('task 3', 'Et bosser...', '2020-08-22 19:10:25-07', '2021-06-22 19:10:25-07', true, false, (SELECT id from lesson where title='Programmation avec CUDA')),
('TP1', 'faites le TP', '2020-08-22 19:10:25-07', '2021-06-22 19:10:25-07', true, true, (SELECT id from lesson where title='TP1')),
('Exam', 'bosser vos exam', '2020-08-22 19:10:25-07', '2021-06-22 19:10:25-07', true, true, (SELECT id from lesson where title='Examen'));

INSERT INTO field_group(field_id, group_id) VALUES
((SELECT id FROM field WHERE field_name='CUDA'), (SELECT id FROM groupe WHERE title='2020TM2ACD1')),
((SELECT id FROM field WHERE field_name='Android'), (SELECT id FROM groupe WHERE title='2020TM2ACD1')),
((SELECT id FROM field WHERE field_name='Android'), (SELECT id FROM groupe WHERE title='2019TM1INF1'));
