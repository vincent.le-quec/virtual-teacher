package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
@Table(name = "field_group", schema = "PUBLIC")
public class FieldGroup extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @ManyToOne
    public Field field;

    @ManyToOne
    public Group group;

    public FieldGroup() {
    }

    public FieldGroup(Field field, Group group) {
        this.field = field;
        this.group = group;
    }
}
