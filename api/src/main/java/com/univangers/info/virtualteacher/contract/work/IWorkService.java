package com.univangers.info.virtualteacher.contract.work;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import io.quarkus.security.Authenticated;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/works")
@Authenticated
public interface IWorkService {

    @GET
    @Path("/{task_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    Response fetch(@PathParam("task_id") long taskId);

    @GET
    @Path("/{task_id}/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    Response download(@PathParam("task_id") long taskId);

    @GET
    @Path("/{task_id}/download/{work_id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    Response downloadWork(@PathParam("task_id") long lessonId,
                          @PathParam("work_id") long workId);

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    Response upload(@MultipartForm WorkFile work) throws IOException;

    @DELETE
    @Path("/{task_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    Response delete(@PathParam("task_id") long taskId);
}