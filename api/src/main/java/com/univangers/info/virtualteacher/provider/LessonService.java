package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.lesson.ILessonService;
import com.univangers.info.virtualteacher.contract.lesson.Lesson;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;
import com.univangers.info.virtualteacher.provider.mapper.lesson.LessonMapper;
import com.univangers.info.virtualteacher.provider.utils.FieldUtils;
import com.univangers.info.virtualteacher.provider.utils.LessonUtils;
import io.quarkus.security.Authenticated;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
@Authenticated
public class LessonService implements ILessonService {

    private static final Logger LOGGER = Logger.getLogger(LessonService.class);

    @Inject
    IUserService userService;

    @Override
    public Response fetchAll(long fieldId) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetchAll : field_id='" + fieldId + "'");

        // Verification
        final Field field = FieldUtils.fetchField(fieldId);
        FieldUtils.verifyHaveAccessToField(fieldId, p);

        // Process
        final List<Lesson> contract = field.lessons
                .stream()
                .filter(l -> l.toDisplay() || p.typePerson != TypePerson.STUDENT)
                .map(LessonMapper::toContract)
                .sorted(Comparator.comparingInt(Lesson::getOrder))
                .collect(Collectors.toList());

        LOGGER.info("[" + p.login + "] fetchAll : return every lessons for field with id '" + fieldId + "'");
        return Response.ok(contract).build();
    }

    @Override
    public Response fetch(long lessonId) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetch : lesson_id='" + lessonId + "'");

        // Verification
        final com.univangers.info.virtualteacher.core.Lesson core = LessonUtils.fetchLesson(lessonId);
        FieldUtils.verifyHaveAccessToField(core.field.id, p);

        // Process
        final Lesson contract = LessonMapper.toContract(core);

        LOGGER.info("[" + p.login + "] fetch : return lesson : " + contract);
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response add(Lesson lesson) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] add : lesson=" + lesson);

        // Verification
        LessonUtils.validateNewContract(lesson);
        final Field field = FieldUtils.fetchField(lesson.getFieldId());
        FieldUtils.verifyHaveAccessToField(lesson.getFieldId(), p);

        // Process
        final com.univangers.info.virtualteacher.core.Lesson core = LessonMapper.fromContract(lesson);
        core.field = field;
        core.persist();

        // Respond
        final Lesson contract = LessonMapper.toContract(core);
        LOGGER.info("[" + p.login + "] add : lesson added : " + contract);
        return Response.status(Response.Status.CREATED).entity(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response update(Lesson lesson) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] update : lesson=" + lesson);

        // VERIFICATION
        LessonUtils.validateContract(lesson); // field + titre
        final com.univangers.info.virtualteacher.core.Lesson core = LessonUtils.fetchLesson(lesson.getId());
        FieldUtils.verifyHaveAccessToField(core.field.id, p);

        // Check if we change Field we check if the field exist and if we have access to the new field
        if (lesson.getFieldId() != null && core.field.id != lesson.getFieldId()) {
            throw new BadRequestException("[" + p.login + "] update : field id cannot be updated");
        }

        // PROCESS
        LessonMapper.fromContract(lesson, core);

        // RESPOND
        final Lesson contract = LessonMapper.toContract(core);
        LOGGER.info("[" + p.login + "] update : lesson updated : " + contract);
        return Response.ok(contract).build();

    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class})
    public Response delete(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] delete : id=" + id);

        // Verification
        final com.univangers.info.virtualteacher.core.Lesson lesson = LessonUtils.fetchLesson(id);
        FieldUtils.verifyHaveAccessToField(lesson.field.id, p);

        // Process
        lesson.delete();

        // Respond
        LOGGER.info("[" + p.login + "] delete : lesson with id '" + id + "' has been deleted");
        return Response.ok().build();
    }
}
