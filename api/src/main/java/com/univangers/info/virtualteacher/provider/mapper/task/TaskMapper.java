package com.univangers.info.virtualteacher.provider.mapper.task;

import com.univangers.info.virtualteacher.core.Task;

public class TaskMapper {
    public static Task fromContract(final com.univangers.info.virtualteacher.contract.task.Task contract) {
        return new Task(
                contract.getTitle(),
                contract.getDescription(),
                contract.getBeginning(),
                contract.getEnding(),
                contract.getDisplayed() == null || contract.getDisplayed(),
                contract.getContainsWork() != null && contract.getContainsWork()
        );
    }

    public static Task fromContract(final com.univangers.info.virtualteacher.contract.task.Task contract, final Task core) {
        if (contract.getTitle() != null) core.title = contract.getTitle();
        if (contract.getDescription() != null) core.description = contract.getDescription();
        if (contract.getBeginning() != null) core.beginning = contract.getBeginning();
        if (contract.getEnding() != null) core.ending = contract.getEnding();
        if (contract.getDisplayed() != null) core.displayed = contract.getDisplayed();
        if (contract.getContainsWork() != null) core.containsWork = contract.getContainsWork();

        return core;
    }

    public static com.univangers.info.virtualteacher.contract.task.Task toContract(final Task core) {
        return new com.univangers.info.virtualteacher.contract.task.Task(core.id, core.lesson.id, core.title, core.description, core.beginning, core.ending, core.displayed, core.containsWork);
    }
}
