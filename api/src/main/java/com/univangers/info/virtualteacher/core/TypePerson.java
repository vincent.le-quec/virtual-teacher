package com.univangers.info.virtualteacher.core;

public enum TypePerson {
    STUDENT,
    TEACHER,
    ADMIN
}
