package com.univangers.info.virtualteacher.provider.mapper.lesson;

import com.univangers.info.virtualteacher.core.Paragraph;
import com.univangers.info.virtualteacher.core.TypeFile;

public class LessonParagraphMapper {

    public static Paragraph fromContract(com.univangers.info.virtualteacher.contract.lesson.Paragraph contract) {
        return new Paragraph(
                contract.getTitle(),
                contract.getTitleSize() != null ? contract.getTitleSize() : 0,
                contract.getContent(),
                contract.getOrder(), TypeFile.IMAGE.getType().equals(contract.getType()) ? TypeFile.IMAGE : TypeFile.TXT);
    }

    public static com.univangers.info.virtualteacher.contract.lesson.Paragraph toContract(final Paragraph core) {
        return new com.univangers.info.virtualteacher.contract.lesson.Paragraph(
                core.id,
                core.title,
                core.titleSize,
                core.content,
                core.order,
                core.type.getType());
    }
}
