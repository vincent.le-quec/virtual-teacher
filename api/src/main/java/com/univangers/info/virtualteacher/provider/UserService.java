package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.user.Credentials;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Group;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.*;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@RequestScoped
public class UserService implements IUserService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class);

    @ConfigProperty(name = "admin.list")
    String adminsConfig;

    @ConfigProperty(name = "admin.login", defaultValue = "admin")
    String adminLogin;

    @ConfigProperty(name = "admin.password")
    String adminPassword;

    @ConfigProperty(name = "ldap.url")
    String ldapUrl;

    @ConfigProperty(name = "ldap.search-base-dn")
    String ldapBaseDn;

    @ConfigProperty(name = "ldap.filter-dn")
    String ldapFilter;

    @ConfigProperty(name = "smallrye.jwt.duration", defaultValue = "3600")
    int tokenDuration;

    @Inject
    SecurityIdentity identity;

    @Override
    @Authenticated
    public Response fetch() {
        final Person p = getPerson();
        LOGGER.info("[" + p.login + "] fetch");
        return Response.ok(PersonMapper.toContract(p)).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    public Response getAllTeachers() {
        final Person p = getPerson();
        LOGGER.info("[" + p.login + "] getAllTeachers");

        final List<com.univangers.info.virtualteacher.contract.Person> contracts = Person.getAll()
                .stream()
                .filter(person -> person.typePerson != TypePerson.STUDENT)
                .map(PersonMapper::toContract)
                .sorted(Comparator.comparing(com.univangers.info.virtualteacher.contract.Person::getName))
                .collect(Collectors.toList());

        LOGGER.info("[" + p.login + "] getAllTeachers : return every teachers");
        return Response.ok(contracts).build();
    }

    @Override
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response authenticate(Credentials cred) {
        LOGGER.info("[" + cred.getLogin() + "] authenticate");

        // get user for login / password
        // TODO remove this temporary code
//        final Person p = Person.findByLogin(cred.getLogin());
        final Person p = connect(cred.getLogin(), cred.getPassword());
        LOGGER.info("[" + p.login + "] authenticate : authentication succeeded");

        // generate token
        String token =
                Jwt.upn(p.login)
                        .groups(Set.of(p.typePerson.toString()))
                        .expiresAt(Instant.now().getEpochSecond() + tokenDuration)
                        .sign();

        LOGGER.info("[" + p.login + "] authenticate : return authentication token");
        return Response.ok(token).build();
    }

    /**
     * return the person object from the security context
     *
     * @return
     */
    @Override
    public Person getPerson() {
        final String login = identity.getPrincipal().getName();
        final Optional<Person> opt = Person.findByLoginOptional(login);

        return opt.orElseThrow(() -> new NotAuthorizedException("[" + login + "] getPerson : unknown connected user"));
    }

    /**
     * Given a login/password, we verify the person identity with the ldap and return the current user
     *
     * @param login
     * @param password
     * @return
     */
    private Person connect(final String login, final String password) {
        LOGGER.info("[" + login + "] connect");

        // user admin
        if (adminLogin.equals(login) && adminPassword.equals(password)) {
            return Person.findByLoginOptional(adminLogin)
                    .orElseGet(() -> {
                        final Person p = new Person(adminLogin, "", adminLogin, "", TypePerson.ADMIN);
                        p.persist();
                        return p;
                    });
        }

        final String parsedLogin = login.replace("\\", "\\\\").replace("\"", "\\\"");
        final String parsedPassword = password.replace("\\", "\\\\").replace("\"", "\\\"");
        final String cmd = "ldapsearch -x -D 'uid=\"" + parsedLogin + "\"," + ldapBaseDn + "' -w \"" + parsedPassword + "\" -H " + ldapUrl + " -b '" + ldapFilter + "' 'uid=" + login + "'";

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("/bin/bash", "-c", cmd);

        try {
            Process process = processBuilder.start();

            int exitVal = process.waitFor();

            if (exitVal != 0) {
                throw new NotAuthorizedException("[" + login + "] connect : Authentication failed");
            }

            // return person if already in database
            final Optional<Person> optional = Person.findByLoginOptional(login);
            if (optional.isPresent()) {
                return optional.get();
            }

            // read the data to insert this new person
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return extractData(reader);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        throw new InternalServerErrorException("[" + login + "] connect : Unable to connect user with login " + login);
    }

    /**
     * From ldap data creates a Map with our needed data
     *
     * @param reader
     * @return
     * @throws IOException
     */
    public Person extractData(final BufferedReader reader) throws IOException {
        final Map<String, String> map = new HashMap<>();
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(": ")) {
                final String key = line.split(": ")[0];
                final String value = line.split(": ")[1];

                map.put(key, value);
            }
        }
        return mapToPerson(map);
    }

    /**
     * Takes user data and return a new core.Person
     *
     * @param map
     * @return
     */
    private Person mapToPerson(final Map<String, String> map) {

        // get user type
        final List<String> admins = Arrays.stream(adminsConfig.split(";")).collect(Collectors.toList());
        final String login = map.get("uid");
        final String mail = map.get("mail");
        TypePerson type = TypePerson.STUDENT;
        if ("perso".equals(map.get("auaStatut"))) type = TypePerson.TEACHER;
        if (admins.contains(login) || admins.contains(mail)) type = TypePerson.ADMIN;

        // insert the new user
        final Person p = new Person(map.get("sn"), map.get("givenName"), login, mail, type);
        p.persist();

        // set user group
        final Optional<Group> optional = Group.findByByNameOptional(map.get("auaEtapeMillesime"));
        if (optional.isEmpty()) {
            final Group g = new Group(map.get("auaEtapeMillesime"));
            p.group = g;
            g.persist();
        } else {
            p.group = optional.get();
        }

        LOGGER.info("[" + login + "] mapToPerson : new Person inserted : " + p + "");
        return p;
    }
}
