package com.univangers.info.virtualteacher.provider.mapper.field;

import com.univangers.info.virtualteacher.contract.Person;
import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FieldMapper {

    public static Field fromContract(final com.univangers.info.virtualteacher.contract.field.Field contract) {
        return new Field(
                contract.getFieldName(),
                contract.getDescription(),
                contract.getIcon()
        );
    }

    public static Field fromContract(final com.univangers.info.virtualteacher.contract.field.Field contract, final Field core) {
        if (contract.getFieldName() != null) core.fieldName = contract.getFieldName();
        if (contract.getDescription() != null) core.description = contract.getDescription();
        if (contract.getIcon() != null) core.icon = contract.getIcon();
        return core;
    }

    public static com.univangers.info.virtualteacher.contract.field.Field toContract(final Field core) {

        List<String> groups = core.groups
                .stream()
                .map(g -> g.group)
                .map(g -> g.title)
                .sorted()
                .collect(Collectors.toList());

        List<com.univangers.info.virtualteacher.contract.Person> teachers = core.teachers
                .stream()
                .map(fieldPerson -> fieldPerson.person)
                .map(PersonMapper::toContract)
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());

        return new com.univangers.info.virtualteacher.contract.field.Field(core.id, core.fieldName, core.description, core.icon, teachers, groups);
    }
}
