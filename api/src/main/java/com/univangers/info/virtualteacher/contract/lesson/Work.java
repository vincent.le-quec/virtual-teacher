package com.univangers.info.virtualteacher.contract.lesson;

public class Work {
    private long id;
    private String path;

    public Work() {
    }

    public Work(String path) {
        this.path = path;
    }

    public Work(long id, String path) {
        this.id = id;
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
