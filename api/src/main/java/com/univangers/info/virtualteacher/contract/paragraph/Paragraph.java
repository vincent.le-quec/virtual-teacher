package com.univangers.info.virtualteacher.contract.paragraph;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Paragraph {
    private long id;
    private String title;
    @JsonProperty("title_size")
    private Integer titleSize;
    private String content;
    private Integer order;
    private String type;
    @JsonProperty("lesson_id")
    private Long lessonId;

    public Paragraph() {
    }

    public Paragraph(String title, Integer titleSize, String content, Integer order, String type, Long lessonId) {
        this.title = title;
        this.titleSize = titleSize;
        this.content = content;
        this.order = order;
        this.type = type;
        this.lessonId = lessonId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTitleSize() {
        return titleSize;
    }

    public void setTitleSize(Integer titleSize) {
        this.titleSize = titleSize;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    @Override
    public String toString() {
        return "Paragraph{" + "id=" + id + ", title='" + title + '\'' + ", titleSize=" + titleSize + ", content='" + content + '\'' + ", order=" + order + ", type='" + type + '\'' + ", lessonId=" + lessonId + '}';
    }
}
