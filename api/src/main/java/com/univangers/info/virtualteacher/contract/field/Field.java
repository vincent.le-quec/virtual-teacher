package com.univangers.info.virtualteacher.contract.field;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.univangers.info.virtualteacher.contract.Person;

import java.util.ArrayList;
import java.util.List;

public class Field {

    private long id;
    @JsonProperty("field_name")
    private String fieldName;
    private String description;
    private String icon;
    private List<Person> teachers = new ArrayList<>();
    private List<String> groups = new ArrayList<>();

    public Field() {
    }

    public Field(long id, String fieldName, String description, String icon) {
        this.id = id;
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
    }

    public Field(String fieldName, String description, String icon, List<Person> teachers) {
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
        this.teachers = teachers;
    }

    public Field(long id, String fieldName, String description, String icon, List<Person> teachers) {
        this.id = id;
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
        this.teachers = teachers;
    }

    public Field(long id, String fieldName, String description, String icon, List<Person> teachers, List<String> groups) {
        this.id = id;
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
        this.teachers = teachers;
        this.groups = groups;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<Person> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Person> teachers) {
        this.teachers = teachers;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Field{id=" + id + ", fieldName='" + fieldName + '\'' + ", description='" + description + '\'' + ", icon='" + icon + '\'' + ", teachers=" + teachers + ", groups=" + groups + '}';
    }
}
