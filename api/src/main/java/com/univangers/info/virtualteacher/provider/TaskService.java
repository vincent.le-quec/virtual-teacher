package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.task.ITaskService;
import com.univangers.info.virtualteacher.contract.task.Task;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;
import com.univangers.info.virtualteacher.provider.mapper.task.TaskMapper;
import com.univangers.info.virtualteacher.provider.utils.FieldUtils;
import com.univangers.info.virtualteacher.provider.utils.LessonUtils;
import com.univangers.info.virtualteacher.provider.utils.TaskUtils;
import io.quarkus.security.Authenticated;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@ApplicationScoped
@Authenticated
public class TaskService implements ITaskService {

    private static final Logger LOGGER = Logger.getLogger(String.valueOf(TaskService.class));

    @Inject
    IUserService userService;

    @Override
    public Response fetchAll() {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetchAll");

        List<Task> contract;

        // Process
        if (p.typePerson == TypePerson.ADMIN) {
            contract = com.univangers.info.virtualteacher.core.Task
                    .getAll()
                    .stream()
                    .map(TaskMapper::toContract)
                    .sorted(Comparator.comparing(Task::getTitle))
                    .collect(Collectors.toList());
        } else if (p.typePerson == TypePerson.TEACHER) {
            contract = p.fields
                    .stream()
                    .map(e -> e.field)
                    .flatMap(e -> e.lessons.stream())
                    .flatMap(e -> e.tasks.stream())
                    .map(TaskMapper::toContract)
                    .sorted(Comparator.comparing(Task::getTitle))
                    .collect(Collectors.toList());
        } else {
            contract = p.group.fields
                    .stream()
                    .flatMap(e -> e.field.lessons.stream())
                    .filter(Lesson::toDisplay)
                    .flatMap(e -> e.tasks.stream())
                    .filter(com.univangers.info.virtualteacher.core.Task::toDisplay)
                    .map(TaskMapper::toContract)
                    .sorted(Comparator.comparing(Task::getTitle))
                    .collect(Collectors.toList());
        }

        // Respond
        LOGGER.info("[" + p.login + "] fetchAll : return every tasks");
        return Response.ok(contract).build();
    }

    @Override
    public Response fetch(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetch : id=" + id);

        // Verification
        final com.univangers.info.virtualteacher.core.Task core = TaskUtils.fetchTask(id);
        LessonUtils.verifyIsAccessible(core.lesson, p);
        TaskUtils.verifyHaveAccess(core.id, p);

        // Process
        final Task contract = TaskMapper.toContract(core);

        // Respond
        LOGGER.info("[" + p.login + "] fetch : return task with id '" + id + "'");
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response add(Task task) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] add : task=" + task);

        // Verification
        TaskUtils.validateNewContract(task);
        final Lesson lesson = LessonUtils.fetchLesson(task.getIdLesson());
        FieldUtils.verifyHaveAccessToField(lesson.field.id, userService.getPerson());

        // Process
        final com.univangers.info.virtualteacher.core.Task core = TaskMapper.fromContract(task);
        core.lesson = lesson;
        core.persist();

        // Respond
        final Task contract = TaskMapper.toContract(core);
        LOGGER.info("[" + p.login + "] add : task added : contract");
        return Response.status(Response.Status.CREATED).entity(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class, ForbiddenException.class})
    public Response update(Task task) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] update : task=" + task);

        // Verification
        TaskUtils.validateContract(task);
        final com.univangers.info.virtualteacher.core.Task core = TaskUtils.fetchTask(task.getId());
        FieldUtils.verifyHaveAccessToField(core.lesson.field.id, userService.getPerson());

        // We can't change the lesson
        if (task.getIdLesson() != 0 && core.lesson.id != task.getIdLesson())
            throw new BadRequestException("[" + p.login + "] update : task id cannot be updated");

        // Process
        TaskMapper.fromContract(task, core);

        // Respond
        final Task contract = TaskMapper.toContract(core);
        LOGGER.info("[" + p.login + "] update : task updated : " + contract);
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class, BadRequestException.class})
    public Response delete(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] delete : id=" + id);

        // Verification
        final com.univangers.info.virtualteacher.core.Task task = TaskUtils.fetchTask(id);
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, userService.getPerson());

        // Process
        task.delete();

        // Respond
        LOGGER.info("[" + p.login + "] delete : task with id '" + id + "' has been deleted");
        return Response.ok().build();
    }
}
