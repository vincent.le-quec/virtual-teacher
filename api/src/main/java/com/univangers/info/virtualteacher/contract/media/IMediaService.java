package com.univangers.info.virtualteacher.contract.media;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("media")
public interface IMediaService {

    @GET
    @Produces("image/png")
    @Path("/{filename}")
    Response fetch(@PathParam("filename") String filename);

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    Response upload(@MultipartForm MediaImage image) throws IOException;
}
