package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.contract.lesson.Lesson;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.util.Optional;

public class LessonUtils {

    private LessonUtils() {
    }

    public static void validateNewContract(final Lesson lesson) {
        // Verify the lesson title is not empty
        if (lesson.getTitle() == null || "".equals(lesson.getTitle())) {
            throw new BadRequestException("A lesson needs to have a title");
        }
    }

    public static void validateContract(final Lesson lesson) {
        // Verify the lesson title is not empty
        if ("".equals(lesson.getTitle())) {
            throw new BadRequestException("A lesson needs to have a title");
        }
    }

    public static com.univangers.info.virtualteacher.core.Lesson fetchLesson(long id) {
        final Optional<com.univangers.info.virtualteacher.core.Lesson> optional = com.univangers.info.virtualteacher.core.Lesson.findByIdOptional(id);
        if (optional.isEmpty()) {
            throw new NotFoundException("Unknown lesson with id '" + id + "'");
        }
        return optional.get();
    }

    public static void verifyIsAccessible(final com.univangers.info.virtualteacher.core.Lesson l, final Person p) {
        if (!l.toDisplay() && p.typePerson == TypePerson.STUDENT)
            throw new NotFoundException("[" + p.login + "] Unknown lesson with id '" + l.id + "'");
    }
}
