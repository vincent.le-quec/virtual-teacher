package com.univangers.info.virtualteacher.provider.mapper.lesson;

import com.univangers.info.virtualteacher.core.Work;

public class LessonWorkMapper {
    public static Work fromContract(com.univangers.info.virtualteacher.contract.lesson.Work contract) {
        return new Work(contract.getPath());
    }

    public static com.univangers.info.virtualteacher.contract.lesson.Work toContract(final Work core) {
        return new com.univangers.info.virtualteacher.contract.lesson.Work(core.id, core.fileName);
    }
}
