package com.univangers.info.virtualteacher.core;

import com.univangers.info.virtualteacher.provider.utils.UserUtils;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "task", schema = "PUBLIC")
public class Task extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String title;
    public String description;
    public Date beginning;
    public Date ending;
    public boolean displayed;
    @Column(name = "contains_work")
    public boolean containsWork;

    @ManyToOne
    public Lesson lesson;

    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Work> works = new ArrayList<>();

    public Task() {
    }

    public Task(String title, String description, Date beginning, Date ending, boolean displayed, boolean containsWork, Lesson lesson) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.containsWork = containsWork;
        this.lesson = lesson;
    }

    public Task(String title, String description, Date beginning, Date ending, boolean displayed, boolean containsWork) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.containsWork = containsWork;
    }

    public static List<Task> getAll() {
        return listAll();
    }

    public static Optional<Task> findByTitleOptional(String title) {
        return find("title", title).firstResultOptional();
    }

    public boolean toDisplay() {
        if (!displayed) return false;
        if (beginning == null && ending == null) return true;
        final Date now = UserUtils.getCurrentDate();
        if (beginning == null) return now.compareTo(ending) < 0;
        if (ending == null) return now.compareTo(beginning) > 0;
        return now.compareTo(beginning) > 0 && now.compareTo(ending) < 0;
    }

    public String getWorkFolder() {
        return String.join("/", Work.BASE_FOLDER, "" + lesson.field.id, "" + lesson.id, id + "/");
    }

    public String getWorkFilename() {
        return String.join("-", lesson.field.fieldName, "" + lesson.title, title).replace(" ", "_");
    }
}
