package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ForbiddenException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(ForbiddenException.class);

    public ForbiddenException(final String m) {
        super(m, Response.status(Response.Status.FORBIDDEN).entity(m).build());
        LOGGER.error(m);
    }
}
