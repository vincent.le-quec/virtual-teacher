package com.univangers.info.virtualteacher.provider.mapper.lesson;

import com.univangers.info.virtualteacher.contract.lesson.Work;
import com.univangers.info.virtualteacher.core.Task;

import java.util.List;
import java.util.stream.Collectors;

public class LessonTaskMapper {
    public static Task fromContract(com.univangers.info.virtualteacher.contract.lesson.Task contract) {
        return new Task(
                contract.getTitle(),
                contract.getDescription(),
                contract.getBeginning(),
                contract.getEnding(),
                contract.getDisplayed() == null || contract.getDisplayed(),
                contract.getContainsWork() != null && contract.getContainsWork(),
                null);
    }

    public static com.univangers.info.virtualteacher.contract.lesson.Task toContract(final Task core) {
        List<Work> works = core.works
                .stream()
                .map(LessonWorkMapper::toContract)
                .collect(Collectors.toList());

        return new com.univangers.info.virtualteacher.contract.lesson.Task(
                core.id,
                core.title,
                core.description,
                core.ending,
                core.beginning,
                core.displayed,
                core.containsWork,
                works);
    }
}
