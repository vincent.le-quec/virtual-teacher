package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class NotAuthorizedException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(NotAuthorizedException.class);

    public NotAuthorizedException(final String m) {
        super(m, Response.status(Response.Status.UNAUTHORIZED).entity(m).build());
        LOGGER.error(m);
    }
}
