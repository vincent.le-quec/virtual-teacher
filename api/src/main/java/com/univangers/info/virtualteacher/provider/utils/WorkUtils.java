package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.contract.work.WorkFile;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.core.Work;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.util.Optional;

public class WorkUtils {

    private WorkUtils() {
    }

    public static void validateContract(WorkFile work) {
        // Verify the work have a fileName
        if (work.getFileName() == null || "".equals(work.getFileName())) {
            throw new BadRequestException("A work needs to have a fileName");
        }

        // Verify the work have a file
        if (work.getFile() == null) {
            throw new BadRequestException("A work needs to have a file");
        }
    }

    public static Work verifyWorkExists(long id) {
        final Optional<Work> optional = Work.findByIdOptional(id);
        if (optional.isEmpty()) {
            throw new NotFoundException("Unknown work with id '" + id + "'");
        }
        return optional.get();
    }

    public static void verifyIsAccessible(final com.univangers.info.virtualteacher.core.Work w, final Person p) {
        if (p.typePerson == TypePerson.STUDENT && w.person != p && w.person.typePerson == TypePerson.STUDENT)
            throw new ForbiddenException("[" + p.login + "] delete : access refused to work with id '" + w.id + "'");
    }
}
