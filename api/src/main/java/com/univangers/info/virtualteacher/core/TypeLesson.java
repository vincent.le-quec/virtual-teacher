package com.univangers.info.virtualteacher.core;

public enum TypeLesson {
    CM("CM"),
    TP("TP");

    private final String type;

    TypeLesson(final String type) {
        this.type = type;
    }
}
