package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "field_person", schema = "PUBLIC")
public class FieldPerson extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @ManyToOne
    public Field field;

    @ManyToOne
    public Person person;

    public FieldPerson() {
    }

    public FieldPerson(Field field, Person person) {
        this.field = field;
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldPerson that = (FieldPerson) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(person, that.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, person);
    }
}
