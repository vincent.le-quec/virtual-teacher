package com.univangers.info.virtualteacher.contract.work;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

public class WorkFile {

    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private InputStream file;

    @FormParam("file_name")
    @PartType(MediaType.TEXT_PLAIN)
    private String fileName;

    @FormParam("task")
    @PartType(MediaType.TEXT_PLAIN)
    private long task;

    public WorkFile() {
    }

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getTask() {
        return task;
    }

    public void setTask(long task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "WorkFile{fileName='" + fileName + '\'' + ", task=" + task + '}';
    }
}
