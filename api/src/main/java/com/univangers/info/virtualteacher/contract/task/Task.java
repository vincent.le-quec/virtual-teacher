package com.univangers.info.virtualteacher.contract.task;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Task {

    private long id;
    @JsonProperty("id_lesson")
    private long idLesson;
    private String title;
    private String description;
    private Date beginning;
    private Date ending;
    private Boolean displayed;
    @JsonProperty("contains_work")
    private Boolean containsWork;

    public Task() {
    }

    public Task(long idLesson, String title, String description, Date beginning, Date ending, Boolean displayed, Boolean containsWork) {
        this.idLesson = idLesson;
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.containsWork = containsWork;
    }

    public Task(long id, long idLesson, String title, String description, Date beginning, Date ending, Boolean displayed, Boolean containsWork) {
        this.id = id;
        this.idLesson = idLesson;
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.containsWork = containsWork;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(long idLesson) {
        this.idLesson = idLesson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginning() {
        return beginning;
    }

    public void setBeginning(Date beginning) {
        this.beginning = beginning;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    public Boolean getContainsWork() {
        return containsWork;
    }

    public void setContainsWork(Boolean containsWork) {
        this.containsWork = containsWork;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", idLesson=" + idLesson + ", title='" + title + '\'' + ", description='" + description + '\'' + ", beginning=" + beginning + ", ending=" + ending + ", displayed=" + displayed + ", containsWork=" + containsWork + '}';
    }
}
