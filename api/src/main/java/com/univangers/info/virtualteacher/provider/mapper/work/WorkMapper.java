package com.univangers.info.virtualteacher.provider.mapper.work;

import com.univangers.info.virtualteacher.contract.Person;
import com.univangers.info.virtualteacher.contract.work.WorkJson;
import com.univangers.info.virtualteacher.core.Work;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;

public class WorkMapper {
    public static WorkJson toContract(final Work core) {
        final Person p = PersonMapper.toContract(core.person);
        return new WorkJson(core.id, core.task.id, core.fileName, p, core.renderingDate);
    }
}
