package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "field", schema = "PUBLIC")
public class Field extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(name = "field_name")
    public String fieldName;
    public String description;
    public String icon;

    @OneToMany(mappedBy = "field", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<FieldPerson> teachers = new ArrayList<>();

    @OneToMany(mappedBy = "field", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Lesson> lessons = new ArrayList<>();

    @OneToMany(mappedBy = "field", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<FieldGroup> groups = new ArrayList<>();

    public Field() {
    }

    public Field(String fieldName, String description, String icon) {
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
    }

    public Field(String fieldName, String description, String icon, List<FieldPerson> teachers, List<Lesson> lessons, List<FieldGroup> groups) {
        this.fieldName = fieldName;
        this.description = description;
        this.icon = icon;
        this.teachers = teachers;
        this.lessons = lessons;
        this.groups = groups;
    }

    public static Optional<Field> findByLoginOptional(final String login) {
        return find("login", login).firstResultOptional();
    }

    public static Optional<Field> findByFieldNameOptional(final String fieldName) {
        return find("fieldName", fieldName).firstResultOptional();
    }

    public void removeTeacher(FieldPerson person) {
        person.field = null;
        person.person = null;
        person.delete();
    }

    public void removeGroup(FieldGroup group) {
        group.field = null;
        group.group = null;
        group.delete();
    }
}
