package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InternalServerErrorException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(InternalServerErrorException.class);

    public InternalServerErrorException(final String m) {
        super(m, Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(m).build());
        LOGGER.error(m);
    }
}
