package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.contract.work.IWorkService;
import com.univangers.info.virtualteacher.contract.work.WorkFile;
import com.univangers.info.virtualteacher.contract.work.WorkJson;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.Task;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.core.Work;
import com.univangers.info.virtualteacher.provider.exception.*;
import com.univangers.info.virtualteacher.provider.mapper.PersonMapper;
import com.univangers.info.virtualteacher.provider.mapper.work.WorkMapper;
import com.univangers.info.virtualteacher.provider.utils.*;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.security.Authenticated;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// this service was not injected in native mode
// see here : https://quarkus.io/guides/writing-native-applications-tips#using-the-registerforreflection-annotation
@RegisterForReflection
@ApplicationScoped
@Authenticated
public class WorkService implements IWorkService {

    private static final Logger LOGGER = Logger.getLogger(WorkService.class);

    @Inject
    IUserService userService;

    /**
     * TODO : de la doc ici pour savoir comment faire : https://quarkus.io/guides/rest-client-multipart
     */

    /**
     * Sends a list of WorkJson with all works of every student if it is the teacher or an admin
     * and a list with the student work if it is a student
     *
     * @param taskId
     * @return
     */
    @Override
    public Response fetch(long taskId) {
        final Person person = userService.getPerson();
        LOGGER.info("[" + person.login + "] fetch : taskId='" + taskId + "'");

        // verification
        final Task task = TaskUtils.fetchTask(taskId);
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, person);
        LessonUtils.verifyIsAccessible(task.lesson, person);
        TaskUtils.verifyIsAccessible(task, person);

        // Process
        final List<WorkJson> contract = task.lesson.field.teachers
                .stream()
                .map(t -> Work.findForTaskAndPersonOptional(task, t.person))
                .filter(Optional::isPresent)
                .map(w -> WorkMapper.toContract(w.get()))
                .collect(Collectors.toList());

        contract.addAll(task.lesson.field.groups
                .stream()
                .flatMap(fg -> fg.group.persons.stream())
                .filter(p -> person.typePerson != TypePerson.STUDENT || p.id == person.id)
                .distinct()
                .sorted(Comparator.comparing(e -> e.name))
                .map(p -> Work
                        .findForTaskAndPersonOptional(task, p)
                        .map(WorkMapper::toContract)
                        .orElseGet(() -> new WorkJson(0, taskId, null, PersonMapper.toContract(p), null))
                )
                .collect(Collectors.toList()));

        LOGGER.info("[" + person.login + "] fetch : return field with taskId '" + taskId + "'");
        return Response.ok(contract).build();
    }

    /**
     * This methods sends every files (no contracts, only File) for this lesson
     * I am not sure whether it is possible to send multiple files.
     * If you cannot find how to do it, let it aside and I will see.
     * ACCESS : - admin have access to it,
     * - teachers have access to it if they teach this lesson
     * - students : do not have access
     *
     * @param taskId
     * @return
     */
    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    public Response download(long taskId) {
        final Person person = userService.getPerson();
        LOGGER.info("[" + person.login + "] download : taskId='" + taskId + "'");

        // verification
        final Task task = TaskUtils.fetchTask(taskId);
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, person);

        // process
        final String folderPath = task.getWorkFolder();
        if (!new File(folderPath).exists())
            throw new InternalServerErrorException("[" + person.login + "] download : Cannot find any folder for path " + folderPath);
        final String outputFileName = task.getWorkFilename() + ".zip";
        final String cmd = "cd " + folderPath + " && zip -r " + outputFileName + " *";

        // delete file if already exists
        File file = new File(folderPath + outputFileName);
        file.delete();

        // run the shell command
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("/bin/bash", "-c", cmd);

        try {
            Process process = processBuilder.start();

            int exitVal = process.waitFor();
            if (exitVal != 0) {
                throw new InternalServerErrorException("[" + person.login + "] download : An error has occurred while zipping the folder for task with id " + taskId);
            }
        } catch (IOException | InterruptedException e) {
            throw new InternalServerErrorException("[" + person.login + "] download : An error has occurred while zipping the folder for task with id " + taskId);
        }

        // response
        LOGGER.info("[" + person.login + "] download : return Works for task with taskId '" + taskId + "'");
        return Response.ok(
                (StreamingOutput) output -> {
                    try {
                        Files.copy(file.toPath(), output);
                    } finally {
                        file.delete();
                    }
                })
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                .header("Access-Control-Expose-Headers", "Content-Disposition")
                .build();
    }

    /**
     * Sends the file (not a contract, a File) for this lesson and this workId
     * And also verifies that you have the right to do it
     * https://stackoverflow.com/questions/12239868/whats-the-correct-way-to-send-a-file-from-rest-web-service-to-client
     * ACCESS : - admin have access to it,
     * - teachers have access to it if they teach this lesson
     * - students have access to it to download their works
     *
     * @param taskId
     * @param workId
     * @return
     */
    @Override
    public Response downloadWork(long taskId, long workId) {
        final Person person = userService.getPerson();
        LOGGER.info("[" + person.login + "] downloadWork : taskId='" + taskId + "', workId='" + workId + "'");

        // verification
        final Task task = TaskUtils.fetchTask(taskId);
        final Work work = WorkUtils.verifyWorkExists(workId);
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, person);
        LessonUtils.verifyIsAccessible(task.lesson, person);
        TaskUtils.verifyIsAccessible(task, person);
        WorkUtils.verifyIsAccessible(work, person);

        // process
        final String folderPath = work.getWorkPath();
        final File file = new File(folderPath);
        if (!file.exists())
            throw new InternalServerErrorException("[" + person.login + "] downloadWork : Cannot find any folder for path " + folderPath);

        // response
        LOGGER.info("[" + person.login + "] downloadWork : return Work for task with id '" + taskId + "' and work with id='" + workId + "'");
        return Response.ok(
                (StreamingOutput) output -> {
                    try {
                        Files.copy(file.toPath(), output);
                    } catch (IOException e) {
                        LOGGER.error("[" + person.login + "] downloadWork : Cannot find any folder for path " + folderPath);
                    }
                })
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                .header("Access-Control-Expose-Headers", "Content-Disposition")
                .build();
    }

    /**
     * Takes a WorkFile and saves the input stream as a file aside the application.
     * Also a core.Work is saved at the same time.
     * https://www.baeldung.com/convert-input-stream-to-a-file#java
     * ACCESS : - admin have access to it;
     * - teachers have access to it if they teach this lesson
     * - students have access to it if they follow this lesson
     *
     * @param work
     * @return
     */
    @Override
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class, ConflictException.class})
    public Response upload(WorkFile work) throws IOException {
        final Person person = userService.getPerson();
        LOGGER.info("[" + person.login + "] upload : work=" + work);

        // verifications
        final String filename = WorkJson.formatFilename(person.id, work.getFileName());
        WorkUtils.validateContract(work); // All data are needed there
        Task task = TaskUtils.fetchTask(work.getTask());
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, person);
        LessonUtils.verifyIsAccessible(task.lesson, person);
        TaskUtils.verifyIsAccessible(task, person);

        // delete previous work
        final Optional<Work> optional = Work.findForTaskAndPersonOptional(task, person);
        if (optional.isPresent()) {
            final Work w = optional.get();
            w.deleteFile();
            w.delete();
        }

        //Create core
        Work core = new Work(filename, UserUtils.getCurrentDate(), person, task);
        core.persist();

        // Save file
        File targetDirectory = new File(task.getWorkFolder());
        targetDirectory.mkdirs();
        File targetFile = new File(core.getWorkPath());
        try (FileOutputStream outputStream = new FileOutputStream(targetFile)) {
            int read;
            byte[] bytes = new byte[1024];

            while ((read = work.getFile().read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }

        // response
        final WorkJson contract = WorkMapper.toContract(core);
        LOGGER.info("[" + person.login + "] upload : work uploaded : " + contract);
        return Response.ok(contract).build();
    }

    /**
     * deletes the work with this id and deletes the saved file
     * ACCESS : - admin have access to it;
     * - teachers have access to it if they teach this lesson
     * - students have access to it if it is their works
     *
     * @param taskId
     * @return
     */
    @Override
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class})
    public Response delete(long taskId) {
        final Person person = userService.getPerson();
        LOGGER.info("[" + person.login + "] delete : taskId=" + taskId);

        // Verification
        final com.univangers.info.virtualteacher.core.Task task = TaskUtils.fetchTask(taskId);
        FieldUtils.verifyHaveAccessToField(task.lesson.field.id, person);
        final Optional<Work> optional = Work.findForTaskAndPersonOptional(task, person);
        final Work work = optional.orElseThrow(() -> new NotFoundException("[" + person.login + "] delete : No work found for this user on task with id '" + taskId + "'"));

        // Process
        work.deleteWork();

        // Respond
        LOGGER.info("[" + person.login + "] delete : work correctly deleted for taskId " + taskId);
        return Response.ok().build();
    }
}
