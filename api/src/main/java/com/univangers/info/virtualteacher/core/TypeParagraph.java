package com.univangers.info.virtualteacher.core;

public enum TypeParagraph {
    TEXT("TEXT"),
    IMAGE("IMAGE");

    private final String type;

    TypeParagraph(final String type) {
        this.type = type;
    }
}
