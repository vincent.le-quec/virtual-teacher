package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ConflictException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(ConflictException.class);

    public ConflictException(final String m) {
        super(m, Response.status(Response.Status.CONFLICT).entity(m).build());
        LOGGER.error(m);
    }
}
