package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.contract.task.Task;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ConflictException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.io.File;
import java.util.Optional;

public class TaskUtils {

    private TaskUtils() {
    }

    public static void validateNewContract(final Task task) {
        // Verify the task have a title
        if (task.getTitle() == null || "".equals(task.getTitle())) {
            throw new BadRequestException("A task needs to have a title");
        }
    }

    public static void validateContract(final Task task) {
        // Verify the task have a title
        if ("".equals(task.getTitle())) {
            throw new BadRequestException("A task needs to have a title");
        }
    }

    public static com.univangers.info.virtualteacher.core.Task fetchTask(final long id) {
        final Optional<com.univangers.info.virtualteacher.core.Task> task = com.univangers.info.virtualteacher.core.Task.findByIdOptional(id);
        if (task.isEmpty()) {
            throw new NotFoundException("Unknown task with id '" + id + "'");
        }
        return task.get();
    }

    public static void verifyHaveAccess(long taskId, final Person p) {
        if (p.typePerson == TypePerson.ADMIN)
            return;

        final com.univangers.info.virtualteacher.core.Task t = fetchTask(taskId);
        FieldUtils.verifyHaveAccessToField(t.lesson.field.id, p);

        if (p.typePerson == TypePerson.STUDENT && !t.toDisplay()) {
            throw new NotFoundException("[" + p.login + "] verifyHaveAccess : unknown task with id '" + taskId + "'");
        }
    }

    public static void verifyIsAccessible(final com.univangers.info.virtualteacher.core.Task t, final Person p) {
        if (!t.toDisplay() && p.typePerson == TypePerson.STUDENT)
            throw new NotFoundException("[" + p.login + "] Unknown task with id '" + t.id + "'");
    }

    public static void verifyFilenameIsFree(final String basePath, final String fileName, final Person p) {
        if (new File(basePath + fileName).exists())
            throw new ConflictException("[" + p.login + "] Filename '" + fileName + "' is already taken. Please choose another one or follow your teacher's naming rules.");
    }
}
