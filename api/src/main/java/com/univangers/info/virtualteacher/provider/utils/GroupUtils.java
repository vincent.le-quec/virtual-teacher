package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.core.FieldGroup;
import com.univangers.info.virtualteacher.core.Group;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GroupUtils {

    private GroupUtils() {
    }

    public static List<FieldGroup> verifAllGroupsExist(final List<String> groups, com.univangers.info.virtualteacher.core.Field field) {
        return groups
                .stream()
                .map(GroupUtils::verifyOneGroupExists)
                .map(group -> new FieldGroup(field, group))
                .collect(Collectors.toList());
    }

    public static Group verifyOneGroupExists(final String group) {
        final Optional<Group> option = Group.findByByNameOptional(group);
        if (option.isEmpty()) {
            throw new NotFoundException("Unknown group with title '" + group + "'");
        }
        return option.get();
    }
}
