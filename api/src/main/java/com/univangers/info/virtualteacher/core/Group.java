package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "groupe", schema = "PUBLIC")
public class Group extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String title;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Person> persons = new ArrayList<>();

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<FieldGroup> fields = new ArrayList<>();

    public Group() {
    }

    public Group(String title) {
        this.title = title;
    }

    public static Optional<Group> findByByNameOptional(final String name) {
        return find("title", name).firstResultOptional();
    }
}
