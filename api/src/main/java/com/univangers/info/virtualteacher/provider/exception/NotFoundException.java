package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class NotFoundException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(NotFoundException.class);

    public NotFoundException(final String m) {
        super(m, Response.status(Response.Status.NOT_FOUND).entity(m).build());
        LOGGER.error(m);
    }
}
