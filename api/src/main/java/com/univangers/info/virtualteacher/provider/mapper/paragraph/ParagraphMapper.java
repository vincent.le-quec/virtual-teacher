package com.univangers.info.virtualteacher.provider.mapper.paragraph;

import com.univangers.info.virtualteacher.core.Paragraph;
import com.univangers.info.virtualteacher.core.TypeFile;

public class ParagraphMapper {
    public static Paragraph fromContract(com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract) {
        return new Paragraph(contract.getTitle(),
                contract.getTitleSize() != null ? contract.getTitleSize() : 0,
                contract.getContent(),
                contract.getOrder() != null ? contract.getOrder() : 0,
                contract.getType() == null ? TypeFile.TXT : TypeFile.IMAGE.getType().equals(contract.getType()) ? TypeFile.IMAGE : TypeFile.TXT
        );
    }

    public static Paragraph fromContract(final com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract, final Paragraph core) {
        if (contract.getTitle() != null) core.title = contract.getTitle();
        if (contract.getTitleSize() != null) core.titleSize = contract.getTitleSize();
        if (contract.getContent() != null) core.content = contract.getContent();
        if (contract.getTitle() != null) core.title = contract.getTitle();
        if (contract.getOrder() != null) core.order = contract.getOrder();
        if (contract.getType() != null)
            core.type = TypeFile.IMAGE.getType().equals(contract.getType()) ? TypeFile.IMAGE : TypeFile.TXT;

        return core;
    }

    public static com.univangers.info.virtualteacher.contract.paragraph.Paragraph toContract(final Paragraph core) {
        com.univangers.info.virtualteacher.contract.paragraph.Paragraph contract = new com.univangers.info.virtualteacher.contract.paragraph.Paragraph(
                core.title,
                core.titleSize,
                core.content,
                core.order,
                core.type.toString(),
                core.lesson.id);

        contract.setId(core.id);
        return contract;
    }
}
