package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Entity
@Table(name = "person", schema = "PUBLIC")
public class Person extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String name;
    @Column(name = "first_name")
    public String firstName;
    public String login;
    public String email;
    @Column(name = "type_person")
    public TypePerson typePerson;

    @ManyToOne
    public Group group;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<FieldPerson> fields = new ArrayList<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Work> works = new ArrayList<>();

    public Person() {
    }

    public Person(String name, String firstName, String login, String email, TypePerson typePerson) {
        this.name = name;
        this.firstName = firstName;
        this.login = login;
        this.email = email;
        this.typePerson = typePerson;
    }

    public static List<Person> getAll() {
        return Person.listAll();
    }

    public static Person findByLogin(final String login) {
        return find("login", login).firstResult();
    }

    public static Optional<Person> findByEmailOptional(final String email) {
        return find("email", email).firstResultOptional();
    }

    public static Optional<Person> findByLoginOptional(final String login) {
        return find("login", login).firstResultOptional();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return login.equals(person.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", typePerson=" + typePerson +
                ", group=" + (group != null ? group.title : null) +
                ", fields=" + fields.stream().map(f -> f.field.fieldName).collect(Collectors.toList()) +
                ", works=" + works.stream().map(w -> w.fileName).collect(Collectors.toList()) +
                '}';
    }
}
