package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.contract.Person;
import com.univangers.info.virtualteacher.core.FieldPerson;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserUtils {

    private UserUtils() {
    }

    public static Date getCurrentDate() {
        return Date.from(ZonedDateTime.now(ZoneId.of("Europe/Paris")).toInstant());
    }

    /**
     * throw exception if a person doesn't exist or if a person is not admin and teachers
     *
     * @param lp
     */
    public static List<FieldPerson> verifAllTeacherExist(List<Person> lp, com.univangers.info.virtualteacher.core.Field field) { //List de contract person
        return lp
                .stream()
                .map(UserUtils::verifyOneTeacherExists)
                .map(person -> new FieldPerson(field, person))
                .collect(Collectors.toList());
    }

    /**
     * @param contract
     * @return
     */
    public static com.univangers.info.virtualteacher.core.Person verifyOneTeacherExists(com.univangers.info.virtualteacher.contract.Person contract) {
        final Optional<com.univangers.info.virtualteacher.core.Person> p = com.univangers.info.virtualteacher.core.Person.findByEmailOptional(contract.getEmail());
        if (p.isEmpty()) {
            throw new NotFoundException("Unknown teacher with name '" + contract.getName() + "'");
        }
        return p.get();
    }

    public static com.univangers.info.virtualteacher.core.Person findById(long id, com.univangers.info.virtualteacher.core.Person p) {
        return (com.univangers.info.virtualteacher.core.Person) com.univangers.info.virtualteacher.core.Person.findByIdOptional(id)
                .orElseThrow(() -> new NotFoundException("[" + p.login + "] Unknown user with id '" + id + "'"));
    }
}
