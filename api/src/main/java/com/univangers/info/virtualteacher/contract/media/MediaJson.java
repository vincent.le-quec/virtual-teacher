package com.univangers.info.virtualteacher.contract.media;

public class MediaJson {

    private String path;

    public MediaJson() {
    }

    public MediaJson(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "MediaJson{" +
                "path='" + path + '\'' +
                '}';
    }
}
