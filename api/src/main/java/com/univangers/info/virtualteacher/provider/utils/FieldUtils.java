package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.core.Field;
import com.univangers.info.virtualteacher.core.TypePerson;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FieldUtils {

    private FieldUtils() {
    }

    public static void validateNewContract(final com.univangers.info.virtualteacher.contract.field.Field field) {
        if (field.getFieldName() == null || "".equals(field.getFieldName())) {
            throw new BadRequestException("A field needs to have a name");
        }

        if (field.getTeachers() == null || field.getTeachers().size() == 0) {
            throw new BadRequestException("A field needs to have at least one teacher");
        }

        if (field.getTeachers().stream().anyMatch(p -> p.getTypePerson() == TypePerson.STUDENT)) {
            throw new BadRequestException("A student cannot teach a field");
        }
    }

    public static void validateContract(final com.univangers.info.virtualteacher.contract.field.Field field) {
        if ("".equals(field.getFieldName())) {
            throw new BadRequestException("A field needs to have a name");
        }

        if (field.getTeachers() != null && field.getTeachers().size() == 0) {
            throw new BadRequestException("A field needs to have at least one teacher");
        }

        if (field.getTeachers() != null && field.getTeachers().stream().anyMatch(p -> p.getTypePerson() == TypePerson.STUDENT)) {
            throw new BadRequestException("A student cannot teach a field");
        }
    }

    public static com.univangers.info.virtualteacher.core.Field fetchField(final long id) {
        final Optional<com.univangers.info.virtualteacher.core.Field> field = com.univangers.info.virtualteacher.core.Field.findByIdOptional(id);
        if (field.isEmpty()) {
            throw new NotFoundException("Unknown field with id '" + id + "'");
        }
        return field.get();
    }

    /**
     * throw exception if the person has no read access on this field
     *
     * @param fieldId
     * @param p
     */
    public static void verifyHaveAccessToField(long fieldId, com.univangers.info.virtualteacher.core.Person p) {
        if (p.typePerson == TypePerson.ADMIN)
            return;

        final Optional<Field> optional = fromPersonGetFields(p)
                .stream()
                .filter(f -> f.id == fieldId)
                .findAny();

        if (optional.isEmpty()) {
            throw new ForbiddenException("[" + p.login + "] verifyHaveAccessToField : access refused to field with id '" + fieldId + "'");
        }
    }

    /**
     * For any person, this method lists every core field he has access to
     *
     * @param p
     * @return
     */
    public static List<Field> fromPersonGetFields(final com.univangers.info.virtualteacher.core.Person p) {

        // fetch fields for admin
        if (p.typePerson == TypePerson.ADMIN) {
            return com.univangers.info.virtualteacher.core.Field.listAll();
        }

        // fetch fields for teachers
        if (p.typePerson == TypePerson.TEACHER) {
            return p.fields.stream()
                    .map(fieldPerson -> fieldPerson.field)
                    .collect(Collectors.toList());
        }

        // fetch fields for student
        return p.group.fields
                .stream()
                .map(f -> f.field)
                .collect(Collectors.toList());
    }
}
