package com.univangers.info.virtualteacher.contract.lesson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class Lesson {

    private long id;
    private String title; // mandatory
    private String description;
    private Integer order;
    private Date beginning;
    private Date ending;
    @JsonProperty("field_id")
    private Long fieldId;// mandatory
    private Boolean hide;

    private List<Paragraph> paragraphs;
    private List<Task> tasks;

    public Lesson() {
    }

    public Lesson(String title, String description, Date beginning, Date ending, Boolean hide, Integer order, Long fieldId, List<Paragraph> paragraphs, List<Task> tasks) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.hide = hide;
        this.fieldId = fieldId;
        this.paragraphs = paragraphs;
        this.tasks = tasks;
        this.order = order;
    }

    public Lesson(long id, String title, String description, Date beginning, Date ending, Boolean hide, Integer order, Long fieldId, List<Paragraph> paragraphs, List<Task> tasks) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.hide = hide;
        this.fieldId = fieldId;
        this.paragraphs = paragraphs;
        this.tasks = tasks;
        this.order = order;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer order() {

        return (order != null ? order : 0);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginning() {
        return beginning;
    }

    public void setBeginning(Date beginning) {
        this.beginning = beginning;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public Boolean isHide() {
        return hide;
    }

    public Boolean hide() {
        return (hide != null ? hide : false);
    }

    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    @Override
    public String toString() {
        return "Lesson{" + "id=" + id + ", title='" + title + '\'' + ", description='" + description + '\'' + ", order=" + order + ", beginning=" + beginning + ", ending=" + ending + ", fieldId=" + fieldId + ", hide=" + hide + ", paragraphs=" + paragraphs + ", tasks=" + tasks + '}';
    }
}
