package com.univangers.info.virtualteacher.provider.mapper.lesson;

import com.univangers.info.virtualteacher.contract.lesson.Paragraph;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LessonMapper {

    public static Lesson fromContract(final com.univangers.info.virtualteacher.contract.lesson.Lesson contract) {
        return new Lesson(
                contract.getTitle(),
                contract.getDescription(),
                contract.getBeginning(),
                contract.getEnding(),
                contract.hide() != null && contract.hide(),
                contract.order() != null ? contract.order() : 0,
                null,
                new ArrayList<>(),
                new ArrayList<>());
    }

    public static Lesson fromContract(final com.univangers.info.virtualteacher.contract.lesson.Lesson contract, final Lesson core) {

        if (contract.getTitle() != null) core.title = contract.getTitle();
        if (contract.getDescription() != null) core.description = contract.getDescription();
        if (contract.getBeginning() != null) core.beginning = contract.getBeginning();
        if (contract.getEnding() != null) core.ending = contract.getEnding();
        if (contract.isHide() != null) core.hide = contract.isHide();
        if (contract.getOrder() != null) core.order = contract.getOrder();

        return core;
    }

    public static com.univangers.info.virtualteacher.contract.lesson.Lesson toContract(final Lesson core) {

        // Set to List
        List<com.univangers.info.virtualteacher.contract.lesson.Paragraph> paragraphs = core.paragraphs
                .stream()
                .map(LessonParagraphMapper::toContract)
                .sorted(Comparator.comparingInt(Paragraph::getOrder))
                .collect(Collectors.toList());

        List<com.univangers.info.virtualteacher.contract.lesson.Task> tasks = core.tasks
                .stream()
                .filter(Task::toDisplay)
                .map(LessonTaskMapper::toContract)
                .collect(Collectors.toList());

        return new com.univangers.info.virtualteacher.contract.lesson.Lesson(
                core.id,
                core.title,
                core.description,
                core.beginning,
                core.ending,
                core.hide,
                core.order,
                core.field.id,
                paragraphs,
                tasks);
    }
}
