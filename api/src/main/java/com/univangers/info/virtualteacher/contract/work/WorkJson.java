package com.univangers.info.virtualteacher.contract.work;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.univangers.info.virtualteacher.contract.Person;

import java.util.Date;

public class WorkJson {

    private long id;
    @JsonProperty("task_id")
    private long taskId;
    @JsonProperty("file_name")
    private String fileName;
    private Person user;
    @JsonProperty("rendering_date")
    private Date renderingDate;

    public WorkJson() {
    }

    public WorkJson(long id, long taskId, String fileName, Person user, Date renderingDate) {
        this.id = id;
        this.taskId = taskId;
        this.fileName = fileName;
        this.user = user;
        this.renderingDate = renderingDate;
    }

    public static String formatFilename(final long id, String filename) {
        return id + "_" + filename;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Person getUser() {
        return user;
    }

    public void setUser(Person user) {
        this.user = user;
    }

    public Date getRenderingDate() {
        return renderingDate;
    }

    public void setRenderingDate(Date renderingDate) {
        this.renderingDate = renderingDate;
    }

    @Override
    public String toString() {
        return "WorkJson{" +
                "id=" + id +
                ", taskId=" + taskId +
                ", fileName='" + fileName + '\'' +
                ", user=" + user +
                ", renderingDate=" + renderingDate +
                '}';
    }
}
