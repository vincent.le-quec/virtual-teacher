package com.univangers.info.virtualteacher.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.univangers.info.virtualteacher.core.TypePerson;

public class Person {

    private String name;
    @JsonProperty("first_name")
    private String firstName;
    private String login;
    private String email;
    @JsonProperty("type")
    private TypePerson typePerson;
    private String group;

    public Person() {
    }

    public Person(String name, String firstName, String login, String email, TypePerson typePerson, String group) {
        this.name = name;
        this.firstName = firstName;
        this.login = login;
        this.email = email;
        this.typePerson = typePerson;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public TypePerson getTypePerson() {
        return typePerson;
    }

    public String getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "Person{" + "name='" + name + '\'' + ", firstName='" + firstName + '\'' + ", login='" + login + '\'' + ", email='" + email + '\'' + ", typePerson=" + typePerson + ", group='" + group + '\'' + '}';
    }
}