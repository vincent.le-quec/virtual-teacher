package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.field.Field;
import com.univangers.info.virtualteacher.contract.field.IFieldService;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;
import com.univangers.info.virtualteacher.provider.mapper.field.FieldMapper;
import com.univangers.info.virtualteacher.provider.utils.FieldUtils;
import com.univangers.info.virtualteacher.provider.utils.GroupUtils;
import com.univangers.info.virtualteacher.provider.utils.UserUtils;
import io.quarkus.security.Authenticated;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
@Authenticated
public class FieldService implements IFieldService {

    private static final Logger LOGGER = Logger.getLogger(FieldService.class);

    @Inject
    IUserService userService;

    @Override
    public Response fetchAll() {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetchAll");

        // LIST OF FIELDS CORES
        List<Field> contracts = FieldUtils.fromPersonGetFields(p)
                .stream()
                .map(FieldMapper::toContract)
                .sorted(Comparator.comparing(Field::getFieldName))
                .collect(Collectors.toList());

        LOGGER.info("[" + p.login + "] fetchAll : return every fields");
        return Response.ok(contracts).build();
    }

    @Override
    public Response fetch(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetch : id='" + id + "'");

        // LIST OF FIELDS CORES
        final com.univangers.info.virtualteacher.core.Field core = FieldUtils.fetchField(id);
        FieldUtils.verifyHaveAccessToField(id, p);

        final Field contract = FieldMapper.toContract(core);
        LOGGER.info("[" + p.login + "] fetch : return field with id '" + id + "'");
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response add(Field field) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] add : field=" + field);

        // Verification
        FieldUtils.validateNewContract(field);
        final com.univangers.info.virtualteacher.core.Field core = FieldMapper.fromContract(field);

        // Process
        core.groups = GroupUtils.verifAllGroupsExist(field.getGroups(), core);
        core.teachers = UserUtils.verifAllTeacherExist(field.getTeachers(), core);
        core.persist();

        // Respond
        final Field contract = FieldMapper.toContract(core);
        LOGGER.info("[" + p.login + "] add : field added : " + contract);
        return Response.status(Response.Status.CREATED).entity(contract).build();
    }


    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response update(Field field) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] update : field=" + field);

        // Verification
        FieldUtils.validateContract(field);
        final com.univangers.info.virtualteacher.core.Field core = FieldUtils.fetchField(field.getId());
        FieldUtils.verifyHaveAccessToField(field.getId(), p);

        // Process
        // rename field folder if exists
        FieldMapper.fromContract(field, core);

        if (field.getGroups() != null) {
            for (int i = 0; i < core.groups.size(); i++) {
                core.removeGroup(core.groups.get(0));
                core.groups.remove(core.groups.get(0));
            }
            core.groups.addAll(GroupUtils.verifAllGroupsExist(field.getGroups(), core));
        }
        if (field.getTeachers() != null) {
            for (int i = 0; i < core.teachers.size(); i++) {
                core.removeTeacher(core.teachers.get(0));
                core.teachers.remove(core.teachers.get(0));
            }
            core.teachers.addAll(UserUtils.verifAllTeacherExist(field.getTeachers(), core));
        }

        // Respond
        final Field contract = FieldMapper.toContract(core);
        LOGGER.info("[" + p.login + "] update : field updated : " + contract);
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response delete(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] delete : id=" + id);

        final com.univangers.info.virtualteacher.core.Field core = FieldUtils.fetchField(id);
        FieldUtils.verifyHaveAccessToField(id, p);

        core.teachers.forEach(core::removeTeacher);
        core.groups.forEach(core::removeGroup);
        core.delete();

        LOGGER.info("[" + p.login + "] delete : field with id '" + id + "' has been deleted");
        return Response.ok().build();
    }
}
