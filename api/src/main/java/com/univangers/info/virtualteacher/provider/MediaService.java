package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.media.IMediaService;
import com.univangers.info.virtualteacher.contract.media.MediaImage;
import com.univangers.info.virtualteacher.contract.media.MediaJson;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

@ApplicationScoped
public class MediaService implements IMediaService {

    private static final Logger LOGGER = Logger.getLogger(MediaService.class);
    private static final String MEDIA_FOLDER = "media/";

    @Inject
    IUserService userService;

    @Override
    public Response fetch(String filename) {
        LOGGER.info("fetch : filename='" + filename + "'");

        // verification
        final String completePath = MEDIA_FOLDER + filename;
        if (!new File(completePath).exists())
            throw new NotFoundException("fetch : Unknown file '" + filename + "'");

        // process
        final File file = new File(completePath);

        // response
        LOGGER.info("fetch : return image '" + filename + "'");
        return Response.ok(
                (StreamingOutput) output -> {
                    try {
                        Files.copy(file.toPath(), output);
                    } catch (IOException e) {
                        LOGGER.error("fetch : Cannot find any folder for path '" + completePath + "'");
                    }
                })
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                .build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    public Response upload(MediaImage image)  throws IOException {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] upload : image=" + image.getFilename());

        // verifications
        String filename = UUID.randomUUID().getMostSignificantBits() + "_" + image.getFilename();
        // verify filename is not already taken
        String completePath = MEDIA_FOLDER + filename;
        while (new File(completePath).exists()) {
            filename = UUID.randomUUID().getMostSignificantBits() + "_" + image.getFilename();
            completePath = MEDIA_FOLDER + filename;
        }

        // Save file
        File targetDirectory = new File(MEDIA_FOLDER);
        targetDirectory.mkdirs();
        File targetFile = new File(completePath);
        try (FileOutputStream outputStream = new FileOutputStream(targetFile)) {
            int read;
            byte[] bytes = new byte[1024];

            while ((read = image.getFile().read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

        }

        // response
        final MediaJson contract = new MediaJson("/" + completePath);
        LOGGER.info("[" + p.login + "] upload : image uploaded : " + contract);
        return Response.ok(contract).build();
    }
}
