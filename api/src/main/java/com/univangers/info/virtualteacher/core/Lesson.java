package com.univangers.info.virtualteacher.core;

import com.univangers.info.virtualteacher.provider.utils.UserUtils;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "lesson", schema = "PUBLIC")
public class Lesson extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String title;
    public String description;
    public Date beginning;
    public Date ending;
    public boolean hide;
    @Column(name = "type_lesson")
    public TypeLesson typeLesson;

    @Column(name = "lesson_order")
    public int order;

    @ManyToOne
    public Field field;

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Paragraph> paragraphs = new ArrayList<>();

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Task> tasks = new ArrayList<>();

    public Lesson() {
    }

    public Lesson(String title, String description, Date beginning, Date ending, boolean hide, int order, Field field) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.hide = hide;
        this.field = field;
        this.order = order;
    }

    public Lesson(String title, String description, Date beginning, Date ending, boolean hide, int order, Field field, List<Paragraph> paragraphs, List<Task> tasks) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.hide = hide;
        this.field = field;
        this.paragraphs = paragraphs;
        this.tasks = tasks;
        this.order = order;
    }

    public static Optional<Lesson> findByTitleOptional(String title) {
        return find("title", title).firstResultOptional();
    }

    public boolean toDisplay() {
        if (hide) return false;
        if (beginning == null && ending == null) return true;
        final Date now = UserUtils.getCurrentDate();
        if (beginning == null) return now.compareTo(ending) < 0;
        if (ending == null) return now.compareTo(beginning) > 0;
        return now.compareTo(beginning) > 0 && now.compareTo(ending) < 0;
    }
}
