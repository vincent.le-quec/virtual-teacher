package com.univangers.info.virtualteacher.core;

public enum TypeFile {
    TXT("TXT"),
    IMAGE("IMAGE");

    private final String type;

    TypeFile(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
