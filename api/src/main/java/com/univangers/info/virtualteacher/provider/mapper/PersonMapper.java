package com.univangers.info.virtualteacher.provider.mapper;

import com.univangers.info.virtualteacher.core.Person;

public class PersonMapper {

    public static com.univangers.info.virtualteacher.contract.Person toContract(final Person core) {
        return new com.univangers.info.virtualteacher.contract.Person(
                core.name,
                core.firstName,
                core.login,
                core.email,
                core.typePerson,
                core.group != null ? core.group.title : null
        );
    }
}