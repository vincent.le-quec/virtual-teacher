package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.paragraph.IParagraphService;
import com.univangers.info.virtualteacher.contract.paragraph.Paragraph;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Lesson;
import com.univangers.info.virtualteacher.core.Person;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.ForbiddenException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;
import com.univangers.info.virtualteacher.provider.mapper.paragraph.ParagraphMapper;
import com.univangers.info.virtualteacher.provider.utils.FieldUtils;
import com.univangers.info.virtualteacher.provider.utils.LessonUtils;
import com.univangers.info.virtualteacher.provider.utils.ParagraphUtils;
import io.quarkus.security.Authenticated;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

@ApplicationScoped
@Authenticated
public class ParagraphService implements IParagraphService {

    private static final Logger LOGGER = Logger.getLogger(LessonService.class);

    @Inject
    IUserService userService;

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, BadRequestException.class, ForbiddenException.class})
    public Response add(Paragraph paragraph) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] add : paragraph=" + paragraph);

        // Verification
        ParagraphUtils.validateNewContract(paragraph); // titre + content
        final Lesson lesson = LessonUtils.fetchLesson(paragraph.getLessonId());
        FieldUtils.verifyHaveAccessToField(lesson.field.id, userService.getPerson());

        // Process
        final com.univangers.info.virtualteacher.core.Paragraph core = ParagraphMapper.fromContract(paragraph);
        core.lesson = lesson;
        core.persist();

        // Respond
        final Paragraph contract = ParagraphMapper.toContract(core);
        LOGGER.info("[" + p.login + "] add : paragraph added : " + contract);
        return Response.status(Response.Status.CREATED).entity(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class, ForbiddenException.class})
    public Response update(Paragraph paragraph) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] update : paragraph=" + paragraph);

        // Verification
        ParagraphUtils.validateContract(paragraph);
        final com.univangers.info.virtualteacher.core.Paragraph core = ParagraphUtils.fetchParagraph(paragraph.getId());
        FieldUtils.verifyHaveAccessToField(core.lesson.field.id, userService.getPerson());

        // We can't change the lesson
        if (paragraph.getLessonId() != null && core.lesson.id != paragraph.getLessonId())
            throw new BadRequestException("[" + p.login + "] update : lesson id cannot be updated");

        // Process
        ParagraphMapper.fromContract(paragraph, core);

        // Respond
        final Paragraph contract = ParagraphMapper.toContract(core);
        LOGGER.info("[" + p.login + "] update : paragraph updated : " + contract);
        return Response.ok(contract).build();
    }

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {NotFoundException.class, ForbiddenException.class, BadRequestException.class})
    public Response delete(long id) {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] delete : id=" + id);

        // Verification
        final com.univangers.info.virtualteacher.core.Paragraph paragraph = ParagraphUtils.fetchParagraph(id);
        FieldUtils.verifyHaveAccessToField(paragraph.lesson.field.id, userService.getPerson());

        // Process
        paragraph.delete();

        // Respond
        LOGGER.info("[" + p.login + "] delete : paragraph with id '" + id + "' has been deleted");
        return Response.ok().build();
    }
}
