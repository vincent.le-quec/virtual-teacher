package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Parameters;

import javax.persistence.*;
import java.io.File;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "work", schema = "PUBLIC")
@NamedQuery(name = "Work.getForTaskAndPerson", query = "SELECT w FROM Work w WHERE w.task=:task AND w.person=:person")
public class Work extends PanacheEntityBase {

    @Transient
    public static final String BASE_FOLDER = "uploads";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    @Column(name = "file_name")
    public String fileName;
    @Column(name = "rendering_date")
    public Date renderingDate;

    @ManyToOne
    public Person person;

    @ManyToOne
    public Task task;

    public Work() {
    }

    public Work(String fileName) {
        this.fileName = fileName;
    }

    public Work(String fileName, Date renderingDate, Person person, Task task) {
        this.fileName = fileName;
        this.renderingDate = renderingDate;
        this.person = person;
        this.task = task;
    }

    public static Optional<Work> findForTaskAndPersonOptional(final Task t, final Person p) {
        return find("#Work.getForTaskAndPerson", Parameters.with("task", t).and("person", p)).firstResultOptional();
    }

    public String getWorkPath() {
        return task.getWorkFolder() + fileName;
    }

    public void deleteFile() {
        File file = new File(getWorkPath());
        file.delete();
    }

    public void deleteWork() {
        deleteFile();
        person.works.remove(this);
        task.works.remove(this);
        task = null;
        person = null;
        delete();
    }
}
