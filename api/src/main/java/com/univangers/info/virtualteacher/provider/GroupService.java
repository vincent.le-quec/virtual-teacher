package com.univangers.info.virtualteacher.provider;

import com.univangers.info.virtualteacher.contract.group.IGroupService;
import com.univangers.info.virtualteacher.contract.user.IUserService;
import com.univangers.info.virtualteacher.core.Group;
import com.univangers.info.virtualteacher.core.Person;
import io.quarkus.security.Authenticated;
import org.jboss.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
@Authenticated
public class GroupService implements IGroupService {

    private static final Logger LOGGER = Logger.getLogger(GroupService.class);

    @Inject
    IUserService userService;

    @Override
    @RolesAllowed({"ADMIN", "TEACHER"})
    public Response fetchAll() {
        final Person p = userService.getPerson();
        LOGGER.info("[" + p.login + "] fetchAll");

        final List<String> groups = Group.listAll()
                .stream()
                .map(o -> (Group) o)
                .map(o -> o.title)
                .sorted()
                .collect(Collectors.toList());

        LOGGER.info("[" + p.login + "] fetchAll : return every groups");
        return Response.ok(groups).build();
    }
}
