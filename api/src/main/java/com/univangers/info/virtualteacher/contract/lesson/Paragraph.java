package com.univangers.info.virtualteacher.contract.lesson;

public class Paragraph {

    private long id;
    private String title;
    private Integer titleSize;
    private String content;
    private Integer order;
    private String type;

    public Paragraph() {
    }

    public Paragraph(String title, Integer titleSize, String content, Integer order, String type) {
        this.title = title;
        this.titleSize = titleSize;
        this.content = content;
        this.order = order;
        this.type = type;
    }

    public Paragraph(long id, String title, Integer titleSize, String content, Integer order, String type) {
        this.id = id;
        this.title = title;
        this.titleSize = titleSize;
        this.content = content;
        this.order = order;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTitleSize() {
        return titleSize;
    }

    public void setTitleSize(Integer titleSize) {
        this.titleSize = titleSize;
    }
}
