package com.univangers.info.virtualteacher.contract.lesson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class Task {

    private long id;
    private String title;
    private String description;
    private Date beginning;
    private Date ending;
    private Boolean displayed;
    @JsonProperty("contains_work")
    private Boolean containsWork;

    private List<Work> works;

    public Task() {
    }

    public Task(String title, String description, Date beginning, Date ending, Boolean displayed, Boolean containsWork) {
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.containsWork = containsWork;
    }

    public Task(long id, String title, String description, Date beginning, Date ending, Boolean displayed, Boolean containsWork, List<Work> works) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.beginning = beginning;
        this.ending = ending;
        this.displayed = displayed;
        this.works = works;
        this.containsWork = containsWork;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginning() {
        return beginning;
    }

    public void setBeginning(Date beginning) {
        this.beginning = beginning;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    public Boolean getContainsWork() {
        return containsWork;
    }

    public void setContainsWork(Boolean containsWork) {
        this.containsWork = containsWork;
    }

    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
    }
}
