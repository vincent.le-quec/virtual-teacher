package com.univangers.info.virtualteacher.provider.utils;

import com.univangers.info.virtualteacher.core.Paragraph;
import com.univangers.info.virtualteacher.provider.exception.BadRequestException;
import com.univangers.info.virtualteacher.provider.exception.NotFoundException;

import java.util.Optional;

public class ParagraphUtils {

    private ParagraphUtils() {
    }

    public static com.univangers.info.virtualteacher.core.Paragraph fetchParagraph(long id) {
        final Optional<Paragraph> paragraph = com.univangers.info.virtualteacher.core.Paragraph.findByIdOptional(id);
        if (paragraph.isEmpty()) {
            throw new NotFoundException("Unknown paragraph with id '" + id + "'");
        }
        return paragraph.get();
    }

    public static void validateNewContract(com.univangers.info.virtualteacher.contract.paragraph.Paragraph paragraph) {
        // Verify the lesson title is not empty
        if (paragraph.getTitle() == null || "".equals(paragraph.getTitle())) {
            throw new BadRequestException("A paragraph needs to have a title");
        }
    }

    public static void validateContract(com.univangers.info.virtualteacher.contract.paragraph.Paragraph paragraph) {
        // Verify the lesson title is not empty
        if ("".equals(paragraph.getTitle())) {
            throw new BadRequestException("A paragraph needs to have a title");
        }
    }
}
