package com.univangers.info.virtualteacher.core;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "paragraph", schema = "PUBLIC")
public class Paragraph extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String title;
    @Column(name = "title_size")
    public int titleSize;
    @Column(columnDefinition = "TEXT")
    public String content;
    @Column(name = "paragraph_order")
    public int order;
    public TypeFile type;

    @ManyToOne
    public Lesson lesson;

    public Paragraph() {
    }

    public Paragraph(String title, int titleSize, String content, int order, TypeFile type) {
        this.title = title;
        this.titleSize = titleSize;
        this.content = content;
        this.order = order;
        this.type = type;
    }

    public Paragraph(String title, int titleSize, String content, int order, TypeFile type, Lesson lesson) {
        this.title = title;
        this.titleSize = titleSize;
        this.content = content;
        this.order = order;
        this.type = type;
        this.lesson = lesson;
    }

    public static Optional<Paragraph> findByTitleOptional(String title) {
        return find("title", title).firstResultOptional();
    }
}
