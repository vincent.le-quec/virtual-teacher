package com.univangers.info.virtualteacher.provider.exception;

import org.jboss.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class BadRequestException extends WebApplicationException {

    private static final Logger LOGGER = Logger.getLogger(BadRequestException.class);

    public BadRequestException(final String m) {
        super(m, Response.status(Response.Status.BAD_REQUEST).entity(m).build());
        LOGGER.error(m);
    }
}
