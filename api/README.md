Find how to configure openAPI [documentation](https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations).

[Article](https://www.generation-nt.com/facebook-mots-passe-clair-stockage-instagram-actualite-1964226.html) sur les logs de mots de passe

Configure CAS following [this documentation](https://github.com/apereo/java-cas-client#spring-boot-autoconfiguration)

Here is the list of the users that you have access to :
(every user has for password "password")

### ADMIN :
-  tesla

### TEACHERS :
- einstein
- euler

### STUDENTS :
- riemann (group 2020TM2ACD1)
- gauss (group 2020TM2ACD1)
- euclid (group 2019TM1INF1)
- newton (group 2019TM1INF1)
- galieleo (group 2018TL3INF1)

### Connexion au leria
```shell script
ssh tbouriaud@leria-etud.univ-angers.fr -p 2019
_vps_kvm_create
_vps_kvm_start
_vps_kvm_connect
_vps_kvm_stop
```
[Mon site est déployé ici](https://etud-kvm-tbouriaud.leria-etud.univ-angers.fr/)

[documentation ici](https://wiki.info.univ-angers.fr/leria:cloud)

# Installation of Virtual Teacher

The virtual teacher API uses the framework Quarkus.
Here are the requirement to use this project.

## For development
### Requirements :
- Maven >= 3.6.3
- Java >= 11
- Docker >= 18.09.7
- Docker-Compose >= 1.23.1

For Ubuntu :
```shell script
bash install.sh
```

### Run :
```shell script
make dev
```

## For production
### Requirements :
- Docker >= 18.09.7
- Docker-Compose >= 1.23.1

### Run
Create the docker container for production (it needs cpu, memory and time) :
```shell script
make prod # Executes tests + generate executable
```