# install java 11
# https://doc.ubuntu-fr.org/openjdk
#install-java:
echo "INSTALLING JAVA 11 ..."
sudo mkdir -p /usr/lib/jvm && sudo wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz && sudo tar xvf openjdk-11.0.2_linux-x64_bin.tar.gz --directory /usr/lib/jvm/ && sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk-11.0.2/bin/java 1 && sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk-11.0.2/bin/javac 1
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt install openjdk-11-jre

# install maven
# https://www.vultr.com/docs/how-to-install-apache-maven-on-ubuntu-16-04
#install-maven:
echo "INSTALLING MAVEN ..."
cd /opt/
wget http://apache.mirrors.pair.com/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
sudo tar -xvzf apache-maven-3.6.3-bin.tar.gz
sudo mv apache-maven-3.6.3 maven
echo 'export M2_HOME=/opt/maven' >> /etc/profile.d/mavenenv.sh
echo 'export PATH=${M2_HOME}/bin:${PATH}' >> /etc/profile.d/mavenenv.sh
sudo chmod +x /etc/profile.d/mavenenv.sh
source /etc/profile.d/mavenenv.sh
echo "VERIFING MAVEN INSTALLATION"
mvn --version

# install docker
# https://docs.docker.com/engine/install/ubuntu/
echo "INSTALLING DOCKER ..."
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker --version

# install latest docker-compose version
# https://linuxize.com/post/how-to-install-and-use-docker-compose-on-ubuntu-18-04/
echo "INSTALLING DOCKER-COMPOSE ..."
sudo apt-get remove docker-compose
sudo rm /usr/local/bin/docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version